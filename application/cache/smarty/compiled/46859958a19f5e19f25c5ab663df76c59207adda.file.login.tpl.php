<?php /* Smarty version Smarty-3.1.18, created on 2014-09-30 13:42:17
         compiled from "..\application\views\admin\login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:29983542aeb79ea4173-90596330%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '46859958a19f5e19f25c5ab663df76c59207adda' => 
    array (
      0 => '..\\application\\views\\admin\\login.tpl',
      1 => 1412023107,
      2 => 'file',
    ),
    '4d9ddc900a69da25d22cc515be89ea544dd1c538' => 
    array (
      0 => '..\\application\\views\\admin\\layouts\\login_layout.tpl',
      1 => 1412023107,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '29983542aeb79ea4173-90596330',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_542aeb7a09e442_61143529',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_542aeb7a09e442_61143529')) {function content_542aeb7a09e442_61143529($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en-us">
	<head>
		<?php echo $_smarty_tpl->getSubTemplate ('admin/includes/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	</head>

	<body id="login" class="animated fadeInDown">
		<header id="header" style="background-color: #22262e !important; background-image: none;">
	    	<div id="logo-group">
				<?php echo $_smarty_tpl->getSubTemplate ('admin/includes/logo.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			</div>
		</header>

		<div id="main" role="main">

			<!-- MAIN CONTENT -->
			<div id="content" class="container">
				<div class="row">
					
					<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">

						<div class="well no-padding">
							<?php if (!empty($_smarty_tpl->tpl_vars['message']->value)) {?>
								<p class="alert alert-info"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
							<?php }?>

							<?php echo form_open('login','id="login-form" class="smart-form client-form"');?>
						
							
								<header>
									Sign In
								</header>

								<fieldset>
									
									<section>
										<label class="label">E-mail</label>
										<label class="input"> <i class="icon-append fa fa-user"></i>
											<input type="text" name="identity">											
											<b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter email address/username</b></label>
									</section>

									<section>
										<label class="label">Password</label>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="password" name="password">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
										<div class="note">
											<a href="<?php echo base_url('forgot_password');?>
">Forgot password?</a>
										</div>
									</section>

									<section>
										<label class="checkbox">
											<input type="checkbox" name="remember" >
											<i></i>Stay signed in</label>
									</section>
								</fieldset>
								<footer>
									<button type="submit" class="btn btn-primary">
										Sign in
									</button>
								</footer>							

							<?php echo form_close();?>

						</div>
					</div>
                
				</div>
			</div>

		</div>
        <?php echo $_smarty_tpl->getSubTemplate ('admin/includes/footer_scripts.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
        
        
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url('assets/js/plugin/jquery-validate/jquery.validate.min.js');?>
 "></script>

		<script type="text/javascript">
			runAllForms();

			$(function() {
				// Validation
				$("#login-form").validate({
					// Rules for form validation
					rules : {
						email : {
							required : true,
							email : true
						},
						password : {
							required : true,
							minlength : 3,
							maxlength : 20
						}
					},

					// Messages for form validation
					messages : {
						email : {
							required : 'Please enter your email address',
							email : 'Please enter a VALID email address'
						},
						password : {
							required : 'Please enter your password'
						}
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
			});
		</script>

	</body>
</html><?php }} ?>
