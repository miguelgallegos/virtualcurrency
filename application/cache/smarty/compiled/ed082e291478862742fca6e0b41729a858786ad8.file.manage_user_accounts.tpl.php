<?php /* Smarty version Smarty-3.1.18, created on 2014-09-30 16:45:03
         compiled from "..\application\views\admin\pages\manage_user_accounts.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10364542b164f4d93a3-06759198%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ed082e291478862742fca6e0b41729a858786ad8' => 
    array (
      0 => '..\\application\\views\\admin\\pages\\manage_user_accounts.tpl',
      1 => 1412023107,
      2 => 'file',
    ),
    'fbf6213b867d92e7d4c5db30ee131de7d1a84b34' => 
    array (
      0 => '..\\application\\views\\admin\\layouts\\admin_layout_navigation.tpl',
      1 => 1412023107,
      2 => 'file',
    ),
    '04d7c429c190a823fea00e5b5040d5ed0263fb68' => 
    array (
      0 => '..\\application\\views\\admin\\layouts\\admin_layout_full.tpl',
      1 => 1412023107,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10364542b164f4d93a3-06759198',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_542b164f961730_85815263',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_542b164f961730_85815263')) {function content_542b164f961730_85815263($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en-us">
	<head>
		<?php echo $_smarty_tpl->getSubTemplate ('admin/includes/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
        
    </head>
    
	<body class="smart-style-1 fixed-header">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
        <!-- possible classes: smart-style-0, smart-style-1, smart-style-2, smart-style-3, -->

		<!-- HEADER -->
	    <header id="header">
	        <div id="logo-group">
	            <!-- PLACE YOUR LOGO HERE -->
	            <?php echo $_smarty_tpl->getSubTemplate ('admin/includes/logo.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	            <!-- END LOGO PLACEHOLDER -->       
	        </div>
	    	
	    	<!-- pulled right: nav area -->
	    	<div class="pull-right">
	    
	    		<!-- logout button -->
	    		<div id="logout" class="btn-header transparent pull-right">
	    			<span> <a href="<?php echo base_url('logout');?>
" title="Sign Out"><i class="fa fa-sign-out"></i></a> </span>
	    		</div>
	    		<!-- end logout button -->
	    
	    	</div>
	    	<!-- end pulled right: nav area -->
	    
	    </header>

		<!-- END HEADER -->

		<!-- Left panel : NAVIGATION AREA -->		
		
    <!-- Note: This width of the aside area can be adjusted through LESS variables -->
    <aside id="left-panel">
    
    	<!-- User info -->
    	<div class="login-info"></div>
    	<!-- end user info -->
    	<nav>
    		<ul>
    			<li <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='dashboard') {?> class="active" <?php }?>>
    				<a href="<?php echo base_url('admin/dashboard');?>
"><i class="fa fa-lg fa-fw fa-tachometer"></i><span class="menu-item-parent">Dashboard</span></a>
    			</li>

                <li <?php if (strstr($_smarty_tpl->tpl_vars['current_page']->value,"manage_user")) {?> class="active open" <?php }?>>
                    <a href="#"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">Manage Users</span></a>
                    <ul>
                        <li <?php if (strstr($_smarty_tpl->tpl_vars['current_page']->value,'manage_user_accounts')) {?> class="active" <?php }?>>
                            <a href="<?php echo base_url('admin/manage_users/accounts');?>
">User Accounts</a>
                        </li>

                        <li <?php if (strstr($_smarty_tpl->tpl_vars['current_page']->value,'manage_user_groups')) {?> class="active" <?php }?>>
                            <a href="<?php echo base_url('admin/manage_users/groups');?>
">User Groups</a>
                        </li>
                    </ul>
                </li>

    			<li <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_offers') {?> class="active" <?php }?>>
    				<a href="<?php echo base_url('admin/manage_offers');?>
"><i class="fa fa-lg fa-fw fa-pencil"></i><span class="menu-item-parent">Manage Offers</span></a>
    			</li>

    			<li <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='stats') {?> class="active" <?php }?>>
    				<a href="<?php echo base_url('admin/stats');?>
"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i><span class="menu-item-parent">Statistics</span></a>
    			</li>

                <li <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='update') {?> class="active" <?php }?>>
                    <a href="<?php echo base_url('admin/update');?>
"><i class="fa fa-lg fa-fw fa-cloud-upload"></i><span class="menu-item-parent">Update Database</span></a>
                </li>
            </ul>
    	</nav>
    
    </aside>

        <!-- END NAVIGATION -->

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">
				<!-- breadcrumb -->
					
	<ol class="breadcrumb">
		<?php if ($_smarty_tpl->tpl_vars['current_page']->value=='dashboard') {?>
			<li>Home</li><li>Dashboard</li>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_offers') {?>
			<li>Home</li><li>Manage Offers</li>
		<?php }?>

        <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_user_accounts') {?>
            <li>Home</li><li>Manage Users</li><li>User Accounts</li>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_user_accounts_create') {?>
            <li>Home</li><li>Manage Users</li><li>User Accounts</li><li>Create</li>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_user_accounts_edit') {?>
            <li>Home</li><li>Manage Users</li><li>User Accounts</li><li>Edit</li>
        <?php }?>        

        <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_user_groups') {?>
            <li>Home</li><li>Manage Users</li><li>User Groups</li>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_user_groups_create') {?>
            <li>Home</li><li>Manage Users</li><li>User Groups</li><li>Create</li>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_user_groups_edit') {?>
            <li>Home</li><li>Manage Users</li><li>User Groups</li><li>Edit</li>
        <?php }?>

		<?php if ($_smarty_tpl->tpl_vars['current_page']->value=='stats') {?>
			<li>Home</li><li>Statistics</li>
		<?php }?>				

		<?php if ($_smarty_tpl->tpl_vars['current_page']->value=='update') {?>
			<li>Home</li><li>Update Database</li>
		<?php }?>
	</ol>

				<!-- end breadcrumb -->
			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						

<!-- Link to datatables plugin -->
<link rel="stylesheet" type="text/css" media="screen" href="//cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
<script src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>    


<script>
	$(document).ready(function(){
	    $('#table_manage_user_accounts').dataTable(
			{ "aaSorting": [ [0, 'asc'] ]  }

	    );	    
	});
</script>

<section id="widget-grid">
        <!-- row -->
        <div class="row">
            <!-- SINGLE GRID -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0">
			 
			        <header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2><?php echo lang('index_heading');?>
</h2>
			        </header><!-- widget div-->
			 
			        <div>
			        	<p><?php echo lang('index_subheading');?>
</p>
			            <!-- widget edit box -->
			            <div class="jarviswidget-editbox">
			                <!-- This area used as dropdown edit box -->
			                <input class="form-control" type="text">
			            </div><!-- end widget edit box -->
			 
			            <!-- widget content -->
			            <div class="widget-body">

							<?php if (!empty($_smarty_tpl->tpl_vars['message']->value)) {?>
								<p class="alert alert-info"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
							<?php }?>										            

							<p>
								<a href="<?php echo base_url('admin/manage_users/create_user');?>
"><?php echo lang('index_create_user_link');?>
</a> | 
								<a href="<?php echo base_url('admin/manage_users/create_group');?>
"><?php echo lang('index_create_group_link');?>
</a>
							</p>

							<div class="table-responsive">
							
								<table id='table_manage_user_accounts' class="table table-bordered table-striped table-hover" width="100%">
									<thead>
										<tr>
											<th><?php echo lang('index_fname_th');?>
</th>
											<th><?php echo lang('index_lname_th');?>
</th>
											<th><?php echo lang('index_email_th');?>
</th>
											<th><?php echo lang('index_groups_th');?>
</th>
											<th><?php echo lang('index_status_th');?>
</th>
											<th><?php echo lang('index_action_th');?>
</th>
										</tr>
									</thead>
									<tbody>

										<?php  $_smarty_tpl->tpl_vars['user'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['user']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['users']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['user']->key => $_smarty_tpl->tpl_vars['user']->value) {
$_smarty_tpl->tpl_vars['user']->_loop = true;
?>
										<tr>
											<td><?php echo $_smarty_tpl->tpl_vars['user']->value->first_name;?>
</td>
											<td><?php echo $_smarty_tpl->tpl_vars['user']->value->last_name;?>
</td>
											<td><?php echo $_smarty_tpl->tpl_vars['user']->value->email;?>
</td>

											<td>
												<?php  $_smarty_tpl->tpl_vars['group'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['group']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['user']->value->groups; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['group']->key => $_smarty_tpl->tpl_vars['group']->value) {
$_smarty_tpl->tpl_vars['group']->_loop = true;
?>
													<?php echo anchor(("admin/manage_users/edit_group/").($_smarty_tpl->tpl_vars['group']->value->id),$_smarty_tpl->tpl_vars['group']->value->name);?>
<br />													
								                <?php } ?>
											</td>
											<td>
												<?php if ($_smarty_tpl->tpl_vars['user']->value->active) {?>
													<?php echo lang('index_active_link');?>

												<?php } else { ?>
													<?php echo lang('index_inactive_link');?>
													
												<?php }?>
											</td>
											<td>
												<?php if ($_smarty_tpl->tpl_vars['user']->value->active) {?>
													<?php echo anchor(('admin/manage_users/deactivate_user/').($_smarty_tpl->tpl_vars['user']->value->id),'Deactivate');?>
 | 
												<?php } else { ?>
													<?php echo anchor(('admin/manage_users/activate_user/').($_smarty_tpl->tpl_vars['user']->value->id),'Activate');?>
 | 
												<?php }?>											
												<?php echo anchor(('admin/manage_users/edit_user/').($_smarty_tpl->tpl_vars['user']->value->id),'Edit');?>
 | 
												<?php echo anchor(('admin/manage_users/delete_user/').($_smarty_tpl->tpl_vars['user']->value->id),'Delete');?>

											</td>
										</tr>
										<?php } ?>

									</tbody>
								</table>
								
							</div>			                
			 
			            </div><!-- end widget content -->
			        </div><!-- end widget div -->
			    </div><!-- end widget -->
 
            </article><!-- END GRID -->
           
 
        </div><!-- end row -->
 
    </section><!-- end widget grid -->


                    
                        
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->

		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white">JPNC | IncentEngine 2000 © 2013-2014</span>
				</div>
			</div>
		</div>		

		<?php echo $_smarty_tpl->getSubTemplate ('admin/includes/footer_scripts.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
        
        
		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();
			
				
			});

		</script>        

	</body>
</html><?php }} ?>
