<?php /* Smarty version Smarty-3.1.18, created on 2014-09-30 16:50:23
         compiled from "..\application\views\admin\pages\manage_user_accounts_edit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11196542b178f5730b3-30306468%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'acdb8c982df0fe511760ee7b29bf0976e891bcbb' => 
    array (
      0 => '..\\application\\views\\admin\\pages\\manage_user_accounts_edit.tpl',
      1 => 1412023107,
      2 => 'file',
    ),
    'fbf6213b867d92e7d4c5db30ee131de7d1a84b34' => 
    array (
      0 => '..\\application\\views\\admin\\layouts\\admin_layout_navigation.tpl',
      1 => 1412023107,
      2 => 'file',
    ),
    '04d7c429c190a823fea00e5b5040d5ed0263fb68' => 
    array (
      0 => '..\\application\\views\\admin\\layouts\\admin_layout_full.tpl',
      1 => 1412023107,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11196542b178f5730b3-30306468',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_542b178fa551e9_67885205',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_542b178fa551e9_67885205')) {function content_542b178fa551e9_67885205($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en-us">
	<head>
		<?php echo $_smarty_tpl->getSubTemplate ('admin/includes/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
        
    </head>
    
	<body class="smart-style-1 fixed-header">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
        <!-- possible classes: smart-style-0, smart-style-1, smart-style-2, smart-style-3, -->

		<!-- HEADER -->
	    <header id="header">
	        <div id="logo-group">
	            <!-- PLACE YOUR LOGO HERE -->
	            <?php echo $_smarty_tpl->getSubTemplate ('admin/includes/logo.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	            <!-- END LOGO PLACEHOLDER -->       
	        </div>
	    	
	    	<!-- pulled right: nav area -->
	    	<div class="pull-right">
	    
	    		<!-- logout button -->
	    		<div id="logout" class="btn-header transparent pull-right">
	    			<span> <a href="<?php echo base_url('logout');?>
" title="Sign Out"><i class="fa fa-sign-out"></i></a> </span>
	    		</div>
	    		<!-- end logout button -->
	    
	    	</div>
	    	<!-- end pulled right: nav area -->
	    
	    </header>

		<!-- END HEADER -->

		<!-- Left panel : NAVIGATION AREA -->		
		
    <!-- Note: This width of the aside area can be adjusted through LESS variables -->
    <aside id="left-panel">
    
    	<!-- User info -->
    	<div class="login-info"></div>
    	<!-- end user info -->
    	<nav>
    		<ul>
    			<li <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='dashboard') {?> class="active" <?php }?>>
    				<a href="<?php echo base_url('admin/dashboard');?>
"><i class="fa fa-lg fa-fw fa-tachometer"></i><span class="menu-item-parent">Dashboard</span></a>
    			</li>

                <li <?php if (strstr($_smarty_tpl->tpl_vars['current_page']->value,"manage_user")) {?> class="active open" <?php }?>>
                    <a href="#"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">Manage Users</span></a>
                    <ul>
                        <li <?php if (strstr($_smarty_tpl->tpl_vars['current_page']->value,'manage_user_accounts')) {?> class="active" <?php }?>>
                            <a href="<?php echo base_url('admin/manage_users/accounts');?>
">User Accounts</a>
                        </li>

                        <li <?php if (strstr($_smarty_tpl->tpl_vars['current_page']->value,'manage_user_groups')) {?> class="active" <?php }?>>
                            <a href="<?php echo base_url('admin/manage_users/groups');?>
">User Groups</a>
                        </li>
                    </ul>
                </li>

    			<li <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_offers') {?> class="active" <?php }?>>
    				<a href="<?php echo base_url('admin/manage_offers');?>
"><i class="fa fa-lg fa-fw fa-pencil"></i><span class="menu-item-parent">Manage Offers</span></a>
    			</li>

    			<li <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='stats') {?> class="active" <?php }?>>
    				<a href="<?php echo base_url('admin/stats');?>
"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i><span class="menu-item-parent">Statistics</span></a>
    			</li>

                <li <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='update') {?> class="active" <?php }?>>
                    <a href="<?php echo base_url('admin/update');?>
"><i class="fa fa-lg fa-fw fa-cloud-upload"></i><span class="menu-item-parent">Update Database</span></a>
                </li>
            </ul>
    	</nav>
    
    </aside>

        <!-- END NAVIGATION -->

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">
				<!-- breadcrumb -->
					
	<ol class="breadcrumb">
		<?php if ($_smarty_tpl->tpl_vars['current_page']->value=='dashboard') {?>
			<li>Home</li><li>Dashboard</li>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_offers') {?>
			<li>Home</li><li>Manage Offers</li>
		<?php }?>

        <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_user_accounts') {?>
            <li>Home</li><li>Manage Users</li><li>User Accounts</li>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_user_accounts_create') {?>
            <li>Home</li><li>Manage Users</li><li>User Accounts</li><li>Create</li>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_user_accounts_edit') {?>
            <li>Home</li><li>Manage Users</li><li>User Accounts</li><li>Edit</li>
        <?php }?>        

        <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_user_groups') {?>
            <li>Home</li><li>Manage Users</li><li>User Groups</li>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_user_groups_create') {?>
            <li>Home</li><li>Manage Users</li><li>User Groups</li><li>Create</li>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_user_groups_edit') {?>
            <li>Home</li><li>Manage Users</li><li>User Groups</li><li>Edit</li>
        <?php }?>

		<?php if ($_smarty_tpl->tpl_vars['current_page']->value=='stats') {?>
			<li>Home</li><li>Statistics</li>
		<?php }?>				

		<?php if ($_smarty_tpl->tpl_vars['current_page']->value=='update') {?>
			<li>Home</li><li>Update Database</li>
		<?php }?>
	</ol>

				<!-- end breadcrumb -->
			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						

<section id="widget-grid">
        <!-- row -->
        <div class="row">
            <!-- SINGLE GRID -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0">
			 
			        <header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2><?php echo lang('groups_heading');?>
</h2>
			        </header><!-- widget div-->
			 
			        <div>
			            <!-- widget edit box -->
			            <div class="jarviswidget-editbox">
			                <!-- This area used as dropdown edit box -->
			                <input class="form-control" type="text">
			            </div><!-- end widget edit box -->
			 
			            <!-- widget content -->
			            <div class="widget-body">

							<?php if (!empty($_smarty_tpl->tpl_vars['message']->value)) {?>
								<p class="alert alert-info"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
							<?php }?>

							<h1><?php echo lang('edit_user_heading');?>
</h1>
							<p><?php echo lang('edit_user_subheading');?>
</p>



							<?php echo form_open(("admin/manage_users/edit_user/").($_smarty_tpl->tpl_vars['id']->value),'class="smart-form"');?>
						

								<fieldset>
									<div class="row">
										<section class="col col-6">
											<label class="label"><?php echo lang('edit_user_fname_label','first_name');?>
</label>
											<label class="input">
												<input type="<?php echo $_smarty_tpl->tpl_vars['first_name']->value['type'];?>
" name="<?php echo $_smarty_tpl->tpl_vars['first_name']->value['name'];?>
" id="<?php echo $_smarty_tpl->tpl_vars['first_name']->value['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['first_name']->value['value'];?>
">
											</label>
										</section>
									</div>

									<div class="row">
										<section class="col col-6">
											<label class="label"><?php echo lang('edit_user_lname_label','last_name');?>
</label>
											<label class="input">
												<input type="<?php echo $_smarty_tpl->tpl_vars['last_name']->value['type'];?>
" name="<?php echo $_smarty_tpl->tpl_vars['last_name']->value['name'];?>
" id="<?php echo $_smarty_tpl->tpl_vars['last_name']->value['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['last_name']->value['value'];?>
">
											</label>
										</section>
									</div>

									<div class="row">
										<section class="col col-6">
											<label class="label"><?php echo lang('edit_user_company_label','company');?>
</label>
											<label class="input">
												<input type="<?php echo $_smarty_tpl->tpl_vars['company']->value['type'];?>
" name="<?php echo $_smarty_tpl->tpl_vars['company']->value['name'];?>
" id="<?php echo $_smarty_tpl->tpl_vars['company']->value['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['company']->value['value'];?>
">
											</label>
										</section>
									</div>

									<div class="row">
										<section class="col col-6">
											<label class="label"><?php echo lang('edit_user_phone_label','phone');?>
</label>
											<label class="input">
												<input type="<?php echo $_smarty_tpl->tpl_vars['phone']->value['type'];?>
" name="<?php echo $_smarty_tpl->tpl_vars['phone']->value['name'];?>
" id="<?php echo $_smarty_tpl->tpl_vars['phone']->value['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['phone']->value['value'];?>
">				
											</label>
										</section>
									</div>

									<div class="row">
										<section class="col col-6">
											<label class="label"><?php echo lang('edit_user_password_label','password');?>
</label>
											<label class="input">
												<input type="<?php echo $_smarty_tpl->tpl_vars['password']->value['type'];?>
" name="<?php echo $_smarty_tpl->tpl_vars['password']->value['name'];?>
" id="<?php echo $_smarty_tpl->tpl_vars['password']->value['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['password']->value['value'];?>
">				
											</label>
										</section>
									</div>

									<div class="row">
										<section class="col col-6">
											<label class="label"><?php echo lang('edit_user_password_confirm_label','password_confirm');?>
</label>
											<label class="input">
												<input type="<?php echo $_smarty_tpl->tpl_vars['password_confirm']->value['type'];?>
" name="<?php echo $_smarty_tpl->tpl_vars['password_confirm']->value['name'];?>
" id="<?php echo $_smarty_tpl->tpl_vars['password_confirm']->value['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['password_confirm']->value['value'];?>
"				
											</label>
										</section>
									</div>

									<div class="row">
										<section class="col col-6">
											<h3><?php echo lang('edit_user_groups_heading');?>
</h3>

											<?php  $_smarty_tpl->tpl_vars['group'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['group']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['groups']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['group']->key => $_smarty_tpl->tpl_vars['group']->value) {
$_smarty_tpl->tpl_vars['group']->_loop = true;
?>
												<label class="checkbox">
													<input type="checkbox" name="groups[]" value="<?php echo $_smarty_tpl->tpl_vars['group']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['user_groups']->value[$_smarty_tpl->tpl_vars['group']->value['id']]['checked']=='1') {?>checked=checked<?php }?>>
													<i></i><?php echo $_smarty_tpl->tpl_vars['group']->value['description'];?>
 [<?php echo $_smarty_tpl->tpl_vars['group']->value['name'];?>
]
												</label>
											<?php } ?>

										</section>
									</div>

									<input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->id;?>
">
									<input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['csrf']->value['name'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['csrf']->value['value'];?>
">      

								</fieldset>									

								<footer>
									<button type="submit" class="btn btn-primary" >
										<?php echo lang('edit_user_submit_btn');?>

									</button>
								</footer>									

							<?php echo form_close();?>

			 
			            </div><!-- end widget content -->
			        </div><!-- end widget div -->
			    </div><!-- end widget -->
 
            </article><!-- END GRID -->
           
 
        </div><!-- end row -->
 
    </section><!-- end widget grid -->


                    
                        
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->

		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white">JPNC | IncentEngine 2000 © 2013-2014</span>
				</div>
			</div>
		</div>		

		<?php echo $_smarty_tpl->getSubTemplate ('admin/includes/footer_scripts.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
        
        
		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();
			
				
			});

		</script>        

	</body>
</html><?php }} ?>
