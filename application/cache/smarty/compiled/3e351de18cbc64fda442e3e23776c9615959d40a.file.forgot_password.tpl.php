<?php /* Smarty version Smarty-3.1.18, created on 2014-09-30 16:57:23
         compiled from "..\application\views\admin\forgot_password.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6761542b1933d8db09-65942357%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3e351de18cbc64fda442e3e23776c9615959d40a' => 
    array (
      0 => '..\\application\\views\\admin\\forgot_password.tpl',
      1 => 1412023107,
      2 => 'file',
    ),
    '4d9ddc900a69da25d22cc515be89ea544dd1c538' => 
    array (
      0 => '..\\application\\views\\admin\\layouts\\login_layout.tpl',
      1 => 1412023107,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6761542b1933d8db09-65942357',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_542b1933eaec41_37030439',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_542b1933eaec41_37030439')) {function content_542b1933eaec41_37030439($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en-us">
	<head>
		<?php echo $_smarty_tpl->getSubTemplate ('admin/includes/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	</head>

	<body id="login" class="animated fadeInDown">
		<header id="header" style="background-color: #22262e !important; background-image: none;">
	    	<div id="logo-group">
				<?php echo $_smarty_tpl->getSubTemplate ('admin/includes/logo.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			</div>
		</header>

		<div id="main" role="main">

			<!-- MAIN CONTENT -->
			<div id="content" class="container">
				<div class="row">
					
					<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">

						<div class="well no-padding">

							<?php if (!empty($_smarty_tpl->tpl_vars['message']->value)) {?>
								<p class="alert alert-info"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
							<?php }?>						

							<?php echo form_open('forgot_password','id="login-form" class="smart-form client-form"');?>
						
							
								<header>
									Forgot Password
								</header>

								<fieldset>

									<section>
										<label class="label">Enter your email address</label>
										<label class="input"> <i class="icon-append fa fa-envelope"></i>
											<input type="email" name="email">
											<b class="tooltip tooltip-top-right"><i class="fa fa-envelope txt-color-teal"></i> Please enter email address for password reset</b></label>
									</section>

								</fieldset>
								<footer>
									<button type="submit" class="btn btn-primary">
										<i class="fa fa-refresh"></i> Reset Password
									</button>
								</footer>							

							<?php echo form_close();?>

						</div>
					</div>
                
				</div>
			</div>

		</div>
        <?php echo $_smarty_tpl->getSubTemplate ('admin/includes/footer_scripts.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
        
        
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url('assets/js/plugin/jquery-validate/jquery.validate.min.js');?>
 "></script>

		<script type="text/javascript">
			runAllForms();

			$(function() {
				// Validation
				$("#login-form").validate({
					// Rules for form validation
					rules : {
						email : {
							required : true,
							email : true
						},
						password : {
							required : true,
							minlength : 3,
							maxlength : 20
						}
					},

					// Messages for form validation
					messages : {
						email : {
							required : 'Please enter your email address',
							email : 'Please enter a VALID email address'
						},
						password : {
							required : 'Please enter your password'
						}
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
			});
		</script>

	</body>
</html><?php }} ?>
