<?php /* Smarty version Smarty-3.1.18, created on 2014-09-30 16:30:40
         compiled from "..\application\views\admin\pages\dashboard.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15086542b12f0c8e756-98380761%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2d5bae41294d9ec04b5d1a885d79e5bae89cd1e4' => 
    array (
      0 => '..\\application\\views\\admin\\pages\\dashboard.tpl',
      1 => 1412023107,
      2 => 'file',
    ),
    'fbf6213b867d92e7d4c5db30ee131de7d1a84b34' => 
    array (
      0 => '..\\application\\views\\admin\\layouts\\admin_layout_navigation.tpl',
      1 => 1412023107,
      2 => 'file',
    ),
    '04d7c429c190a823fea00e5b5040d5ed0263fb68' => 
    array (
      0 => '..\\application\\views\\admin\\layouts\\admin_layout_full.tpl',
      1 => 1412023107,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15086542b12f0c8e756-98380761',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_542b12f1382249_94574718',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_542b12f1382249_94574718')) {function content_542b12f1382249_94574718($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en-us">
	<head>
		<?php echo $_smarty_tpl->getSubTemplate ('admin/includes/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
        
    </head>
    
	<body class="smart-style-1 fixed-header">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
        <!-- possible classes: smart-style-0, smart-style-1, smart-style-2, smart-style-3, -->

		<!-- HEADER -->
	    <header id="header">
	        <div id="logo-group">
	            <!-- PLACE YOUR LOGO HERE -->
	            <?php echo $_smarty_tpl->getSubTemplate ('admin/includes/logo.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	            <!-- END LOGO PLACEHOLDER -->       
	        </div>
	    	
	    	<!-- pulled right: nav area -->
	    	<div class="pull-right">
	    
	    		<!-- logout button -->
	    		<div id="logout" class="btn-header transparent pull-right">
	    			<span> <a href="<?php echo base_url('logout');?>
" title="Sign Out"><i class="fa fa-sign-out"></i></a> </span>
	    		</div>
	    		<!-- end logout button -->
	    
	    	</div>
	    	<!-- end pulled right: nav area -->
	    
	    </header>

		<!-- END HEADER -->

		<!-- Left panel : NAVIGATION AREA -->		
		
    <!-- Note: This width of the aside area can be adjusted through LESS variables -->
    <aside id="left-panel">
    
    	<!-- User info -->
    	<div class="login-info"></div>
    	<!-- end user info -->
    	<nav>
    		<ul>
    			<li <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='dashboard') {?> class="active" <?php }?>>
    				<a href="<?php echo base_url('admin/dashboard');?>
"><i class="fa fa-lg fa-fw fa-tachometer"></i><span class="menu-item-parent">Dashboard</span></a>
    			</li>

                <li <?php if (strstr($_smarty_tpl->tpl_vars['current_page']->value,"manage_user")) {?> class="active open" <?php }?>>
                    <a href="#"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">Manage Users</span></a>
                    <ul>
                        <li <?php if (strstr($_smarty_tpl->tpl_vars['current_page']->value,'manage_user_accounts')) {?> class="active" <?php }?>>
                            <a href="<?php echo base_url('admin/manage_users/accounts');?>
">User Accounts</a>
                        </li>

                        <li <?php if (strstr($_smarty_tpl->tpl_vars['current_page']->value,'manage_user_groups')) {?> class="active" <?php }?>>
                            <a href="<?php echo base_url('admin/manage_users/groups');?>
">User Groups</a>
                        </li>
                    </ul>
                </li>

    			<li <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_offers') {?> class="active" <?php }?>>
    				<a href="<?php echo base_url('admin/manage_offers');?>
"><i class="fa fa-lg fa-fw fa-pencil"></i><span class="menu-item-parent">Manage Offers</span></a>
    			</li>

    			<li <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='stats') {?> class="active" <?php }?>>
    				<a href="<?php echo base_url('admin/stats');?>
"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i><span class="menu-item-parent">Statistics</span></a>
    			</li>

                <li <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='update') {?> class="active" <?php }?>>
                    <a href="<?php echo base_url('admin/update');?>
"><i class="fa fa-lg fa-fw fa-cloud-upload"></i><span class="menu-item-parent">Update Database</span></a>
                </li>
            </ul>
    	</nav>
    
    </aside>

        <!-- END NAVIGATION -->

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">
				<!-- breadcrumb -->
					
	<ol class="breadcrumb">
		<?php if ($_smarty_tpl->tpl_vars['current_page']->value=='dashboard') {?>
			<li>Home</li><li>Dashboard</li>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_offers') {?>
			<li>Home</li><li>Manage Offers</li>
		<?php }?>

        <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_user_accounts') {?>
            <li>Home</li><li>Manage Users</li><li>User Accounts</li>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_user_accounts_create') {?>
            <li>Home</li><li>Manage Users</li><li>User Accounts</li><li>Create</li>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_user_accounts_edit') {?>
            <li>Home</li><li>Manage Users</li><li>User Accounts</li><li>Edit</li>
        <?php }?>        

        <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_user_groups') {?>
            <li>Home</li><li>Manage Users</li><li>User Groups</li>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_user_groups_create') {?>
            <li>Home</li><li>Manage Users</li><li>User Groups</li><li>Create</li>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['current_page']->value=='manage_user_groups_edit') {?>
            <li>Home</li><li>Manage Users</li><li>User Groups</li><li>Edit</li>
        <?php }?>

		<?php if ($_smarty_tpl->tpl_vars['current_page']->value=='stats') {?>
			<li>Home</li><li>Statistics</li>
		<?php }?>				

		<?php if ($_smarty_tpl->tpl_vars['current_page']->value=='update') {?>
			<li>Home</li><li>Update Database</li>
		<?php }?>
	</ol>

				<!-- end breadcrumb -->
			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						

<?php $_smarty_tpl->tpl_vars['script_start'] = new Smarty_variable('<script type="text/javascript" src="', null, 0);?>
<?php $_smarty_tpl->tpl_vars['script_end'] = new Smarty_variable('"></script>', null, 0);?>


<section id="widget-grid">
 
        <!-- row -->
        <div class="row">

            <article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0">
			 
			        <header>
						<span class="widget-icon"> <i class="fa fa-cog"></i> </span>
			            <h2>Content Locker</h2>
			        </header><!-- widget div-->
			 
			        <div>
			            <!-- widget edit box -->
			            <div class="jarviswidget-editbox">
			                <!-- This area used as dropdown edit box -->
			                <input class="form-control" type="text">
			            </div><!-- end widget edit box -->
			 
			            <!-- widget content -->
			            <div class="widget-body">

							<?php $_smarty_tpl->tpl_vars['direct_link'] = new Smarty_variable(base_url('locker/content_locker?fn=direct&w=123ac37bed0a0cafa41a86310a036d82'), null, 0);?>	
							<?php $_smarty_tpl->tpl_vars['content_locker_src'] = new Smarty_variable(base_url('locker/content_locker?fn=script&w=123ac37bed0a0cafa41a86310a036d82'), null, 0);?>

							<?php $_smarty_tpl->tpl_vars['onclick_version_src'] = new Smarty_variable(base_url('locker/content_locker?fn=onclick&w=123ac37bed0a0cafa41a86310a036d82'), null, 0);?>
							<?php $_smarty_tpl->tpl_vars['onclick_load_widget'] = new Smarty_variable('<a onclick="load_widget()" href="#">Click to Load Widget</a>', null, 0);?>

							<h3>Direct Link</h3>
							<textarea style="font-size:13px;width:100%;display:table-caption !important;" rows="2"><?php echo $_smarty_tpl->tpl_vars['direct_link']->value;?>
</textarea>
							<p><a href="<?php echo $_smarty_tpl->tpl_vars['direct_link']->value;?>
" target="_blank">Content Locker Sample</a></p>
							<p><a href="http://tbm.verifystation.com/go.php?w=f9b09ce4369c1129f6791547d6ad9c2d" target="_blank">TBM Original Content Locker Example</a>							

							<h3>Content Locker Code</h3>
							<textarea style="font-size:13px;width:100%;display:table-caption !important;" rows="2"><?php echo $_smarty_tpl->tpl_vars['script_start']->value;?>
<?php echo $_smarty_tpl->tpl_vars['content_locker_src']->value;?>
<?php echo $_smarty_tpl->tpl_vars['script_end']->value;?>
</textarea>

							<h3>On-Click Version</h3>
							<textarea style="font-size:13px;width:100%;display:table-caption !important;" rows="2"><?php echo $_smarty_tpl->tpl_vars['script_start']->value;?>
<?php echo $_smarty_tpl->tpl_vars['onclick_version_src']->value;?>
<?php echo $_smarty_tpl->tpl_vars['script_end']->value;?>
</textarea>
							<textarea style="font-size:13px;width:100%;display:table-caption !important;" rows="2"><?php echo $_smarty_tpl->tpl_vars['onclick_load_widget']->value;?>
</textarea>
			 
			            </div><!-- end widget content -->
			        </div><!-- end widget div -->
			    </div><!-- end widget -->

				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-1">
			 
			        <header>
						<span class="widget-icon"> <i class="fa fa-cog"></i> </span>
			            <h2>Offer Wall</h2>
			        </header><!-- widget div-->
			 
			        <div>
			            <!-- widget edit box -->
			            <div class="jarviswidget-editbox">
			                <!-- This area used as dropdown edit box -->
			                <input class="form-control" type="text">
			            </div><!-- end widget edit box -->
			 
			            <!-- widget content -->
			            <div class="widget-body">
							
							<?php $_smarty_tpl->tpl_vars['offer_wall'] = new Smarty_variable(base_url('locker/offer_wall?w=610685b00395fb6d9b33a957468b5fe4'), null, 0);?>								

							<textarea style="font-size:13px;width:100%;display:table-caption !important;" rows="2"><?php echo $_smarty_tpl->tpl_vars['offer_wall']->value;?>
</textarea>
							<p><a href="<?php echo $_smarty_tpl->tpl_vars['offer_wall']->value;?>
" target="_blank">Offer Wall Sample</a></p>
							<p><a href="http://tbm.verifystation.com/offer_wall/?w=610685b00395fb6d9b33a957468b5fe4" target="_blank">TBM Original Offer Wall Example</a>							

			            </div><!-- end widget content -->
			        </div><!-- end widget div -->
			    </div><!-- end widget -->

            </article><!-- END GRID -->

            <article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0">
			 
			        <header>
						<span class="widget-icon"> <i class="fa fa-cog"></i> </span>
			            <h2>File Locker</h2>
			        </header><!-- widget div-->
			 
			        <div>
			            <!-- widget edit box -->
			            <div class="jarviswidget-editbox">
			                <!-- This area used as dropdown edit box -->
			                <input class="form-control" type="text">
			            </div><!-- end widget edit box -->
			 
			            <!-- widget content -->
			            <div class="widget-body">

							<?php $_smarty_tpl->tpl_vars['direct_link'] = new Smarty_variable(base_url('locker/file_locker?l=c2c52f392bd63e45749bf829eec4d7c8'), null, 0);?>
							<?php $_smarty_tpl->tpl_vars['html_code'] = new Smarty_variable((('<a href="').($_smarty_tpl->tpl_vars['direct_link']->value)).('">Download Now</a>'), null, 0);?>

							<h3>Direct Link</h3>							
							<textarea style="font-size:13px;width:100%;display:table-caption !important;" rows="2"><?php echo $_smarty_tpl->tpl_vars['direct_link']->value;?>
</textarea>
							<p><a href="<?php echo $_smarty_tpl->tpl_vars['direct_link']->value;?>
" target="_blank">File Locker Sample</a></p>
							<p><a href="http://tbm.verifystation.com/file_lock/?l=c2c52f392bd63e45749bf829eec4d7c8" target="_blank">TBM Original File Locker Example</a>

							<h3>HTML Code</h3>							
							<textarea style="font-size:13px;width:100%;display:table-caption !important;" rows="2"><?php echo $_smarty_tpl->tpl_vars['html_code']->value;?>
</textarea>			            
		 
			            </div><!-- end widget content -->
			        </div><!-- end widget div -->
			    </div><!-- end widget -->

				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-1">
			 
			        <header>
						<span class="widget-icon"> <i class="fa fa-cog"></i> </span>
			            <h2>Super URL</h2>
			        </header><!-- widget div-->
			 
			        <div>
			            <!-- widget edit box -->
			            <div class="jarviswidget-editbox">
			                <!-- This area used as dropdown edit box -->
			                <input class="form-control" type="text">
			            </div><!-- end widget edit box -->
			 
			            <!-- widget content -->
			            <div class="widget-body">

							<?php $_smarty_tpl->tpl_vars['super_url'] = new Smarty_variable(base_url('locker/super_url?a=1002'), null, 0);?>								

							<textarea style="font-size:13px;width:100%;display:table-caption !important;" rows="2"><?php echo $_smarty_tpl->tpl_vars['super_url']->value;?>
</textarea>
							<p><a href="<?php echo $_smarty_tpl->tpl_vars['super_url']->value;?>
" target="_blank">Super URL Sample</a></p>
							<p><a href="http://tbm.verifystation.com/my_url.php?a=1002" target="_blank">TBM Original Super URL Example</a>

			            </div><!-- end widget content -->
			        </div><!-- end widget div -->
			    </div><!-- end widget -->

				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-1">
			 
			        <header>
						<span class="widget-icon"> <i class="fa fa-cog"></i> </span>
			            <h2>Landing Page</h2>
			        </header><!-- widget div-->
			 
			        <div>
			            <!-- widget edit box -->
			            <div class="jarviswidget-editbox">
			                <!-- This area used as dropdown edit box -->
			                <input class="form-control" type="text">
			            </div><!-- end widget edit box -->
			 
			            <!-- widget content -->
			            <div class="widget-body">

							<?php $_smarty_tpl->tpl_vars['landing_page'] = new Smarty_variable(base_url('locker/landing_page?l=6a37e5a906493cf06d2920d6fba087d1'), null, 0);?>								

							<textarea style="font-size:13px;width:100%;display:table-caption !important;" rows="2"><?php echo $_smarty_tpl->tpl_vars['landing_page']->value;?>
</textarea>
							<p><a href="<?php echo $_smarty_tpl->tpl_vars['landing_page']->value;?>
" target="_blank">Landing Page Sample</a></p>
							<p><a href="http://tbm.verifystation.com/lp/?l=6a37e5a906493cf06d2920d6fba087d1" target="_blank">TBM Original Super URL Example</a>

			            </div><!-- end widget content -->
			        </div><!-- end widget div -->
			    </div><!-- end widget -->			    

            </article><!-- END GRID -->
 
        </div><!-- end row -->
 
    </section><!-- end widget grid -->





	





                    
                        
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->

		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white">JPNC | IncentEngine 2000 © 2013-2014</span>
				</div>
			</div>
		</div>		

		<?php echo $_smarty_tpl->getSubTemplate ('admin/includes/footer_scripts.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
        
        
		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();
			
				
			});

		</script>        

	</body>
</html><?php }} ?>
