<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// This is the overloaded version of the Mongo DB library used by ION Auth.  
// It's actually suppose to be using this MongoDB Library:
// https://github.com/huglester/MongoDB-CodeIgniter-Driver/blob/master/libraries/Mongo_db.php

class Mongo_db extends cimongo {
	
    public function __construct() {
            parent::__construct();
    }

    /**
     * Get the documents based upon the passed parameters
     *
     * @since v1.0.0
     */
    public function get($collection = "", $limit = FALSE, $offset = FALSE) {
            if (empty($collection)) {
                    //FIXME theow exception instead show error
                    show_error("In order to retreive documents from MongoDB, a collection name must be passed", 500);
            }
            $cursor = $this->db->selectCollection($collection)->find($this->wheres, $this->selects);
            $cimongo_cursor = new Cimongo_cursor($cursor);

            $this->limit = ($limit !== FALSE && is_numeric($limit)) ? $limit : $this->limit;
            if ($this->limit !== FALSE) {
                    $cimongo_cursor->limit($this->limit);
            }

            $this->offset = ($offset !== FALSE && is_numeric($offset)) ? $offset : $this->offset;
            if ($this->offset !== FALSE) {
                    $cimongo_cursor->skip($this->offset);
            }
            if (!empty($this->sorts) && count($this->sorts) > 0) {
                    $cimongo_cursor->sort($this->sorts);
            }

            $this->_clear();

            return $cimongo_cursor->result_array();
    }


    /**
     *
     * Sets a field to a value
     *
     *  @usage: $this->cimongo->where(array('blog_id'=>123))->set(array('posted'=>1)->update('users');
     *   @since v1.0.0
     */
    public function set($fields = array(), $value='') {
            if (is_array($fields)) {
                    $this->_update_init('$set');
                    foreach ($fields as $field => $value) {
                            $this->updates['$set'][$field] = $value;
                    }
            }
            else
            {
                $this->_update_init('$set');
                $this->updates['$set'][$fields] = $value;

            }
            return $this;
    }

    /**
     *  --------------------------------------------------------------------------------
     *  WHERE PARAMETERS
     *  --------------------------------------------------------------------------------
     *
     *  Get the documents based on these search parameters.  The $wheres array should 
     *  be an associative array with the field as the key and the value as the search
     *  criteria.
     *
     *  @usage = $this->mongo_db->where(array('foo' => 'bar'))->get('foobar');
     */
     
    public function where($wheres = array(), $where_value = '') {
        if (is_array($wheres)) 
        {
            foreach ($wheres as $where => $value) 
            {
                    $this->_where_init($where);
                    $this->wheres[$where] = $value;
            }
        }
        else
        {
            $this->wheres[$wheres] = $where_value;
        }
        return $this;
    }     




}

