<?php if (!defined('BASEPATH')) die();

    /**
     * HasOffersApi Class
     * Documentation: https://github.com/LartTyler/HasOffers-API-Wrapper
     */


    class Has_offers_api_v3_impl  {
        // See config/constants.php for CONSTANTS values
        private $NET_ID = NET_ID;
        private $API_KEY = API_KEY;
        private $API_DOMAIN = API_DOMAIN;
        private $BASE_URL = 'http://api.hasoffers.com/v3/';
        private $WHITE_IPS = array();

        public function getNetworkId() {
            return $this->NET_ID;
        }

        public function getApiKey() {
            return $this->API_KEY;
        }

        public function getApiDomain() {
            return $this->API_DOMAIN;
        }

        public function getKnownWhitelistedIps() {
            return $this->WHITE_IPS;
        }

        public function getBaseUrl() {
            return $this->BASE_URL;
        }

        public function generateRequestHeader($target, $method, $format = 'json', $version = 3) {
            return array(
                'Format' => $format,
                'Method' => $method,
                'Service' => 'HasOffers',
                'Version' => $version,
                'NetworkId' => $this->NET_ID,
                'NetworkToken' => $this->API_KEY,
                'api_key' => $this->API_KEY
            );
        }
    }


    class Has_offers_api extends Has_offers_api_v3_impl {
        private $cache = array();
        private $query = array();
        private $method = '';
        private $target = '';
        private $cache_time_limit = '900';  // Length of time in seconds for caching data
        
        /**
         *	Sets the target resource for the query. This should be an API object, such as Affiliate, AffiliateUser, etc.
         *
         *	@param $target		The target API object
         *	@return		A reference to this object, for chaining calls
         */
        public function setTarget($target) {
            $this->target = $target;

            return $this;
        }

        /**
         *	Sets  the method to call on the query's target. This should be a valid method of the API object selected by setTarget
         *
         *	@param $method		The target method of the selected API object
         *	@return		A reference to this object, for chaining calls
         */
        public function setMethod($method) {
            $this->method = $method;

            return $this;
        }

        /**
         *	Prepares this API instance for a query by flushing the stored query and building a new header set. This is the preferred method of preparing
         *	to execute a query, and should be the first call before addParameter.
         *
         *	@param $target		The target API object
         *	@param $method		The method to call on the target API object
         *	@param	$flushCache		Sets whether or not the flush the cache of previous queries (default: false)
         *	@return		A reference to this object, for chaining calls
         */
        public function prepare($target, $method, $flushCache = false) {
            $this->method = $method;
            $this->target = $target;
            $this->query = array();

            if ($flushCache)
                $this->flushCache();

            $this->buildHeader();

            return $this;
        }
        
        
        /**
         *	Executes one of the hasoffers retrieval methods 
         */
        public function execute($asObject = TRUE, $cacheResult = TRUE, $use_cache_backup = FALSE) 
        {
            $response = $this->execute_post_curl($asObject, TRUE);
            
            //log_message('error', 'HasOffersLOGGING: ' . print_r($response, TRUE) );
            
            
            // ** USE CASE for Error Testing **
            /*            
            $response = array(
                'response' => array(
                    'status' => -1,
                    'httpStatus' => 200,
                    'data' => '',
                    'errors' => array(
                        0 => array(
                            'publicMessage' => 'API usage exceeded rate limit: 52 API calls over 10 second window'
                        )
                    ),
                    'errorMessage' => 'API usage exceeded rate limit: 52 API calls over 10 second window'
                )            
            );            
            */
            
            

            // if a response doesn't exist, then try sending the cached response
            if( !isset($response['response']) )
            {
                $response = '';
                log_message('error', 'HasOffersError !isset($response[response]): no response from the server' );
                if (!empty($this->cache[md5(print_r($this->query, true))]))
                    $response = $this->cache[md5(print_r($this->query, true))];

                $response['response']['data'] = array();
                return $response;
            }
            // If there is an error with hasoffers, then print it to the log
            if($response['response']['status'] == -1) 
            {

                /*
                // Remove password data from login form for logging
                if( isset($response['request']['password']) )
                    $response['request']['password'] = '********';
                if( isset($response['request']['password_confirmation']) )
                    $response['request']['password_confirmation'] = '********';
                if( isset($this->query['password']) )
                    $this->query['password'] = '********';
                    
                // Remove password data from registration form for logging
                if( isset($response['request']['user']['password']) )
                    $response['request']['user']['password'] = '********';
                if( isset($response['request']['user']['password_confirmation']) )
                    $response['request']['user']['password_confirmation'] = '********';
                    
                if( isset($this->query['user']['password']) )
                    $this->query['user']['password'] = '********';
                if( isset($this->query['user']['password_confirmation']) )
                    $this->query['user']['password_confirmation'] = '********';
                
                log_message('error', 'use_cache_backup: ' . $use_cache_backup );
                log_message('error', 'HasOffersError: ' . print_r($response, TRUE) . ' this->query: ' . print_r($this->query, true) );
                */
                
                // Use cache if explicity stated
                if ($use_cache_backup == TRUE && !empty($this->cache[md5(print_r($this->query, true))]))
                   $response = $this->cache[md5(print_r($this->query, true))];
                
                
            }
            
            
            
            
            /*
            // This is the memcached version.  Uses /cache as a backup with files
            $response = $this->execute_post_curl_memcache_backup($asObject, TRUE);
                        
            // If HasOffer is not responsive, attempt to retrieve a cached version            
            if( !isset($response['response']) )
            {
                log_message('error', 'HasOffersError !isset($response[response]): ' . print_r($response, TRUE) );
                
                // If data exists in the cache, then retrieve it
                $CI = &get_instance();
                if ( $CI->cache->get(md5(print_r($this->query, true))) )
                    $response = $CI->cache->get(md5(print_r($this->query, true)));
            }
            
            // If there is an error with hasoffers, then print it to the log
            if($response['response']['status'] == -1) 
            {
                log_message('error', 'HasOffersError: ' . print_r($response, TRUE) );
            }
            */
            
                        
            
            
            //return $this->execute_get($asObject, $cacheResult);    
            //return $this->execute_post($asObject, $cacheResult);
            //return $this->execute_post_memcached($asObject, $cacheResult);
            
            return $response;
            
        }

        /**
         *	Executes the stored query using the GET method
         *
         *	@param $asObject		Sets whether or not to return the result as an object (default) or an associative array
         *	@param $cacheResult		Sets whether or not to store the result in the cache (default: true)
         *	@return		An object or associative array (depending on $asObject) containing the API call response
         */
        public function execute_get($asObject = true, $cacheResult = true) {
            if (!empty($this->cache[md5(print_r($this->query, true))]))
                $result = $this->cache[md5(print_r($this->query, true))];
            else
                $result = @file_get_contents(parent::getBaseUrl() . $this->target . ".json?" . http_build_query($this->query));

            if ($result !== false && !is_object($result)) {
                $result = json_decode($result, !$asObject);

                if ($cacheResult)
                    $this->cache[md5(print_r($this->query, true))] = $result;
            }

            return $result;
        }
        /**
         *	Executes the stored query using the POST method
         *
         *	@param $asObject		Sets whether or not to return the result as an object (default) or an associative array
         *	@param $cacheResult		Sets whether or not to store the result in the cache (default: true)
         *	@return		An object or associative array (depending on $asObject) containing the API call response
         */        
        public function execute_post($asObject = true, $cacheResult = true) {
            if (!empty($this->cache[md5(print_r($this->query, true))]))
                $result = $this->cache[md5(print_r($this->query, true))];
            else {
                $opts = array('http' =>
                    array(
                        'method'  => 'POST',
                        'header'  => 'Content-type: application/x-www-form-urlencoded',
                        'content' => http_build_query($this->query)
                    )
                );
                
                $context  = stream_context_create($opts);                
                
                $result = @file_get_contents(parent::getBaseUrl() . $this->target . ".json", FALSE, $context);
            }

            if ($result !== false && !is_object($result)) {
                $result = json_decode($result, !$asObject);

                if ($cacheResult)
                    $this->cache[md5(print_r($this->query, true))] = $result;
            }
            
            return $result;
        }
        
        
        /**
         *	Executes the stored query using the POST method
         *
         *	@param $asObject		Sets whether or not to return the result as an object (default) or an associative array
         *	@param $cacheResult		Sets whether or not to store the result in the cache (default: true)
         *	@return		An object or associative array (depending on $asObject) containing the API call response
         */        
        public function execute_post_curl($asObject = true, $cacheResult = true) {
            // Load CI object and cURL library
            $CI = &get_instance();
            $CI->load->library('curl');
            
            $result = $CI->curl->simple_get(parent::getBaseUrl() . $this->target . ".json", $this->query);
            
            if ($result !== false && !is_object($result)) {
                $result = json_decode($result, !$asObject);
                
                if ($cacheResult)
                    $this->cache[md5(print_r($this->query, true))] = $result;
            }
            
            return $result;
        
        }
        
        
        /**
         *	Executes the stored query using the POST method
         *
         *	@param $asObject		Sets whether or not to return the result as an object (default) or an associative array
         *	@param $cacheResult		Sets whether or not to store the result in the cache (default: true)
         *	@return		An object or associative array (depending on $asObject) containing the API call response
         */        
        public function execute_post_curl_memcache_backup($asObject = true, $cacheResult = true) {
            // Load CI object and cURL library
            $CI = &get_instance();
            $CI->load->library('curl');
            
            $result = $CI->curl->simple_get(parent::getBaseUrl() . $this->target . ".json", $this->query);
            
            if ($result !== false && !is_object($result)) {
                $result = json_decode($result, !$asObject);
                
                // Load CI object
                
                if(class_exists('Memcache'))
                    $CI->load->driver('cache', array('adapter' => 'memcached', 'backup' => 'file'));
                else
                    $CI->load->driver('cache', array('adapter' => 'file'));   
   
                // Save result into cache for $cache_time_limit seconds
                //if ($cacheResult && !$CI->cache->get(md5(print_r($this->query, true))) )
                if ($cacheResult)
                    $CI->cache->save(md5(print_r($this->query, true)), $result, $this->cache_time_limit);
            }
            
            return $result;
        }        
                        

        /**
         *	Executes the stored query using the POST method
         *
         *	@param $asObject		Sets whether or not to return the result as an object (default) or an associative array
         *	@param $cacheResult		Sets whether or not to store the result in the cache (default: true)
         *	@return		An object or associative array (depending on $asObject) containing the API call response
         */        
        public function execute_post_memcached($asObject = true, $cacheResult = true) 
        {
            // Load CI object
            $CI = &get_instance();

/*
            // load memcached driver
            if($_SERVER['HTTP_HOST'] == 'localhost')
                $CI->load->driver('cache', array('adapter' => 'file'));
            else
                //$CI->load->driver('cache', array('adapter' => 'memcached', 'backup' => 'file'));
                $CI->load->driver('cache', array('adapter' => 'file'));   
                //$CI->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
*/
                
                
            if(class_exists('Memcache'))
                $CI->load->driver('cache', array('adapter' => 'memcached', 'backup' => 'file'));
            else
                $CI->load->driver('cache', array('adapter' => 'file'));   
                
                
/*
            if($CI->cache->memcached->is_supported())
                echo "memcached";
            else if($CI->cache->file->is_supported()) 
                echo "file";
            else if($CI->cache->apc->is_supported()) 
                echo "apc";
            else if($CI->cache->dummy->is_supported()) 
                echo "dummy";
            else
                echo "nothing";
*/                
            //$CI->cache->clean();
            
            // If data exists in the cache, then retrieve it
            if ( $CI->cache->get(md5(print_r($this->query, true))) )
            {
                $result = $CI->cache->get(md5(print_r($this->query, true)));
            }
            else {
                $opts = array('http' =>
                    array(
                        'method'  => 'POST',
                        'header'  => 'Content-type: application/x-www-form-urlencoded',
                        'content' => http_build_query($this->query)
                    )
                );
                
                $context  = stream_context_create($opts);                
                $result = @file_get_contents(parent::getBaseUrl() . $this->target . ".json", FALSE, $context);
            }

            // If data was retrieved from Hasoffers, then decode it
            if ($result !== false && !is_object($result)) {
                
                // Save result into cache for $cache_time_limit seconds
                if ($cacheResult)
                    $CI->cache->save(md5(print_r($this->query, true)), $result, $this->cache_time_limit);
                
                $result = json_decode($result, !$asObject);
            }
            return $result;
        }      

       
        /**
         *	Flushes the query cache.
         *
         *	@return		A reference to this object, for chaining calls
         */
        public function flushCache() {
            $cache = array();
            
            // Load CI object
            $CI = &get_instance();
                        
            // load memcached driver
            if($_SERVER['HTTP_HOST'] == 'localhost')
                $CI->load->driver('cache', array('adapter' => 'file'));
            else
                $CI->load->driver('cache', array('adapter' => 'memcached', 'backup' => 'file'));
                
            // Clean the cache               
            $CI->cache->clean();

            return $this;
        }

        /**
         *	Adds a new argument to the query.
         *
         *	This function can accept two types of parameters, a key and a value, or a key and a numerical array of items that should be below that key.
         *
         *	@param	Varargs		A variable set of arguments that can be adapted to different argument sets
         *	@return		A reference to this object, for chaining calls
         */
        public function addParameter(/* Varargs */) {
            $args = func_get_args();

            switch (sizeof($args)) {
                case 0:
                case 1:
                    throw new Exception('Invalid argument count');

                    break;
                case 2:
                    if (is_string($args[0]))
                        $this->query[$args[0]] = $args[1];
                    else
                        throw new Exception('Keys can only be strings.');

                    break;
                default:
                    if (is_string($args[0])) {
                        $this->query[$args[0]] = array();

                        for ($i = 1; $i < sizeof($args); $i++)
                            $this->query[$args[0]][] = $args[$i];
                    }
            }

            return $this;
        }

        /**
         *	Internal use only.
         */
        public function buildHeader() {
            if (empty($this->query))
                $this->query = parent::generateRequestHeader($this->target, $this->method);
        }
    }
?>