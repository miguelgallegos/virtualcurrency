<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Locker_model extends CI_Model {

    // **********************************************************************
    //  Private Functions
    //  Functions used in this class
    // **********************************************************************

    /**
     * content_locker_v1_get_widget_settings()
     * Get old widget settings
     */
    private function content_locker_v1_get_widget_settings($click_data)
    {
        $user_widget = $this->cimongo->where(array('widget_id' => $click_data['query_string']['w'], 'widget_status' => '1'))->get('clv1_user_widgets')->result_array();

        // widget_settings
        $widget_settings = array();

        if( count($user_widget) > 0)
        {
            $widget_settings = array(
                'affiliate_id'          => $user_widget[0]['affiliate_id'],
                'widget_id'             => $user_widget[0]['widget_id'],
                'widget_status'         => $user_widget[0]['widget_status'],
                'user_ip'               => $click_data['ip_address'],
                'aff_sub3'              => '',
                'aff_sub4'              => 'widget',
//deprecated fields                
//                'title'                 => $user_widget[0]['title_web'],
//                'message'               => $user_widget[0]['message_web'],
                'title'                 => $user_widget[0]['title_mobile'],
                'message'               => $user_widget[0]['message_mobile'],                
                'conversions_required'  => $user_widget[0]['required_conversions'],
                'background_url'        => $user_widget[0]['background_url'],
                'redirect_url'          => $user_widget[0]['redirect_url']
                );
        }
        return $widget_settings;
    }

    /**
     * file_locker_v1_get_widget_settings()
     * Get old widget settings
     */
    private function file_locker_v1_get_widget_settings($click_data)
    {
        $user_widget = $this->cimongo->where(array('lock_id' => $click_data['query_string']['l'], 'lock_status' => '1'))->get('clv1_user_locks')->result_array();

        // widget_settings
        $widget_settings = array();

        if( count($user_widget) > 0)
        {
            $widget_settings = array(
                'affiliate_id'  => $user_widget[0]['affiliate_id'],
                'widget_id'       => $user_widget[0]['lock_id'],
                'lock_name'     => $user_widget[0]['lock_name'],
                'lock_status'   => $user_widget[0]['lock_status'],
                'user_ip'       => $click_data['ip_address'],
                'aff_sub3'      => '',
                'aff_sub4'      => 'widget',
                'file_name'     => $user_widget[0]['file_name'],
                'file_size'     => $user_widget[0]['file_size'],
                'file_uploaded' => $user_widget[0]['file_uploaded'],
                'file_url'      => $user_widget[0]['file_url']
                );
        }

        return $widget_settings;
    }    

    /**
     * super_url_v1_get_widget_settings()
     * Prepare widget settings for Super URL
     */
    private function super_url_v1_get_widget_settings($click_data)
    {
        // Get the affiliate_id from the query string
        $affiliate_id = $this->input->get('a');

        // widget_settings
        $widget_settings = array(
            'affiliate_id'  => $affiliate_id,
            'widget_id'     => 'super-url-' . $affiliate_id,
            'user_ip'       => $click_data['ip_address'],
            'aff_sub3'      => '',
            'aff_sub4'      => 'super-url'
            );

        return $widget_settings;
    }


    /**
     * offer_wall_v1_get_widget_settings()
     * Get old widget settings
     */
    private function offer_wall_v1_get_widget_settings($click_data)
    {
        $user_widget = $this->cimongo->where(array('wall_id' => $click_data['query_string']['w'], 'wall_status' => '1'))->get('clv1_user_walls')->result_array();

        // widget_settings
        $widget_settings = array();

        if( count($user_widget) > 0)
        {
            $widget_settings = array(
                'affiliate_id'          => $user_widget[0]['affiliate_id'],
                'widget_id'             => $user_widget[0]['wall_id'],
                'widget_status'         => $user_widget[0]['wall_status'],
                'user_ip'               => $click_data['ip_address'],
                'aff_sub3'              => '',
                'aff_sub4'              => 'offer_wall'
                );
        }

        return $widget_settings;
    }


    /**
     * landing_page_v1_get_widget_settings()
     * Get old widget settings
     */
    private function landing_page_v1_get_widget_settings($click_data)
    {
        $user_widget = $this->cimongo->where(array('lp_id' => $click_data['query_string']['l'], 'lp_status' => '1'))->get('clv1_user_lps')->result_array();

        // widget_settings
        $widget_settings = array();

        if( count($user_widget) > 0)
        {
            $widget_settings = array(
                'affiliate_id'   => $user_widget[0]['affiliate_id'],
                'lp_id'          => $user_widget[0]['lp_id'],
                'widget_id'      => $user_widget[0]['widget_id'],
                'widget_status'  => $user_widget[0]['lp_status'],
                'user_ip'        => $click_data['ip_address'],
                'aff_sub3'       => '',
                'aff_sub4'       => 'widget',                                
                'lp_button_text' => $user_widget[0]['lp_button_text'],
                'lp_image_name'  => $user_widget[0]['lp_image_name'],
                'lp_image_url'   => $user_widget[0]['lp_image_url'],
                'lp_name'        => $user_widget[0]['lp_name']
                );

            $widget_settings['lp_bgcolor'] = $user_widget[0]['lp_bgcolor'] ? 'background-color: ' . $user_widget[0]['lp_bgcolor'] : '';
            $widget_settings['gotoURL'] = base_url('clv1/content_locker?fn=direct&w=' . $user_widget[0]['widget_id']);
        }

        return $widget_settings;
    }





    /**
     * prepare_offer_feed_links()
     * Format links for use in offer feeds
     */
    private function prepare_offer_feed_links($widget_settings, $get_offers, $referrer)
    {
        $content_locker_output = array();
        foreach($get_offers['offer_feed'] as $offer)
        {
            $offer_link = 'http://' . TRACKING_DOMAIN . '/aff_c?offer_id=' . $offer['offer_id'] .
                        '&aff_id='   . $widget_settings['affiliate_id'] .
                        '&aff_sub='  . $widget_settings['widget_id'] .
                        '&aff_sub2=' . $widget_settings['user_ip'] .
                        '&aff_sub3=' . $referrer .
                        '&aff_sub4=' . $widget_settings['aff_sub4'];

            $content_locker_output[] = array(
                'offer_id'      => $offer['offer_id'], 
                'offer_name'    => $offer['offer_name'],
                'offer_image'   => str_replace(' ', '%20', $offer['offer_image']),
                'offer_link'    => $offer_link
                );
        }

        return $content_locker_output;
    }    

    // **********************************************************************
    //  Public Functions
    //  Functions used in this class
    // **********************************************************************

    public function load_locker($locker_type, $category=FALSE)
    {
        // Load security class
        //$this->load->helper('security');

        // Load models: click_manager and offer_server
        $this->load->model('common/click_manager_model');
        $this->load->model('common/offer_server_model');        

        // Retrieve click profile for user agent
        $click_data = $this->click_manager_model->get_click_profile();

        // Determine which category of offers to show
        if($category === FALSE)
            $category = ($click_data['mobile_detect']['isMobile'] == '1') ? HO_CATEGORY_CONTENT_LOCKER_MOBILE : HO_CATEGORY_CONTENT_LOCKER_WEB;

        // Get Offers based on the $category and the $click_data
        $get_offers = $this->offer_server_model->get_offers($category, $click_data);

        // Check to see if there are offers available.  Redirect to global if no offers are available.
        if(count($get_offers['offer_feed']) == 0)
            redirect(GLOBAL_REDIRECT_URL);

        switch($locker_type)
        {
            default:
            case 'content_locker':
                // Get the function type: [direct, script, onclick]
            	$data['content_locker_fn'] = $this->input->get('fn');

                switch($category)
                {
                    case HO_CATEGORY_CONTENT_LOCKER_MOBILE:

                        // Get Widget settings from the database
                        $widget_settings = $this->content_locker_v1_get_widget_settings($click_data);

                        // If there are no matching widgets, then redirect to global
                        if(count($widget_settings) == 0)
                            redirect(GLOBAL_REDIRECT_URL);

                        // Store in $this->data so that it can be passed to the view
                        $data['widget_settings'] = $widget_settings;

                        // Store referrer in encoded format so it won't be visible to regular visitors
                        $referrer = base64_encode($click_data['agent_referrer']);

                        // Load appropriate locker view
                        $locker_view = 'user/content_locker_v1/mobile_locker';
                        break;

                    default:    
                    case HO_CATEGORY_CONTENT_LOCKER_WEB:
                        // Grab query string data and pass it to the web locker view
                        $click_data['query_string']['aff_sub3'] = base64_encode($click_data['agent_referrer']);

                        // Get Widget settings from the database
                        $widget_settings = $this->content_locker_v1_get_widget_settings($click_data);

                        // If there are no matching widgets, then redirect to global
                        if(count($widget_settings) == 0)
                            redirect(GLOBAL_REDIRECT_URL);                        

                        // Prepare query_string by converting array to a string
                        $data['query_string'] = http_build_query($click_data['query_string']);

                        // Return view and data to the controller for display
                        return array('view' => 'user/content_locker_v1/web_locker', 'data' => $data);
                }
                break;

            case 'clv1_web_overlay':
                // Get Widget settings from the database
                $widget_settings = $this->content_locker_v1_get_widget_settings($click_data);

                // Store widget_settings so it can be passed to the view
                $data['widget_settings'] = $widget_settings;

                // Referrer is already base64 encoded.  This will be passed to the affiliate links
                $referrer = $this->input->get('aff_sub3');

                // Load appropriate locker view
                $locker_view = 'user/content_locker_v1/web_locker_overlay';
                break;

            case 'file_locker':
                // Get Widget settings from the database
                $widget_settings = $this->file_locker_v1_get_widget_settings($click_data);

                // If there are no matching widgets, then redirect to global
                if(count($widget_settings) == 0)
                    redirect(GLOBAL_REDIRECT_URL);                

                // Store referrer in encoded format so it won't be visible to regular visitors
                $referrer = base64_encode($click_data['agent_referrer']);

                // Store settings in $data so it can be passed to the view
                $data['lock_settings'] = $widget_settings;

                // Return view and data to the controller for display
                $locker_view = 'user/content_locker_v1/file_locker';
                break;

            case 'super_url':
                // Get Widget settings from the database
                $widget_settings = $this->super_url_v1_get_widget_settings($click_data);

                // If there are no matching widgets, then redirect to global
                if(count($widget_settings) == 0)
                    redirect(GLOBAL_REDIRECT_URL);

                // Create offer output.  Pass 'aff_sub3' from the query string as the referrer
                $data['offer_feed'] = $this->prepare_offer_feed_links($widget_settings, $get_offers, base64_encode($click_data['agent_referrer']));

                // Redirect to the first offer in the array
                redirect($data['offer_feed'][0]['offer_link']);
                break;

            case 'offer_wall':
                // Get Widget settings from the database
                $widget_settings = $this->offer_wall_v1_get_widget_settings($click_data);

                // If there are no matching widgets, then redirect to global
                if(count($widget_settings) == 0)
                    redirect(GLOBAL_REDIRECT_URL);               

                // Store referrer in encoded format so it won't be visible to regular visitors
                $referrer = base64_encode($click_data['agent_referrer']);

                // Store settings in $data so it can be passed to the view
                $data['wall_settings'] = $widget_settings;

                // Return view and data to the controller for display
                $locker_view = 'user/content_locker_v1/offer_wall';
                break;

            case 'landing_page':
                // Get Widget settings from the database
                $widget_settings = $this->landing_page_v1_get_widget_settings($click_data);

                // If there are no matching widgets, then redirect to global
                if(count($widget_settings) == 0)
                    redirect(GLOBAL_REDIRECT_URL);               

                // Store referrer in encoded format so it won't be visible to regular visitors
                $referrer = base64_encode($click_data['agent_referrer']);

                // Store settings in $data so it can be passed to the view
                $data['lp_settings'] = $widget_settings;

                // Return view and data to the controller for display
                $locker_view = 'user/content_locker_v1/landing_page';
                break;                

        } // end switch()

        // Prepare offer links
        $data['offer_feed'] = $this->prepare_offer_feed_links($widget_settings, $get_offers, $referrer);        

        // Return view and data to the controller for display
        return array('view' => $locker_view, 'data' => $data);                

    }	


    /**
     * retrieve_content_locker_v1()
     * Retrieves user widgets from MySQL and updates MongoDB
     */

    public function retrieve_content_locker_v1_for_check($locker_type){
 // Load Database
        $this->load->database();

        // Retrieve 'user_widgets' table and update in Mongo
        $query_result[$locker_type] = $this->db->get($locker_type)->result_array();

        /*
        // Retrieve 'user_widgets' table and update in Mongo
        $query_result['user_widgets'] = $this->db->get('user_widgets')->result_array();

        // Retrieve 'user_locks' table and update in Mongo
        $query_result['user_locks'] = $this->db->get('user_locks')->result_array();

        // Retrieve 'user_widgets' table and update in Mongo
        $query_result['user_walls'] = $this->db->get('user_walls')->result_array();

        // Retrieve 'user_lps' table and update in Mongo
        $query_result['user_lps'] = $this->db->get('user_lps')->result_array();
        */

        return $query_result;
    }

}