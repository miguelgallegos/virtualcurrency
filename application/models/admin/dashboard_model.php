<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public function get_database_dump()
    {
    	// Retrieve all data from the database
	    $ho_offers = $this->cimongo->get('ho_offers')->result_array();
	    $ho_offer_server = $this->cimongo->get('ho_offer_server')->result_array();
	    $ho_widget_conversions = $this->cimongo->get('ho_widget_conversions')->result_array();

	    // Create array of data
	    $database_collections = array(
	    	'ho_offers' 			=> $ho_offers,
	    	'ho_offer_server' 		=> $ho_offer_server,
	    	'ho_widget_conversions' => $ho_widget_conversions
	    	);

	    return $database_collections;
    }

    private function get_elapsed_time($update_function)
    {
        $this->load->model('common/update_model');
        
        $time_start_update = microtime(true);
        call_user_func( array($this->update_model, $update_function) );
        $time_end_update = microtime(true);

        $time = $time_end_update - $time_start_update;
        return "Time Elapsed - " . $update_function . "(): " . $time . "<br>";
    }    

    public function update()
    {
        $update_time = array();

        $time_start = microtime(true);

        $update_time['update_ho_offers'] = $this->get_elapsed_time('update_ho_offers');
        $update_time['update_ho_widget_conversions'] = $this->get_elapsed_time('update_ho_widget_conversions');
        $update_time['update_ho_offer_server'] = $this->get_elapsed_time('update_ho_offer_server');
        $update_time['retrieve_content_locker_v1'] = $this->get_elapsed_time('retrieve_content_locker_v1');

        $time_end = microtime(true);        
        $time = $time_end - $time_start;

        $update_time['total_time'] = "Time Elapsed (TOTAL): " . $time . "<br>";

        return $update_time;
    }




}