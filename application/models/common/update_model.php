<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Update_model extends CI_Model {

	function __construct() 
	{
		parent::__construct();
		$this->load->library('has_offers_api');
	}


    // **********************************************************************
    //  Private functions 
    //  Functions used in this class
    // **********************************************************************

    public function hasoffers_Offer_findAll()
    {
        $this->has_offers_api->prepare('Offer','findAll');
        
        $this->has_offers_api->addParameter('limit', 10000);
        
        $this->has_offers_api->addParameter('fields', array(
                                                        'id',
                                                        'name',
                                                        'description',
                                                        'preview_url',
                                                        'offer_url',                                                        
                                                        'protocol',
                                                        'status',
                                                        'expiration_date',

                                                        'is_private',
                                                        'conversion_cap',

                                                        'payout_type',
                                                        'default_payout',                                                        
                                                        'revenue_type',
                                                        'max_payout',

                                                        'redirect_offer_id',

                                                        'enforce_geo_targeting',
                                                        'use_target_rules'
                                                        ));
                                                        
        $this->has_offers_api->addParameter('contain', array('Offer', 'Country', 'OfferCategory', 'Advertiser', 'Thumbnail', ) );
        $this->has_offers_api->addParameter('filters', array('status' => array('NOT_EQUAL_TO' => 'deleted')));

        $hasoffers_response = $this->has_offers_api->execute(FALSE, TRUE, TRUE);
        
        return $hasoffers_response;
    }

    private function hasoffers_OfferTargeting_getActiveOfferIdsUsingRule($rule_id)
    {
        $this->has_offers_api->prepare('OfferTargeting','getActiveOfferIdsUsingRule');
        
        $this->has_offers_api->addParameter('limit', 10000);
        $this->has_offers_api->addParameter('rule_id', $rule_id);

        $hasoffers_response = $this->has_offers_api->execute(FALSE, TRUE, TRUE);
        
        return $hasoffers_response;
    }

    private function hasoffers_Conversion_findAll()
    {
        // Time 180 seconds ago
        $time_last = time() - CONVERSIONS_RETRIEVE_TIME;

        $this->has_offers_api->prepare('Conversion','findAll');
        
        $this->has_offers_api->addParameter('fields', array(
                                                        'id',
                                                        'datetime',
                                                        'offer_id',
                                                        'affiliate_info1',
                                                        'affiliate_info2'
                                                        ));

        $this->has_offers_api->addParameter('filters', array(
                                                        'datetime' => array('GREATER_THAN' => date('Y-m-d H:i:s', $time_last) ),
                                                        'affiliate_info4' => 'widget'
                                                        ));
        $this->has_offers_api->addParameter('sort', array('datetime' => 'asc'));
        $this->has_offers_api->addParameter('limit', 1000);

        $hasoffers_response = $this->has_offers_api->execute(FALSE, TRUE, TRUE);
        
        return $hasoffers_response;
    }

    private function hasoffers_Report_getStats_clicks_conversions_cpc()
    {
        $this->has_offers_api->prepare('Report','getStats');

        $this->has_offers_api->addParameter('fields', array('Stat.affiliate_info4', 'Stat.date', 'Stat.clicks', 'Stat.conversions','Stat.cpc', 'Offer.id') );
        $this->has_offers_api->addParameter('groups', array('Offer.name'));

        $today = date("Y-m-d");
        $filters['Stat.date'] = array('conditional' => 'EQUAL_TO', 'values' => $today);
        $filters['Stat.affiliate_info4'] = array('conditional' => 'EQUAL_TO', 'values' => 'widget');
        $this->has_offers_api->addParameter('filters', $filters);

        $this->has_offers_api->addParameter('limit', 10000);

        $hasoffers_response = $this->has_offers_api->execute(FALSE, TRUE, TRUE);
        
        return $hasoffers_response;
    }    


    // **********************************************************************
    //  Public functions 
    //  These functions are used in the controller
    // **********************************************************************


    /**
     * update_ho_offers()
     * Retrieves offer data on Hasoffers and updates the 'ho_offers' collection
     */
    public function update_ho_offers()
    {
        // Retrieve all offers from the network
        $hasoffers_response = $this->hasoffers_Offer_findAll();

        $offers_array = array();
        foreach($hasoffers_response['response']['data']['data'] as $offer)
        {
            $offers_array[$offer['Offer']['id']] = array(
                'offer_details_id'                  => $offer['Offer']['id'],
                'offer_details_name'                => $offer['Offer']['name'],
                'offer_details_description'         => $offer['Offer']['description'],
                'offer_details_preview_url'         => $offer['Offer']['preview_url'],
                'offer_details_offer_url'           => $offer['Offer']['offer_url'],
                'offer_details_conversion_tracking' => $offer['Offer']['protocol'],
                'offer_details_status'              => $offer['Offer']['status'],
                'offer_details_expiration_date'     => $offer['Offer']['expiration_date'],                
                'offer_details_advertiser'          => $offer['Advertiser']['company'],
                'offer_details_categories'          => array(),

                'offer_thumbnail'                   => $offer['Thumbnail']['url'],

                'offer_payout_revenue_type'             => $offer['Offer']['revenue_type'],
                'offer_payout_revenue_per_conversion'   => $offer['Offer']['default_payout'],
                'offer_payout_payout_type'              => $offer['Offer']['payout_type'],
                'offer_payout_cost_per_conversion'      => $offer['Offer']['max_payout'],

                'offer_settings_private'                => $offer['Offer']['is_private'],
                'offer_settings_daily_conversions'      => $offer['Offer']['conversion_cap'],

                'offer_targeting_devices'               => array(),
                'offer_targeting_countries'             => array(),
                'offer_targeting_use_target_rules'      => $offer['Offer']['use_target_rules'],
                'offer_targeting_enforce_geo_targeting' => $offer['Offer']['enforce_geo_targeting'],

                'offer_tracking_redirect_offer'         => $offer['Offer']['redirect_offer_id']
                );

            foreach($offer['OfferCategory'] as $category_index => $category_array)
                $offers_array[$offer['Offer']['id']]['offer_details_categories'][] = $category_array;                

            foreach($offer['Country'] as $country_index => $country_array)
                $offers_array[$offer['Offer']['id']]['offer_targeting_countries'][] = $country_array;   
        }

        // Get Targeting for each Offer and update array
        $offer_targeting_array = array(
            HO_TARGETING_ANDROID   => 'Android',
            HO_TARGETING_IOS       => 'iOS',
            HO_TARGETING_IPHONE    => 'iPhone',
            HO_TARGETING_IPAD      => 'iPad',
            HO_TARGETING_MOBILE    => 'Mobile',
            HO_TARGETING_DESKTOP   => 'Desktop'
            );

        // Query HasOffers API for each type of targeting and get offer_ids for each device
        foreach($offer_targeting_array as $targeting_id => $targeting_name)
        {
            $hasoffers_response = $this->hasoffers_OfferTargeting_getActiveOfferIdsUsingRule( $targeting_id );

            foreach($hasoffers_response['response']['data'] as $offer_id)
            {
                if(array_key_exists($offer_id, $offers_array))
                    $offers_array[$offer_id]['offer_targeting_devices'][] = array('id' => $targeting_id, 'name' => $targeting_name);
            }
        }

        // If offer doesn't exist, insert(), else update()
        foreach($offers_array as $offer)
            $this->cimongo->where(array('offer_details_id' => $offer['offer_details_id']))->update('ho_offers', $offer, array('upsert' => TRUE));      
    }


    /**
     * update_ho_widget_conversions()
     * Retrieves conversion data and updates the 'ho_widget_conversions' collection
     */
    public function update_ho_widget_conversions()
    {
        // Retrieve hasoffers conversions
        $hasoffers_response = $this->hasoffers_Conversion_findAll();

        foreach($hasoffers_response['response']['data']['data'] as $conversion)
        {
            $conversion_document = array(
                'id'        => $conversion['Conversion']['id'],
                'offer_id'  => $conversion['Conversion']['offer_id'],
                'widget_id' => $conversion['Conversion']['affiliate_info1'],
                'user_ip'   => $conversion['Conversion']['affiliate_info2'],
                'timestamp' => $conversion['Conversion']['datetime']
                );
            $this->cimongo->where(array('id' => $conversion['Conversion']['id']))->update('ho_widget_conversions', $conversion_document, array('upsert' => TRUE));
        }

        // Delete Older HasOffers conversions
        $this->delete_ho_conversions();
    }

    /**
     * delete_ho_conversions()
     * Deletes old conversion data
     */
    public function delete_ho_conversions()
    {
        // Get timestamp 24 hours ago
        $date_object = date_create( date("Y-m-d H:i:s", time() - CONVERSIONS_DELETE_TIME) );
        $date_string = date_format( $date_object, 'Y-m-d H:i:s' );


        $query_result = $this->cimongo->where( array('timestamp' => array('$lt' => $date_string) ) )->delete('ho_widget_conversions');
    }


    /**
     * update_ho_offer_server()
     * This manages the pool of offers and daily performance
     */
    public function update_ho_offer_server()
    {
        // Retrieve today's stats: click, conversion and CPC data for each offer
        $hasoffers_response = $this->hasoffers_Report_getStats_clicks_conversions_cpc();

        $offer_stats = array();
        foreach($hasoffers_response['response']['data']['data'] as $offer)
        {
            $offer_stats[$offer['Offer']['id']] = array(
                'offer_id'      => $offer['Offer']['id'],
                'clicks'        => $offer['Stat']['clicks'],
                'conversions'   => $offer['Stat']['conversions'],
                'cpc'           => $offer['Stat']['cpc']
                );
        }

        // Get Offers from the 'ho_offers' collection
        $query_result = $this->cimongo->get('ho_offers');

        foreach( $query_result->result_array() as $offer)
        {
            // Grab offer_id for later use
            $offer_id = $offer['offer_details_id'];

            // Modify offer name to remove everything after the hyphen
            $offer_name_array = explode("-", $offer['offer_details_name']);
            $offer_name = trim($offer_name_array[0]);

            // Prepare document data
            $offer_document = array(
                'offer_id'          => $offer_id,
                'offer_name'        => $offer_name,
                'offer_description' => $offer['offer_details_description'],
                'offer_image'       => $offer['offer_thumbnail'],
                'offer_categories'  => $offer['offer_details_categories'],
                'offer_cap'         => $offer['offer_settings_daily_conversions'],
                'offer_status'      => $offer['offer_details_status'],
                'offer_targeting'   => $offer['offer_targeting_devices'],                
                );

            // If offer_id exists in $offer_stats, then add stats data
            if(array_key_exists($offer_id, $offer_stats))
            {
                $offer_document['offer_clicks'] = $offer_stats[$offer_id]['clicks'];
                $offer_document['offer_conversions'] = $offer_stats[$offer_id]['conversions'];
                $offer_document['offer_cpc'] = $offer_stats[$offer_id]['cpc'];
            }
            else
            {
                $offer_document['offer_clicks'] = 0;
                $offer_document['offer_conversions'] = 0;
                $offer_document['offer_cpc'] = 0;
            }

            // Grab country code only
            $countries = array();
            foreach($offer['offer_targeting_countries'] as $country)
                $countries[] = $country['code'];

            $offer_document['offer_countries'] = $countries;

            // Update/Insert into the 'ho_offer_server' collections
            $this->cimongo->where(array('offer_id' => $offer_id))->update('ho_offer_server', $offer_document, array('upsert' => TRUE));
        }
    }


    /**
     * retrieve_content_locker_v1()
     * Retrieves user widgets from MySQL and updates MongoDB
     */
    public function retrieve_content_locker_v1()
    {
        // Load Database
        $this->load->database();

        // Retrieve 'user_widgets' table and update in Mongo
        $query_result = $this->db->get('user_widgets')->result_array();

        foreach($query_result as $row)
            $this->cimongo->where(array('widget_id' => $row['widget_id']))->update('clv1_user_widgets', $row, array('upsert' => TRUE, 'multiple' => TRUE));


        // Retrieve 'user_locks' table and update in Mongo
        $query_result = $this->db->get('user_locks')->result_array();

        foreach($query_result as $row)
            $this->cimongo->where(array('lock_id' => $row['lock_id']))->update('clv1_user_locks', $row, array('upsert' => TRUE, 'multiple' => TRUE));


        // Retrieve 'user_widgets' table and update in Mongo
        $query_result = $this->db->get('user_walls')->result_array();

        foreach($query_result as $row)
            $this->cimongo->where(array('wall_id' => $row['wall_id']))->update('clv1_user_walls', $row, array('upsert' => TRUE, 'multiple' => TRUE));


        // Retrieve 'user_lps' table and update in Mongo
        $query_result = $this->db->get('user_lps')->result_array();

        foreach($query_result as $row)
            $this->cimongo->where(array('lp_id' => $row['lp_id']))->update('clv1_user_lps', $row, array('upsert' => TRUE, 'multiple' => TRUE));                            

        return $query_result;
    }    


}

