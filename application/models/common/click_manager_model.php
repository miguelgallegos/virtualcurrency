<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Click_manager_model extends CI_Model {

    // **********************************************************************
    //  Private functions 
    //  Functions used in this class
    // **********************************************************************

    /**
     * get_mobile_detect()
     * Retrieve mobile data for offer
     */
    private function get_mobile_detect()
    {
        $this->load->library('Mobile_Detect');
        $this->detect = new Mobile_Detect();

        $device = array();

        // Mobile or Desktop
        $device['isMobile'] = $this->detect->isMobile() ? '1' : '0';
        $device['isTablet'] = $this->detect->isTablet() ? '1' : '0';

        // Any mobile device (phones or tablets).
        if ( $this->detect->isMobile() || $this->detect->isTablet() ) 
        	$device[HO_TARGETING_MOBILE] = 'HO_TARGETING_MOBILE';
        
        if ( !$this->detect->isMobile() && !$this->detect->isTablet() )
        	$device[HO_TARGETING_DESKTOP] = 'HO_TARGETING_DESKTOP';
        
        // Check for a specific platform with the help of the magic methods:
        if( $this->detect->isiOS() )
        	$device[HO_TARGETING_IOS] = 'HO_TARGETING_IOS';

        if( $this->detect->isiPhone() )
        	$device[HO_TARGETING_IPHONE] = 'HO_TARGETING_IPHONE';

        if( $this->detect->isiPad() )
        	$device[HO_TARGETING_IPAD] = 'HO_TARGETING_IPAD';

		if( $this->detect->isAndroidOS() )
			$device[HO_TARGETING_ANDROID] = 'HO_TARGETING_ANDROID';

        return $device;
    }

    /**
     * get_geoip()
     * Get GeoIP data
     */
    private function get_geoip()
    {
        $this->load->library('geoip_lib');

        if($this->geoip_lib->InfoIP())
            $output = $this->geoip_lib->result_array();
        else
            $output = array();

        return $output;
    }

    // **********************************************************************
    //  Public Functions
    //  Functions used in this class
    // **********************************************************************

    /**
     * get_click_profile()
     * Retrieve offers based on parameters
     */
	public function get_click_profile()
	{
		// Retrieve Device Detection Array
		$mobile_detect_array = $this->get_mobile_detect();

		// Retrieve GeoIP Data
		$geoip_array = $this->get_geoip();

		// User Agent Data
		$this->load->library('user_agent');
		$this->load->helper('date');

		$click_array = array(
			'timestamp' 		=> now(),
			'ip_address' 		=> $this->input->ip_address(),
			'agent_platform'	=> $this->agent->platform,
			'agent_browser'		=> $this->agent->browser,
			'agent_mobile'		=> $this->agent->mobile,
			'agent_referrer' 	=> $this->agent->referrer(),			
			'agent_UA' 			=> $this->agent->agent_string(),
			'geoip'				=> $geoip_array,
			'mobile_detect' 	=> $mobile_detect_array,
            'query_string'      => $this->input->get()
			);

		return $click_array;
	}

}