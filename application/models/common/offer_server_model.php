<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offer_server_model extends CI_Model {


    // **********************************************************************
    //  Private functions 
    //  Functions used in this class
    // **********************************************************************

	public function get_offer_by_id($id){

		// Filter Offer data
		$where_clause = array('offer_id' => $id);

		// Query Database for offers		
		return $this->cimongo->where($where_clause, TRUE)->get('ho_offer_server')->result_array();

	}


    /**
     * get_offer_feed()
     * Retrieve offers based on parameters
     */    
	public function get_offer_feed($category=FALSE, $country_code=FALSE, $device=FALSE, $status=FALSE)
	{
		// Filter Offer data
		$where_clause = array();

		if($category != FALSE)
			$where_clause['offer_categories.id'] = $category;

		if($country_code != FALSE)
			$where_clause['offer_countries'] = $country_code;

		if($device != FALSE)
		{
			//$where_clause['offer_targeting.id'] = $device;
			$where_clause['offer_targeting.id'] = array('$in' => $device);
		}

		if($status != FALSE)
			$where_clause['offer_status'] = $status;

		// Query Database for offers
		$offer_feed = $this->cimongo->where($where_clause, TRUE)->order_by(array('offer_cpc' => 'DESC'))->get('ho_offer_server')->result_array();

		return $offer_feed;
	}


    // **********************************************************************
    //  Public Functions
    //  Functions used in this class
    // **********************************************************************

    /**
     * get_offer_feed()
     * Retrieve offers based on parameters
     */
	public function get_offers($category, $click_data)
	{
		// Prepare device data
		$device = FALSE;
		foreach($click_data['mobile_detect'] as $index => $value)
			$device[] = $index;

		// Country Code
		$country_code = $click_data['geoip']['country_code'];

		// Get Offers from database
		$offer_feed = $this->get_offer_feed($category, $country_code, $device, 'active');

		return array('click_data' => $click_data, 'offer_feed' => $offer_feed);
	}

}
