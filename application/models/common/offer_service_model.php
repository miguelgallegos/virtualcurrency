<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offer_service_model extends CI_Model {

	public function __construct(){

		$this->load->model('common/click_manager_model');

	}


    // **********************************************************************
    //  Private functions 
    //  Functions used in this class
    // **********************************************************************

	//$demo_offers = array(array('offer_id' => '99', 'offer_name' => 'Mock Offer 1', 'offer_image' => 'https://media.go2speed.org/brand/files/theblumarket/620/thumbnails_100/download.jpg'));

	public function get_offer_by_id($id){

		// Filter Offer data
		//make mock up meanwhile
		//return from CL2 -> API	
		//make LIB to connect to CL2 API!
		//implement login		

		//PASS OVER CLICK DATA!!!

		$os = new Offers_Service_api();
		$os->prepare('offers', 'findById');
		$os->addParameter('id', $id);
		$os->addParameter('click_data', $this->click_manager_model->get_click_profile());
		$res = $os->execute(false, false);
		
		//TODO: check if data exists or status 200
		return $res['response']['data'];
	}

    // **********************************************************************
    //  Public Functions
    //  Functions used in this class
    // **********************************************************************

    /**
     * get_offer_feed()
     * Retrieve offers based on parameters
     */
	public function get_offers($remove_my_offers = true)
	{

		$os = new Offers_Service_api();
		$os->prepare('offers', 'findAll');
		$os->addParameter('click_data', $this->click_manager_model->get_click_profile());
		$res = $os->execute(false, false);

		if(!$remove_my_offers){
			return $res['response']['data'];
		}

		//filter out my offers --- must filter from the service so needs to be passed to mongodb
		$newOffers = array();
		$this->load->model('common/bank_model');
		$myOffers = $this->bank_model->getCreditedOffers();

		foreach ($res['response']['data'] as $offer) {
			$addMe = true;
			foreach ($myOffers as $myOfferData) {
				//if found dont add
				if($offer['offer_id'] == $myOfferData['item_id']){ 
					$addMe = false;
				}
			}

			if($addMe){
				$newOffers[] = $offer;
			}
		}

		return $newOffers;
	}

}
