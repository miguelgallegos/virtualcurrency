<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rewards_model extends CI_Model {


	public function __construct(){
	}

	//returns total points
	public function get(){
		//get local from a json provider
		return $this->rewardsProvider();
	}

	public function getById($id){
		return $this->rewardsProvider($id);
	}

    // **********************************************************************
    //  Private functions 
    //  Functions used in this class
    // **********************************************************************
	private function rewardsProvider($id = null){

		$r = array(
				array("id" => 1, 'name' => '$1 Amazon.com Gift Card', 'image' => 'http://placehold.it/250/fff000&text=AG', 'points' => 1200, 'url' => 'http://www.r1.com/'),
				array('id' => 2, 'name' => 'Infinity Blade III', 'image' => 'http://placehold.it/250/ffaa11&text=IB', 'points' => 900, 'url' => 'http://www.r2.com/'),
				array('id' => 3, 'name' => '$5 Starbucks', 'image' => 'http://placehold.it/250/00f010&text=SB', 'points' => 875, 'url' => 'http://www.r3.com/'),
				array('id' => 4, 'name' => '$10 Google Play', 'image' => 'http://placehold.it/250/00f0FA&text=GP', 'points' => 800, 'url' => 'http://www.r4.com/'),
				array('id' => 5, 'name' => 'Happy Salutation', 'image' => 'http://placehold.it/250/AAf0F0&text=HAPPY', 'points' => 10, 'url' => 'http://www.r5.com/'),
			);

		//by id
		if(!is_null($id)){
			foreach ($r as $r1) {
				if($r1['id'] == $id){
					return $r1;
				}
			}
		}

		//all
		return $r;

	}


}
