<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bank_model extends CI_Model {

	private $credit;
	private $affiliate_id; //user account id

	public function __construct(){
		//TEST 
		$this->affiliate_id = '3212';

	}

	//returns total points
	public function credit(){

		$balance = $this->cimongo->aggregate('bank', 
			array(
				array('$match' => array('affiliate_id' => $this->affiliate_id)),
				array('$group' => array('_id' => array('affiliate_id' => $this->affiliate_id, 'type' => '$type'), 'balance' => array('$sum' => '$points'))),
				)
		);

	// TODO: do it in single operation!
    //{$match:{ affiliate_id : "3212"}},		
    //{$group: {_id: {affiliate_id: "$affiliate_id", type: '$type'}, balance: {$sum: "$points"}}}        

		$credit = $balance['result'][1]['balance'] - $balance['result'][0]['balance'];
		return number_format($credit);
	}

	public function addTransaction($type, $points, $item_id, $widget_id){ //type: credit | debit, points: money value (int for now), widget_id: (download_id|widget_id), item_id: offer_id|reward_id
		//write mongodb transaction
		//verify offers are not stored twice
		//ADD OFFER ID or REWARD ID and check with double key: aff_id + [offer_id|reward_id]
		$data = array(
			'type' => $type,
			'points' => $points,
			'item_id' => $item_id,
			'widget_id' => $widget_id, //for rewards, auto generate an ID or 64encode time stamp
			'affiliate_id' => $this->affiliate_id
			);

/*
		//debit default
		$where = array();
		//credit		
		if($type == 'credit'){
			$where = array('item_id' => $item_id, 'affiliate_id' => $this->affiliate_id, 'type' => $type);	
		}
*/		


		$this->cimongo->insert('bank', $data, array('upsert'=> true));
	}

	//this should help to filter out to display available offers
	public function getCreditedOffers(){
		$where = array('affiliate_id' => $this->affiliate_id, 'type' => 'credit');
		return $this->cimongo->where($where)->get('bank')->result_array();
	}

    // **********************************************************************
    //  Private functions 
    //  Functions used in this class
    // **********************************************************************

}
