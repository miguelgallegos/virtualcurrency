<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

class MY_Input extends CI_Input 
{
    
    // This is the extension class for getting the real visitor IP from cloudflare
	function ip_address()
	{
		$ip = parent::ip_address();
		$proxy = $this->get_request_header('X-Forwarded-For');
        $cloudflare_ip = $this->get_request_header('Cf-Connecting-Ip');

        // If cloudflare contains a 'Cf-connecting-ip' in the headers, then use that as the IP address        
        if($cloudflare_ip)
            return $cloudflare_ip;

        // Otherwise, check current ip_address if it's on the cloudflare white list
		if($proxy) 
		{
			$iplong = ip2long($ip);
 
			// list of IP => mask
            // https://www.cloudflare.com/ips-v4
			$whitelist = array(
                '204.93.240.0' => 24,
                '204.93.177.0' => 24,
                '199.27.128.0' => 21,
                '173.245.48.0' => 20,
                '103.21.244.0' => 22,
                '103.22.200.0' => 22,
                '103.31.4.0' => 22,
                '141.101.64.0' => 18,
                '108.162.192.0' => 18,
                '190.93.240.0' => 20,
                '188.114.96.0' => 20,
                '197.234.240.0' => 22,
                '198.41.128.0' => 17,
                '162.158.0.0' => 15
			);
 
			foreach($whitelist as $whiteip => $mask)
			{
				$bitmask = bindec(str_repeat('1', $mask) . str_repeat('0', 32 - $mask));
				$trusted = ip2long($whiteip);
 
				if(($iplong & $bitmask) == $trusted)
				{
					$ip = $proxy;
					break; 
				}
			}
		}
		return $ip;
	}
}