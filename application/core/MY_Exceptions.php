<?php 

class MY_Exceptions extends CI_Exceptions {
    public function __construct() {
        parent::__construct();
    }
    public function show_error($heading, $message, $template = 'error_general', $status_code = 500) 
    {
        //if(ENVIRONMENT == 'development' || ENVIRONMENT == 'testing') 
        if( strpos($_SERVER['HTTP_HOST'], 'local') !== FALSE )            
        {
            try {
                $str = parent::show_error($heading, $message, $template = 'error_general', $status_code = 500);
                throw new Exception($str);
            } catch (Exception $e) {
                $msg = $e->getMessage();
                log_message("error", "show_error($heading, $message, $template, $status_code)" );
                log_message("error", "Backtrace:\n" . $e->getTraceAsString());
                
                $trace = "<h1>Call Trace</h1><pre>". $e->getTraceAsString(). "<pre>";
                
                //append our stack trace to the error message
                $err = str_replace('</div>', $trace.'</div>', $msg);
                
                echo $err;
            }
        } 
        else 
        {
            
            try {
                $str = parent::show_error($heading, $message, $template = 'error_general', $status_code = 500);
                throw new Exception($str);
            } catch (Exception $e) {
                
                $msg = $e->getMessage();
                
                log_message("error", "show_error($heading, $message, $template, $status_code)" );
                log_message("error", "Backtrace:\n" . $e->getTraceAsString());
                
                //echo $msg;
                
                redirect();
            }
        }
    }
    
    
    public function show_php_error($severity, $message, $filepath, $line)
    {	
        //if(ENVIRONMENT == 'development' || ENVIRONMENT == 'testing') 
        if( strpos($_SERVER['HTTP_HOST'], 'local') !== FALSE )            
        {
            parent::show_php_error($severity, $message, $filepath, $line);
        }
        /*
        else if(ENVIRONMENT == 'production')
        {        
        	$severity = ( ! isset($this->levels[$severity])) ? $severity : $this->levels[$severity];
        	$filepath = str_replace("\\", "/", $filepath);
        	
        	// For safety reasons we do not show the full file path
        	if (FALSE !== strpos($filepath, '/'))
        	{
                $x = explode('/', $filepath);
        	    $filepath = $x[count($x)-2].'/'.end($x);
        	}
        		
        	if (ob_get_level() > $this->ob_level + 1)
        	    ob_end_flush();	
        	ob_start();
        	include(APPPATH.'errors/error_php'.EXT);
        	$buffer = ob_get_contents();
        	ob_end_clean();
        		
        	$msg = 'Severity: '.$severity.'  --> '.$message. ' '.$filepath.' '.$line;
     
            log_message('error', 'MY_Exceptions.php->show_php_error(): ' .  $msg , TRUE);
            
            //mail('dev-mail@example.com', 'An Error Occurred', $msg, 'From: test@example.com');            
        }
        */
 
	    	
	
    }
     
    
    
}