<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
   function __construct()
   {
        parent::__construct();

        // Display the profiler
        $this->display_profiler();
   }


   private function display_profiler($test_param=FALSE)
   {
        // If this is on the local server, set testing to TRUE
        //if($_SERVER['HTTP_HOST'] == 'localhost')
        if( strpos($_SERVER['HTTP_HOST'], 'local') !== FALSE )            
            $testing = TRUE;
        else
            $testing = FALSE;

      
        // To load the CI benchmark and memory usage profiler, set to TRUE
        if ($testing || $test_param) 
        {
            $sections = array(
                'benchmarks' => TRUE, 'memory_usage' => TRUE, 
                'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
                'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => TRUE
            ); 
            //$this->output->set_profiler_sections($sections);
            $this->output->enable_profiler(TRUE);
        }      

        // Define a global variable to store data that is then used by the end view page.
        $this->data = null;
   }

   protected function admin_template()
   {


    
   }




}
