	<ol class="breadcrumb">
		<?php if($current_page == 'dashboard'): ?>
			<li>Home</li><li>Dashboard</li>
		<?php endif; ?>

		<?php if($current_page == 'manage_offers'): ?>
			<li>Home</li><li>Manage Offers</li>
		<?php endif; ?>

        <?php if($current_page == 'manage_user_accounts'): ?>
            <li>Home</li><li>Manage Users</li><li>User Accounts</li>
        <?php endif; ?>

        <?php if($current_page == 'manage_user_accounts_create'): ?>
            <li>Home</li><li>Manage Users</li><li>User Accounts</li><li>Create</li>
        <?php endif; ?>

        <?php if($current_page == 'manage_user_accounts_edit'): ?>
            <li>Home</li><li>Manage Users</li><li>User Accounts</li><li>Edit</li>
        <?php endif; ?>        

        <?php if($current_page == 'manage_user_groups'): ?>
            <li>Home</li><li>Manage Users</li><li>User Groups</li>
        <?php endif; ?>

        <?php if($current_page == 'manage_user_groups_create'): ?>
            <li>Home</li><li>Manage Users</li><li>User Groups</li><li>Create</li>
        <?php endif; ?>

        <?php if($current_page == 'manage_user_groups_edit'): ?>
            <li>Home</li><li>Manage Users</li><li>User Groups</li><li>Edit</li>
        <?php endif; ?>

		<?php if($current_page == 'stats'): ?>
			<li>Home</li><li>Statistics</li>
		<?php endif; ?>				

        <?php if($current_page == 'sync_tool'): ?>
            <li>Home</li><li>Sync Tool</li>
        <?php endif; ?>             

		<?php if($current_page == 'update'): ?>
			<li>Home</li><li>Update Database</li>
		<?php endif; ?>
	</ol>
