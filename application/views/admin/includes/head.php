    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    
    <title> IncentEngine 2000 | <?= ucfirst(str_replace('_', ' ', $current_page) ) ?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    
    <!-- Use the correct meta names below for your web application
    	 Ref: http://davidbcalhoun.com/2010/viewport-metatag 
    	 
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">-->
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
    <!-- Basic Styles -->    
    <link rel="stylesheet" type="text/css" media="screen" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    
    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?= base_url('assets/css/smartadmin-production.css') ?>">
    <link rel="stylesheet" type="text/css" media="screen" href="<?= base_url('assets/css/smartadmin-skins.css') ?>">
    
    
    <!--  Custom styles will also ensure you retrain your customization with each SmartAdmin update. -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?= base_url('assets/css/custom.css') ?>">
    
    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <!-- <link rel="stylesheet" type="text/css" media="screen" href="css/demo.css"> -->
    
    <!-- FAVICONS -->
    <link href="<?= base_url('assets/img/favicon/favicon.ico') ?>" type="image/x-icon" rel="shortcut icon" >
    <link href="<?= base_url('assets/img/favicon/favicon.ico') ?>" type="image/x-icon" rel="icon" >

    <!-- GOOGLE FONT -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

    <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script>
        if (!window.jQuery) {
            document.write('<script src="js/libs/jquery-2.0.2.min.js"><\/script>');
        }
    </script>

    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script>
        if (!window.jQuery.ui) {
            document.write('<script src="js/libs/jquery-ui-1.10.3.min.js"><\/script>');
        }
    </script>    


    