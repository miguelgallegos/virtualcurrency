		<!--================================================== -->

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events
		<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

		<!-- BOOTSTRAP JS -->
        
        
		<!-- <script src="js/bootstrap/bootstrap.min.js"></script>-->
        <script src="<?= base_url('assets/js/bootstrap/bootstrap.min.js') ?>"></script>

		<!-- browser msie issue fix -->
		<script src="<?= base_url('assets/js/plugin/msie-fix/jquery.mb.browser.min.js') ?>"></script>

		<!-- FastClick: For mobile devices -->
		<script src="<?= base_url('assets/js/plugin/fastclick/fastclick.js') ?>"></script>

		<!--[if IE 7]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->

		<!-- MAIN APP JS FILE -->
		<script src="<?= base_url('assets/js/app.js') ?>"></script>
		
