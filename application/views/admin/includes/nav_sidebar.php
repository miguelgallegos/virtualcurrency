    <!-- Note: This width of the aside area can be adjusted through LESS variables -->
    <aside id="left-panel">
    
    	<!-- User info -->
    	<div class="login-info"></div>
    	<!-- end user info -->
    	<nav>
    		<ul>
    			<li <?php if($current_page == 'dashboard'): ?> class="active" <?php endif; ?>>
    				<a href="<?= base_url('admin/dashboard') ?>"><i class="fa fa-lg fa-fw fa-tachometer"></i><span class="menu-item-parent">Dashboard</span></a>
    			</li>

                <li <?php if( strstr($current_page, "manage_user") ): ?> class="active open" <?php endif; ?>>
                    <a href="#"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">Manage Users</span></a>
                    <ul>
                        <li <?php if( strstr($current_page, 'manage_user_accounts') ): ?> class="active" <?php endif; ?>>
                            <a href="<?= base_url('admin/manage_users/accounts') ?>">User Accounts</a>
                        </li>

                        <li <?php if( strstr($current_page, 'manage_user_groups') ): ?> class="active" <?php endif; ?>>
                            <a href="<?= base_url('admin/manage_users/groups') ?>">User Groups</a>
                        </li>
                    </ul>
                </li>

    			<li <?php if($current_page == 'manage_offers'): ?> class="active" <?php endif; ?>>
    				<a href="<?= base_url('admin/manage_offers') ?>"><i class="fa fa-lg fa-fw fa-pencil"></i><span class="menu-item-parent">Manage Offers</span></a>
    			</li>

    			<li <?php if($current_page == 'stats'): ?> class="active" <?php endif; ?>>
    				<a href="<?= base_url('admin/stats') ?>"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i><span class="menu-item-parent">Statistics</span></a>
    			</li>

                <li <?php if($current_page == 'sync_tool'): ?> class="active" <?php endif; ?>>
                    <a href="<?= base_url('admin/sync_tool') ?>"><i class="fa fa-lg fa-bolt"></i><span class="menu-item-parent">Sync Verifier</span></a>
                </li>

                <li <?php if($current_page == 'update'): ?> class="active" <?php endif; ?>>
                    <a href="<?= base_url('admin/update') ?>"><i class="fa fa-lg fa-fw fa-cloud-upload"></i><span class="menu-item-parent">Update Database</span></a>
                </li>
            </ul>
    	</nav>
    
    </aside>
