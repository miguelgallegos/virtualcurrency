<!DOCTYPE html>
<html lang="en-us">
	<head>	
		<?= $admin_view_head ?>
    </head>
    
	<body class="smart-style-1 fixed-header">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
        <!-- possible classes: smart-style-0, smart-style-1, smart-style-2, smart-style-3, -->

		<!-- HEADER -->
	    <header id="header">
	        <div id="logo-group">
	            <!-- PLACE YOUR LOGO HERE -->	            
	            <?= $admin_view_logo ?>
	            <!-- END LOGO PLACEHOLDER -->       
	        </div>
	    	
	    	<!-- pulled right: nav area -->
	    	<div class="pull-right">
	    
	    		<!-- logout button -->
	    		<div id="logout" class="btn-header transparent pull-right">
	    			<span> <a href="<?= base_url('logout') ?>" title="Sign Out"><i class="fa fa-sign-out"></i></a> </span>
	    		</div>
	    		<!-- end logout button -->
	    
	    	</div>
	    	<!-- end pulled right: nav area -->
	    
	    </header>

		<!-- END HEADER -->

		<!-- Left panel : NAVIGATION AREA -->				
		<?= $admin_view_nav_sidebar ?>
        <!-- END NAVIGATION -->

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">
				<!-- breadcrumb -->
					<?= $admin_view_nav_breadcrumbs ?>
				<!-- end breadcrumb -->
			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<?= $content ?>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->

		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white">IncentEngine 2000 &copy; 2013-2014</span>
				</div>
			</div>
		</div>		

		<?= $admin_view_footer_scripts ?>
        
		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();
			
				
			});

		</script>        

	</body>
</html>