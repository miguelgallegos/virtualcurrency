<!DOCTYPE html>
<html lang="en-us">
	<head>
		<?= $admin_view_head ?>
	</head>

	<body id="login" class="animated fadeInDown">
		<header id="header" style="background-color: #22262e !important; background-image: none;">
	    	<div id="logo-group">
				<?= $admin_view_logo ?>
			</div>
		</header>

		<div id="main" role="main">

			<!-- MAIN CONTENT -->
			<div id="content" class="container">
				<div class="row">
					<?= $content ?>
				</div>
			</div>

		</div>
        <?= $admin_view_footer_scripts ?>
        
		<!-- JQUERY VALIDATE -->
		<script src="<?= base_url('assets/js/plugin/jquery-validate/jquery.validate.min.js') ?>"></script>

		<script type="text/javascript">
			runAllForms();

			$(function() {
				// Validation
				$("#login-form").validate({
					// Rules for form validation
					rules : {
						email : {
							required : true,
							email : true
						},
						password : {
							required : true,
							minlength : 3,
							maxlength : 20
						}
					},

					// Messages for form validation
					messages : {
						email : {
							required : 'Please enter your email address',
							email : 'Please enter a VALID email address'
						},
						password : {
							required : 'Please enter your password'
						}
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
			});
		</script>

	</body>
</html>