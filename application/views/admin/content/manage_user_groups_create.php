<section id="widget-grid">
        <!-- row -->
        <div class="row">
            <!-- SINGLE GRID -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0">
			 
			        <header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2><?= lang('groups_heading') ?></h2>
			        </header><!-- widget div-->
			 
			        <div>
			            <!-- widget edit box -->
			            <div class="jarviswidget-editbox">
			                <!-- This area used as dropdown edit box -->
			                <input class="form-control" type="text">
			            </div><!-- end widget edit box -->
			 
			            <!-- widget content -->
			            <div class="widget-body">

							<?= !empty($message) ? $message : '' ?>

							<h1><?= lang('create_group_heading') ?></h1>
							<p><?= lang('create_group_subheading') ?></p>

							<?= form_open("admin/manage_users/create_group/", array('class' => 'smart-form') ) ?>							

								<fieldset>
									<div class="row">
										<section class="col col-6">
											<label class="label"><?= lang('create_group_name_label', 'group_name') ?></label>
											<label class="input">
												<input type="<?= $group_name['type'] ?>" name="<?= $group_name['name'] ?>" id="<?= $group_name['id'] ?>" value="<?= $group_name['value'] ?>">
											</label>
										</section>
									</div>

									<div class="row">
										<section class="col col-6">
											<label class="label"><?= lang('create_group_desc_label', 'description') ?></label>
											<label class="input">
												<input type="<?= $description['type'] ?>" name="<?= $description['name'] ?>" id="<?= $description['id'] ?>" value="<?= $description['value'] ?>">				
											</label>
										</section>
									</div>

								</fieldset>									

								<footer>
									<button type="submit" class="btn btn-primary" >
										<?= lang('create_group_submit_btn') ?>
									</button>
								</footer>									

							<?= form_close() ?>

			 
			            </div><!-- end widget content -->
			        </div><!-- end widget div -->
			    </div><!-- end widget -->
 
            </article><!-- END GRID -->
           
 
        </div><!-- end row -->
 
    </section><!-- end widget grid -->
