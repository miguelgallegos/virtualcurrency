<script>
	var site_url = '<?php echo base_url()?>';	
</script>
<script src="<?php echo base_url('assets/js/ajax_interact.js')?>"></script>

<div class="well">
		<div class="input-group">
			<input class="form-control" type="text" placeholder="Search Widget by ID...">
			<div class="input-group-btn">
				<button class="btn btn-default btn-primary" type="button">
					<i class="fa fa-search"></i> Search Widget
				</button>
			</div>
		</div>
	</div>

<div class="row">
	<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

		<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0">
	 
	        <header>
				<span class="widget-icon"> <i class="fa fa-cog"></i> </span>
	            <h2>MySQL Widgets</h2>
	        </header><!-- widget div-->
	 
	        <div>
	            <!-- widget edit box -->
	            <div class="jarviswidget-editbox">
	                <!-- This area used as dropdown edit box -->
	                <input class="form-control" type="text">
	            </div><!-- end widget edit box -->
	 
	            <!-- widget content -->
	            <div class="widget-body">

	            	<?php $keys = array('user_locks' => 'lock_id', 'user_widgets' => 'widget_id', 'user_walls' => 'wall_id', 'user_lps' => 'lp_id'); ?>
	            	<?php foreach($v1_lockers as $locker_type => $locker_types){?>
	            		<h3><?php echo $locker_type ?></h3>
	            		<table class="table table-bordered">
	            	<?php 	foreach($locker_types as $locker){?>								
								<tbody>
									<!-- new tr -->
									<tr>
										<td>
										<p>
											<?php echo $locker[$keys[$locker_type]] ?>
										</p>
										</td>
									</tr>
								</tbody>								
	            	<?php 	} ?>
	            		</table>	
	            	<?php } ?>

	            </div><!-- end widget content -->
	        </div><!-- end widget div -->
	    </div><!-- end widget -->
    </article>	

	<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0">
	 
	        <header>
				<span class="widget-icon"> <i class="fa fa-cog"></i> </span>
	            <h2>MongoDB Widgets</h2>
	        </header><!-- widget div-->
	 
	        <div>
	            <!-- widget edit box -->
	            <div class="jarviswidget-editbox">
	                <!-- This area used as dropdown edit box -->
	                <input class="form-control" type="text">
	            </div><!-- end widget edit box -->
	 
	            <!-- widget content -->
	            <div class="widget-body">
								 
	            </div><!-- end widget content -->
	        </div><!-- end widget div -->
	    </div><!-- end widget -->
    </article>    
</div>

<div class="row">
	<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

		<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0">
	 
	        <header>
				<span class="widget-icon"> <i class="fa fa-cog"></i> </span>
	            <h2>Results</h2>
	        </header><!-- widget div-->
	 
	        <div>
	            <!-- widget edit box -->
	            <div class="jarviswidget-editbox">
	                <!-- This area used as dropdown edit box -->
	                <input class="form-control" type="text">
	            </div><!-- end widget edit box -->
	 
	            <!-- widget content -->
	            <div class="widget-body">

	            	<div>display here mysql</div>

	            </div><!-- end widget content -->
	        </div><!-- end widget div -->
	    </div><!-- end widget -->
    </article>	

	<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0">
	 
	        <header>
				<span class="widget-icon"> <i class="fa fa-cog"></i> </span>
	            <h2>Monitor</h2>
	        </header><!-- widget div-->
	 
	        <div>
	            <!-- widget edit box -->
	            <div class="jarviswidget-editbox">
	                <!-- This area used as dropdown edit box -->
	                <input class="form-control" type="text">
	            </div><!-- end widget edit box -->
	 
	            <!-- widget content -->
	            <div class="widget-body">

	            	
	            	<?php echo form_open('/', array('class' => "smart-form search-form"))?>
	            		<header>
	            			Enter a Widget ID from Content Locker V1 to check if it exists in the new V2
	            		</header>
	            		<fieldset>
							<section>
								<label class="label">Widget ID</label>
								<label class="input">
									<input type="text" class="input-lg widget-id-input">
								</label>
							</section>	            			
							<section class="text-center">	
							<a href="javascript:void(0);" class="btn btn-primary btn-lg search-widget-id"><i class="fa fa-search"></i> Search/Monitor</a>							
							<a href="javascript:void(0);" class="btn btn-default btn-lg cancel-search-widget-id"><i class="fa fa-times"></i> Cancel</a>							
							</section>
	            		</fieldset>
	            	<?php echo form_close() ?>
	            	<div class="results-container">
						<div class="progress progress-micro">
							<div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;"></div>
						</div>	            	
						<div class="results">
							
						</div>						
				    </div>
								 
	            </div><!-- end widget content -->
	        </div><!-- end widget div -->
	    </div><!-- end widget -->
    </article>    
</div>
<!-- 

	monitor, just enable and trigger check every 5 seconds

	retrieve all mySQL and Mondgo, perform array intersection or diff and display under results, count
	results provide a button to start monitor, activate all monitors, just this, etc
	
	++ mixup wdgets 
	[ MySQL + Mongo ]  [ Monitor ]


-->