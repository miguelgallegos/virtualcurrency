<?php
	$script_start = '<script type="text/javascript" src="';
	$script_end = '"></script>';
?>


<section id="widget-grid">
 
        <!-- row -->
        <div class="row">

            <article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0">
			 
			        <header>
						<span class="widget-icon"> <i class="fa fa-cog"></i> </span>
			            <h2>Content Locker</h2>
			        </header><!-- widget div-->
			 
			        <div>
			            <!-- widget edit box -->
			            <div class="jarviswidget-editbox">
			                <!-- This area used as dropdown edit box -->
			                <input class="form-control" type="text">
			            </div><!-- end widget edit box -->
			 
			            <!-- widget content -->
			            <div class="widget-body">

							<?php 
								$direct_link = base_url('clv1/content_locker?fn=direct&w=123ac37bed0a0cafa41a86310a036d82');
								$content_locker_src = base_url('clv1/content_locker?fn=script&w=123ac37bed0a0cafa41a86310a036d82');

								$onclick_version_src = base_url('clv1/content_locker?fn=onclick&w=123ac37bed0a0cafa41a86310a036d82');
								$onclick_load_widget = '<a onclick="load_widget()" href="#">Click to Load Widget</a>';
							?>

							<h3>Direct Link</h3>
							<textarea style="font-size:13px;width:100%;display:table-caption !important;" rows="2"><?= $direct_link ?></textarea>
							<p><a href="<?= $direct_link ?>" target="_blank">Content Locker Sample</a></p>
							<p><a href="http://tbm.verifystation.com/go.php?w=f9b09ce4369c1129f6791547d6ad9c2d" target="_blank">TBM Original Content Locker Example</a>							

							<h3>Content Locker Code</h3>
							<textarea style="font-size:13px;width:100%;display:table-caption !important;" rows="2"><?= $script_start . $content_locker_src . $script_end ?></textarea>

							<h3>On-Click Version</h3>
							<textarea style="font-size:13px;width:100%;display:table-caption !important;" rows="2"><?= $script_start . $onclick_version_src .$script_end?></textarea>
							<textarea style="font-size:13px;width:100%;display:table-caption !important;" rows="2"><?= $onclick_load_widget ?></textarea>
			 
			            </div><!-- end widget content -->
			        </div><!-- end widget div -->
			    </div><!-- end widget -->

				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-1">
			 
			        <header>
						<span class="widget-icon"> <i class="fa fa-cog"></i> </span>
			            <h2>Offer Wall</h2>
			        </header><!-- widget div-->
			 
			        <div>
			            <!-- widget edit box -->
			            <div class="jarviswidget-editbox">
			                <!-- This area used as dropdown edit box -->
			                <input class="form-control" type="text">
			            </div><!-- end widget edit box -->
			 
			            <!-- widget content -->
			            <div class="widget-body">
							
							<?php 
								$offer_wall = base_url('clv1/offer_wall?w=610685b00395fb6d9b33a957468b5fe4') 
							?>

							<textarea style="font-size:13px;width:100%;display:table-caption !important;" rows="2"><?= $offer_wall ?></textarea>
							<p><a href="<?= $offer_wall ?>" target="_blank">Offer Wall Sample</a></p>
							<p><a href="http://tbm.verifystation.com/offer_wall/?w=610685b00395fb6d9b33a957468b5fe4" target="_blank">TBM Original Offer Wall Example</a>							

			            </div><!-- end widget content -->
			        </div><!-- end widget div -->
			    </div><!-- end widget -->

            </article><!-- END GRID -->

            <article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0">
			 
			        <header>
						<span class="widget-icon"> <i class="fa fa-cog"></i> </span>
			            <h2>File Locker</h2>
			        </header><!-- widget div-->
			 
			        <div>
			            <!-- widget edit box -->
			            <div class="jarviswidget-editbox">
			                <!-- This area used as dropdown edit box -->
			                <input class="form-control" type="text">
			            </div><!-- end widget edit box -->
			 
			            <!-- widget content -->
			            <div class="widget-body">

							<?php
								$direct_link = base_url('clv1/file_locker?l=c2c52f392bd63e45749bf829eec4d7c8');
								$html_code = '<a href="' . $direct_link . '">Download Now</a>';
							?>

							<h3>Direct Link</h3>							
							<textarea style="font-size:13px;width:100%;display:table-caption !important;" rows="2"><?= $direct_link ?></textarea>
							<p><a href="<?= $direct_link ?>" target="_blank">File Locker Sample</a></p>
							<p><a href="http://tbm.verifystation.com/file_lock/?l=c2c52f392bd63e45749bf829eec4d7c8" target="_blank">TBM Original File Locker Example</a>

							<h3>HTML Code</h3>							
							<textarea style="font-size:13px;width:100%;display:table-caption !important;" rows="2"><?= $html_code ?></textarea>			            
		 
			            </div><!-- end widget content -->
			        </div><!-- end widget div -->
			    </div><!-- end widget -->

				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-1">
			 
			        <header>
						<span class="widget-icon"> <i class="fa fa-cog"></i> </span>
			            <h2>Super URL</h2>
			        </header><!-- widget div-->
			 
			        <div>
			            <!-- widget edit box -->
			            <div class="jarviswidget-editbox">
			                <!-- This area used as dropdown edit box -->
			                <input class="form-control" type="text">
			            </div><!-- end widget edit box -->
			 
			            <!-- widget content -->
			            <div class="widget-body">

							<?php 
								$super_url = base_url('clv1/my_url?a=1002');
							?>

							<textarea style="font-size:13px;width:100%;display:table-caption !important;" rows="2"><?= $super_url ?></textarea>
							<p><a href="<?= $super_url ?>" target="_blank">Super URL Sample</a></p>
							<p><a href="http://tbm.verifystation.com/my_url.php?a=1002" target="_blank">TBM Original Super URL Example</a>

			            </div><!-- end widget content -->
			        </div><!-- end widget div -->
			    </div><!-- end widget -->

				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-1">
			 
			        <header>
						<span class="widget-icon"> <i class="fa fa-cog"></i> </span>
			            <h2>Landing Page</h2>
			        </header><!-- widget div-->
			 
			        <div>
			            <!-- widget edit box -->
			            <div class="jarviswidget-editbox">
			                <!-- This area used as dropdown edit box -->
			                <input class="form-control" type="text">
			            </div><!-- end widget edit box -->
			 
			            <!-- widget content -->
			            <div class="widget-body">

							<?php
								$landing_page = base_url('clv1/landing_page?l=6a37e5a906493cf06d2920d6fba087d1');
							?>

							<textarea style="font-size:13px;width:100%;display:table-caption !important;" rows="2"><?= $landing_page ?></textarea>
							<p><a href="<?= $landing_page ?>" target="_blank">Landing Page Sample</a></p>
							<p><a href="http://tbm.verifystation.com/lp/?l=6a37e5a906493cf06d2920d6fba087d1" target="_blank">TBM Original Super URL Example</a>

			            </div><!-- end widget content -->
			        </div><!-- end widget div -->
			    </div><!-- end widget -->			    

            </article><!-- END GRID -->
 
        </div><!-- end row -->
 
    </section><!-- end widget grid -->




