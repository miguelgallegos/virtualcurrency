<!-- Link to datatables plugin -->
<link rel="stylesheet" type="text/css" media="screen" href="//cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
<script src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>    

<script>
	$(document).ready(function(){
	    $('#table_ho_offers').dataTable();
	    $('#table_ho_offer_server').dataTable(
			{ "aaSorting": [ [3, 'desc'] ]  }

	    );	    
	});
</script>

<section id="widget-grid">
 
        <!-- row -->
        <div class="row">
            <!-- SINGLE GRID -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0">
			 
			        <header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
			            <h2>MongoDB: 'ho_offer_server' collection</h2>
			        </header><!-- widget div-->
			 
			        <div>
			            <!-- widget edit box -->
			            <div class="jarviswidget-editbox">
			                <!-- This area used as dropdown edit box -->
			                <input class="form-control" type="text">
			            </div><!-- end widget edit box -->
			 
			            <!-- widget content -->
			            <div class="widget-body">

							<p>&nbsp;</p>
							
							<div class="table-responsive">
							
								<table id='table_ho_offer_server' class="table table-bordered table-striped table-hover" width="100%">
									<thead>
										<tr>
											<th>Offer ID</th>
											<th>Name</th>
											<th>Status</th>
											<th>Clicks</th>
											<th>Conversions</th>
										</tr>
									</thead>
									<tbody>

										<?php foreach ($database['ho_offer_server'] as $num => $column): ?>
										<tr>
											<td><?= $column['offer_id'] ?></td>
											<td><?= $column['offer_name'] ?></td>
											<td><?= $column['offer_status'] ?></td>
											<td><?= $column['offer_clicks'] ?></td>
											<td><?= $column['offer_conversions'] ?></td>
										</tr>
										<?php endforeach; ?>

									</tbody>
								</table>
								
							</div>			                
			 
			            </div><!-- end widget content -->
			        </div><!-- end widget div -->
			    </div><!-- end widget -->

				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0">
			 
			        <header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
			            <h2>MongoDB: 'ho_offers' collection</h2>
			        </header><!-- widget div-->
			 
			        <div>
			            <!-- widget edit box -->
			            <div class="jarviswidget-editbox">
			                <!-- This area used as dropdown edit box -->
			                <input class="form-control" type="text">
			            </div><!-- end widget edit box -->
			 
			            <!-- widget content -->
			            <div class="widget-body">

							<p>&nbsp;</p>
							
							<div class="table-responsive">
							
								<table id='table_ho_offers' class="table table-bordered table-striped table-hover" width="100%">
									<thead>
										<tr>
											<th>Name</th>
											<th>Advertiser</th>
											<th>Status</th>
											<th>Payout</th>
										</tr>
									</thead>
									<tbody>

										<?php foreach($database['ho_offers'] as $num => $column): ?>
										<tr>
											<td><?= $column['offer_details_name'] ?></td>
											<td><?= $column['offer_details_advertiser'] ?></td>
											<td><?= $column['offer_details_status'] ?></td>
											<td><?= $column['offer_payout_cost_per_conversion'] ?></td>
										</tr>
										<?php endforeach; ?>

									</tbody>
								</table>
								
							</div>			                
			 
			            </div><!-- end widget content -->
			        </div><!-- end widget div -->
			    </div><!-- end widget -->

             
 

 
            </article><!-- END GRID -->
 
            
 
        </div><!-- end row -->
 
    </section><!-- end widget grid -->
