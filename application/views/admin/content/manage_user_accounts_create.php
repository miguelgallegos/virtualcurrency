<section id="widget-grid">
        <!-- row -->
        <div class="row">
            <!-- SINGLE GRID -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0">
			 
			        <header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2><?= lang('groups_heading') ?></h2>
			        </header><!-- widget div-->
			 
			        <div>
			            <!-- widget edit box -->
			            <div class="jarviswidget-editbox">
			                <!-- This area used as dropdown edit box -->
			                <input class="form-control" type="text">
			            </div><!-- end widget edit box -->
			 
			            <!-- widget content -->
			            <div class="widget-body">

							<?= !empty($message) ? $message : '' ?>

							<h1><?= lang('create_user_heading') ?></h1>
							<p><?= lang('create_user_subheading') ?></p>
							
							<?= form_open("admin/manage_users/create_user", array('class' => 'smart-form') ) ?>

								<fieldset>
									<div class="row">
										<section class="col col-6">
											<label class="label"><?= lang('create_user_fname_label', 'first_name') ?></label>
											<label class="input">
												<input type="<?= $first_name['type'] ?>" name="<?= $first_name['name'] ?>" id="<?= $first_name['id'] ?>" value="<?= $first_name['value'] ?>">
											</label>
										</section>
									</div>

									<div class="row">
										<section class="col col-6">
											<label class="label"><?= lang('create_user_lname_label', 'last_name') ?></label>
											<label class="input">
												<input type="<?= $last_name['type'] ?>" name="<?= $last_name['name'] ?>" id="<?= $last_name['id'] ?>" value="<?= $last_name['value'] ?>">
											</label>
										</section>
									</div>

									<div class="row">
										<section class="col col-6">
											<label class="label"><?= lang('create_user_company_label', 'company') ?></label>
											<label class="input">
												<input type="<?= $company['type'] ?>" name="<?= $company['name'] ?>" id="<?= $company['id'] ?>" value="<?= $company['value'] ?>">
											</label>
										</section>
									</div>

									<div class="row">
										<section class="col col-6">
											<label class="label"><?= lang('create_user_email_label', 'email') ?></label>
											<label class="input">
												<input type="<?= $email['type'] ?>" name="<?= $email['name'] ?>" id="<?= $email['id'] ?>" value="<?= $email['value'] ?>">				
											</label>
										</section>
									</div>

									<div class="row">
										<section class="col col-6">
											<label class="label"><?= lang('create_user_phone_label', 'phone') ?></label>
											<label class="input">
												<input type="<?= $phone['type'] ?>" name="<?= $phone['name'] ?>" id="<?= $phone['id'] ?>" value="<?= $phone['value'] ?>">				
											</label>
										</section>
									</div>

									<div class="row">
										<section class="col col-6">
											<label class="label"><?= lang('create_user_password_label', 'password') ?></label>
											<label class="input">
												<input type="<?= $password['type'] ?>" name="<?= $password['name'] ?>" id="<?= $password['id'] ?>" value="<?= $password['value'] ?>">				
											</label>
										</section>
									</div>

									<div class="row">
										<section class="col col-6">
											<label class="label"><?= lang('create_user_password_confirm_label', 'password_confirm') ?></label>
											<label class="input">
												<input type="<?= $password_confirm['type'] ?>" name="<?= $password_confirm['name'] ?>" id="<?= $password_confirm['id'] ?>" value="<?= $password_confirm['value'] ?>"				
											</label>
										</section>
									</div>      
								</fieldset>									

								<footer>
									<button type="submit" class="btn btn-primary" >
										<?= lang('create_user_submit_btn') ?>
									</button>
								</footer>									

							<?= form_close() ?>
			 
			            </div><!-- end widget content -->
			        </div><!-- end widget div -->
			    </div><!-- end widget -->
 
            </article><!-- END GRID -->
           
 
        </div><!-- end row -->
 
    </section><!-- end widget grid -->
