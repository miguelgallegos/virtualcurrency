<!-- Link to datatables plugin -->
<link rel="stylesheet" type="text/css" media="screen" href="//cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
<script src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>    


<script>
	$(document).ready(function(){
	    $('#table_manage_user_accounts').dataTable(
			{ "aaSorting": [ [0, 'asc'] ]   ?>

	    );	    
	 ?>);
</script>

<section id="widget-grid">
        <!-- row -->
        <div class="row">
            <!-- SINGLE GRID -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0">
			 
			        <header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2><?= lang('index_heading') ?></h2>
			        </header><!-- widget div-->
			 
			        <div>
			        	<p><?= lang('index_subheading') ?></p>
			            <!-- widget edit box -->
			            <div class="jarviswidget-editbox">
			                <!-- This area used as dropdown edit box -->
			                <input class="form-control" type="text">
			            </div><!-- end widget edit box -->
			 
			            <!-- widget content -->
			            <div class="widget-body">

							<?= !empty($message) ? $message : '' ?>									            

							<p>
								<a href="<?= base_url('admin/manage_users/create_user') ?>"><?= lang('index_create_user_link') ?></a> | 
								<a href="<?= base_url('admin/manage_users/create_group') ?>"><?= lang('index_create_group_link') ?></a>
							</p>

							<div class="table-responsive">
							
								<table id='table_manage_user_accounts' class="table table-bordered table-striped table-hover" width="100%">
									<thead>
										<tr>
											<th><?= lang('index_fname_th') ?></th>
											<th><?= lang('index_lname_th') ?></th>
											<th><?= lang('index_email_th') ?></th>
											<th><?= lang('index_groups_th') ?></th>
											<th><?= lang('index_status_th') ?></th>
											<th><?= lang('index_action_th') ?></th>
										</tr>
									</thead>
									<tbody>

										<?php foreach($users as $user): ?>
										<tr>
											<td><?= $user->first_name ?></td>
											<td><?= $user->last_name ?></td>
											<td><?= $user->email ?></td>

											<td>
												<?php foreach($user->groups as $group): ?>
													<?= anchor("admin/manage_users/edit_group/" . $group->id, $group->name) ?><br />													
								                <?php endforeach; ?>
											</td>
											<td>
												<?php if($user->active): ?>
													<?= lang('index_active_link') ?>
												<?php else: ?>
													<?= lang('index_inactive_link') ?>													
												<?php endif; ?>
											</td>
											<td>
												<?php if($user->active): ?>
													<?= anchor('admin/manage_users/deactivate_user/' . $user->id, 'Deactivate') ?> | 
												<?php else: ?>
													<?= anchor('admin/manage_users/activate_user/' . $user->id, 'Activate') ?> | 
												<?php endif; ?>											
												<?= anchor('admin/manage_users/edit_user/' . $user->id, 'Edit') ?> | 
												<?= anchor('admin/manage_users/delete_user/' . $user->id, 'Delete') ?>
											</td>
										</tr>
										<?php endforeach; ?>

									</tbody>
								</table>
								
							</div>			                
			 
			            </div><!-- end widget content -->
			        </div><!-- end widget div -->
			    </div><!-- end widget -->
 
            </article><!-- END GRID -->
           
 
        </div><!-- end row -->
 
    </section><!-- end widget grid -->
