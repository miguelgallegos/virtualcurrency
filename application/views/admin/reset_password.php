					<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">

						<div class="well no-padding">

							<?= !empty($message) ? $message : '' ?>

							<?= form_open('reset_password' . $code, array('id' => 'login-form', 'class' => 'smart-form client-form') ) ?>							
								<header>
									Change Password
								</header>

								<fieldset>
									
									<section>
										<label class="label">New Password</label>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="<?= $new_password['type'] ?>" name="<?= $new_password['name'] ?>" pattern="<?= $new_password['pattern'] ?>">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
									</section>

									<section>
										<label class="label">Confirm Password</label>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="<?= $new_password_confirm['type'] ?>" name="<?= $new_password_confirm['name'] ?>" pattern="<?= $new_password_confirm['pattern'] ?>">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
									</section>

									<input type="hidden" name="<?= $user_id['name'] ?>" value="<?= $user_id['value'] ?>">
									<input type="hidden" name="<?= $csrf['name'] ?>" value="<?= $csrf['value'] ?>">
								</fieldset>
								<footer>
									<button type="submit" class="btn btn-primary">
										Sign in
									</button>
								</footer>							

							<?= form_close() ?>
						</div>
					</div>
