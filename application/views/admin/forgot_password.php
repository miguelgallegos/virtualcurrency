					<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">

						<div class="well no-padding">

							<?= !empty($message) ? $message : '' ?>

							<?= form_open('forgot_password', array('id' => 'login-form', 'class' => 'smart-form client-form') ) ?>							
							
								<header>
									Forgot Password
								</header>

								<fieldset>

									<section>
										<label class="label">Enter your email address</label>
										<label class="input"> <i class="icon-append fa fa-envelope"></i>
											<input type="email" name="email">
											<b class="tooltip tooltip-top-right"><i class="fa fa-envelope txt-color-teal"></i> Please enter email address for password reset</b></label>
									</section>

								</fieldset>
								<footer>
									<button type="submit" class="btn btn-primary">
										<i class="fa fa-refresh"></i> Reset Password
									</button>
								</footer>							

							<?= form_close() ?>
						</div>
					</div>
