<div class="scrollable">
  <div class="scrollable-content">
    <div class="form-group-row">
        <div class="searchBar">        
            <input ng-model="query" type="search"  class="ng-scope ng-valid needsclick form-control ng-dirty search-key" placeholder="Search" >
        </div>        
    </div>
  
    <div class="list-group">

      <a ng-repeat="reward in rewards | filter:query | orderBy:orderProp" href="#/reward/{{reward.id}}" class="list-group-item media">                   
            <img ng-src="{{reward.image}}" height="50" class="pull-left"/>
            <strong>{{reward.name}}</strong>
            <div class="pull-right">            
                <span class="label label-primary">+{{reward.points}}</span>
                <!--<i class="fa fa-chevron-right"></i>-->
            </div>  
      </a>
    </div>
      

  </div>
</div>