<div class="scrollable">
    <div class="scrollable-content container-fluid section">

        <div class="list-group">
            <div class="list-group-item">
                <img ng-src="{{offer.offer_image}}">
                <h1>{{offer.offer_name}} <span class="label label-primary">+{{offer.offer_points}} points</span></h1>                
                <p>{{offer.offer_description}}</p>                    
                
            </div>
        </div>
        <!--
        <div class="list-group-item">
            <p class="well">__SHOW POINTS__</p>                
        </div>
        -->
        <div class="list-group-item" ng-show="showButton">
            <div class="button-container" >
                <button class="action btn btn-default btn-block" ng-click="downloadButton.doClick(this, $event)">Click to Download</button>
            </div>
        </div>
        <div class="list-group-item" ng-hide="showButton">                
            <h2>Thank you for downloading!</h2>            
            <div class="alert alert-info" role="alert">                
                <p><strong>Congratulations! </strong> You just earned <span class="label label-primary">{{offer.offer_points}}</span> points!</p>
            </div>            
        </div>

        <div class="list-group-item">        
            <div class="well">
                <h3>To earn points...</h3>
                <div class="list-group">

                    <div class="list-group-item media">
                        <img ng-src="{{offer.offer_image}}" height="36" class="pull-left"/>        
                        <div class="media-body">
                            <strong>Use the app for at least 1 minute</strong>
                            <p>Complete any tutorials.</p>
                        </div>
                    </div>
                    <div class="list-group-item media">
                        <i class="fa fa-wifi fa-2x pull-left"></i>
                        <div class="media-body">
                            <strong>Don't switch networks</strong>
                            <p>Such as switching from 3G to Wi-Fi.</p>
                        </div>    
                    </div>
                    <div class="list-group-item media">
                        <i class="fa fa-smile-o fa-2x pull-left"></i>
                        <div class="media-body">
                            <strong>Check your points</strong>
                            <p>Wait until your points becomes unlocked.</p>
                        </div>    
                    </div>

                </div>
            </div>            
        </div>


    </div>
</div>    

