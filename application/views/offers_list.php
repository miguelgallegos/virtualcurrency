<div class="scrollable">
  <div class="scrollable-content">
    <div class="form-group-row">
        <div class="searchBar">        
            <input ng-model="query" type="search"  class="ng-scope ng-valid needsclick form-control ng-dirty search-key" placeholder="Search" >
        </div>        
    </div>
  
    <div class="list-group">

      <a ng-repeat="offer in offers | filter:query | orderBy:orderProp" href="#/offer/{{offer.offer_id}}" class="list-group-item media">                   
            <img ng-src="{{offer.offer_image}}" height="50" class="pull-left"/>
            <strong>{{offer.offer_name}}</strong>
            <div class="pull-right">            
                <span class="label label-primary">+{{offer.offer_points}}</span>
                <!--<i class="fa fa-chevron-right"></i>-->
            </div>  
      </a>
    </div>
      

  </div>
</div>