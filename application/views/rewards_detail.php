<div content-for="title">
  <span>Reward Details</span>
</div>
<div class="scrollable">
    <div class="scrollable-content container-fluid section">

        <div class="list-group">
            <div class="list-group-item">
                <img ng-src="{{reward.image}}">
                <h1>{{reward.name}} <span class="label label-primary">+{{reward.points}} points</span></h1>                
                <!--<p>{{reward.description}}</p>                    -->
                
            </div>
        </div>
        <!--
        <div class="list-group-item">
            <p class="well">__SHOW POINTS__</p>                
        </div>
        -->

        <div class="list-group-item">        
            <div class="well">
                <div class="list-group">
                    <div class="list-group-item media">
                        <i class="fa fa-wifi fa-2x pull-left"></i>
                        <div class="media-body">
                            <strong>This reward is for the US only.</strong>
                            <p>Use on the appropriate network US only.</p>
                        </div>    
                    </div>
                </div>
            </div>            
        </div>

        <div class="list-group-item" ng-show="showRewardButton">
            <div class="button-container" >
                <button class="action btn btn-danger btn-block" ng-click="rewardButton.doClick(this, $event)">Claim reward for {{reward.points}} points</button>
            </div>
        </div>
        
        <div class="list-group-item" ng-hide="showRewardButton">                
            <h2>Your reward has been redeemed!</h2>            
            <div class="alert alert-info" role="alert">                
                <p><strong>Congratulations! </strong> You just earned an <span class="label label-primary">{{reward.name}}</span></p>
                <p>We will send you an email about your reward detials.</p>
            </div>            
        </div>
        
    </div>
</div>    

<div overlay="overlay1" default="{{overlayShow}}">
  <h4 class="overlay-title">Reward cannot be redeemed</h4>    
  <p>
    {{rewardClaimError}}
  </p>
  <p toggle="off" bubble target="overlay1">
    <span class="btn btn-default">OK</span>
    <!--<span class="btn btn-default">Cancel</span>-->
  </p>  
</div>


<!-- 
    add logic to button:
        add flag to display the DX on the credit
        add service (ajax) to store rewards
            debit my account (add a bank.transaction(debit,item, reward_id))
        add instructions per reward, so it doesn't only display "US reward only"
            add code generator for gift cards        
        check if reward should be displayed on my list

    for OFFERS:
        check if offer should be displayed on my list        
        add redirect to main page (/offers) after completing download

-->

