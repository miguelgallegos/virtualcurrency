<!DOCTYPE html>
<html style="height:100%;" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>File Locked</title>

    <link rel="shortcut icon" href="http://cdn.verifystation.com/favicon.ico" type="image/x-icon">
    <link rel="icon" href="http://cdn.verifystation.com/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" type="text/css" href="<?= base_url('cntlockv1/css/main.css') ?>">
    <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato">
    
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

    <script type="text/javascript">
        function conversion_check() {
                setTimeout("conversion_check();", 20000);
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('ajax/clv1_conversion_check') ?>",
                    dataType: "json",
                    data: {
                        type:       "mobile",
                        aff_id:     "<?= $lock_settings['affiliate_id']; ?>",
                        w_id:       "<?= $lock_settings['widget_id']; ?>",
                        ip:         "<?= $lock_settings['user_ip']; ?>",
                        red_url:    "<?= base64_encode($lock_settings['file_url']); ?>",
                        o_count:    "<?= count($offer_feed); ?>", 
                        <?= $this->security->get_csrf_token_name() ?>: "<?= $this->security->get_csrf_hash() ?>"                        
                    },
                    success: function(data) {
                        // If redirect is not blank, send browser to that URL
                        if(data.redirect != '')
                            window.location.replace(data.redirect);
                    }
                })
        }        

        function start_check() {
            $('#offerlst').hide();
            $('#offerwait').show('slow');
            conversion_check();
        }

        function check_back() {
            $('#offerwait').hide();
            $('#offerlst').fadeIn('slow');
        }

        function randomFromTo(from, to) {
            var numb = Math.floor(Math.random() * (to - from + 1) + from);
            return numb;
        }
        var totalScans, totalDone;

        function start() {
            var targetDOM = $('.scanStatus');
            totalDone = 0;
            totalScans = targetDOM.length;
            targetDOM.each(function () {
                perform_scan($(this));
            });
        }

        function perform_scan(dom) {
            var state = dom.attr("state");
            var rand = randomFromTo(1, 100);
            if (state == "waitingScan") {
                if (rand > 73) {
                    var progressDiv = dom.find(".progress");
                    dom.find(".waitingScan").css('display', 'none');
                    progressDiv.find('.progress-bar').text("Analysing...");
                    progressDiv.css('display', 'block');
                    dom.attr("state", "progress");
                }
            } else if (state == "progress") {
                if (rand > 55) {
                    var progressBar = dom.find(".progress-bar");
                    var progressVal = parseInt(progressBar.attr("data-percentage"));
                    var increase = 0;
                    if (100 - progressVal < 9) {
                        increase = 100 - progressVal;
                    } else {
                        increase = randomFromTo(1, 8);
                    }
                    var newVal = increase + progressVal;
                    progressBar.width(newVal);
                    progressBar.attr("data-percentage", newVal);
                    if (newVal >= 100) {
                        dom.find(".progress").css('display', 'none');
                        dom.find(".foundNothing").css('display', 'block');
                        dom.attr("state", "foundNothing");
                    }
                }
            }
            if (state != "complete") {
                setTimeout(function () {
                    perform_scan(dom)
                }, randomFromTo(150, 300));
            }
        }
        $(document).ready(function () {
            start();
            $("#dload").click(function () {
                $("#dload").fadeOut(500, function () {
                    $('#offers').slideDown();
                });
            });
        });
    </script>
</head>
<body>
    <div style="margin: 0px auto;">
        <div class="headerbox">
            <div class="header">
                <div class="titlecontainer">File Locked</div>
            </div>
        </div>
        <div class="box1">
            <div style="margin-left:12px;"><?php echo $lock_settings['file_name']; ?></div>
        </div>
        <div class="box2">
            <div class="info">
                <div class="leftcontent">
                    <div class="datagrid">
                        <table>
                            <tbody>
                                <tr><td>File Name:</td><td><?php echo $lock_settings['file_name']; ?></td></tr>
                                <tr class="alt"><td>Size:</td><td><?php echo $lock_settings['file_size']; ?></td></tr>
                                <tr><td>Date Uploaded:</td><td><?php echo $lock_settings['file_uploaded']; ?></td></tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="offerarea">
                        <button id="dload" type="button" class="classname">DOWNLOAD</button>
                        <div id="offers">

                            <?php for($x=0; $x<count($offer_feed); $x++): ?>
                                <p class="offer">
                                    <a class="olink" onclick="start_check();" href="<?= $offer_feed[$x]['offer_link'] ?>" target="_blank"><?= $offer_feed[$x]['offer_name'] ?></a>
                                </p>
                            <?php endfor; ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="rightcontent">
                <div class="stepbox">
                    <img src="http://cdn.verifystation.com/lock.png" style="margin-right:8px;"><b>Step 1:</b> Click "Download" to view available offers in your region.
                </div>
                <div class="stepbox">
                    <img src="http://cdn.verifystation.com/offer.png" style="margin-right:8px;"><b>Step 2:</b> Unlock Your Download by completing a free offer of your choice.
                </div>
                <div class="stepbox">
                    <img src="http://cdn.verifystation.com/check.png" style="margin-right:8px;"><b>Step 3:</b> Your file will be unlocked and start to download!
                </div>
            </div>
        </div>
        <div class="box3">
            <div class="content">
                <div id="table1">
                    <table class="waiting-for-scan" cellspacing="0">
                        <tbody>
                            <tr style="height:30px;">
                                <td style="width:90px;"><img src="http://cdn.verifystation.com/antivir.png"></td>
                                <td style="width:105px;" class="scanStatus" state="waitingScan" id="6" align="right">
                                    <span class="waitingScan">Waiting for scan...</span>
                                    <div class="progress active progress-striped" style="width: 100px; display: none;">
                                        <div class="progress-bar" style="float: left; width: 100px;" data-percentage="0">Analysing...</div>
                                    </div>
                                    <span class="label label-success foundNothing" style="display: none; text-align: center;">No Viruses Found</span>
                                </td>
                            </tr>
                            <tr style="height:30px;">
                                <td style="width:90px;"><img src="http://cdn.verifystation.com/avast.png"></td>
                                <td style="width:105px;" class="scanStatus" state="waitingScan" id="6" align="right">
                                    <span class="waitingScan">Waiting for scan...</span>
                                    <div class="progress active progress-striped" style="width: 100px; display: none;">
                                        <div class="progress-bar" style="float: left; width: 100px;" data-percentage="0">Analysing...</div>
                                    </div>
                                    <span class="label label-success foundNothing" style="display: none; text-align: center;">No Viruses Found</span>
                                </td>
                            </tr>
                            <tr style="height:30px;">
                                <td style="width:90px;"><img src="http://cdn.verifystation.com/avg.png"></td>
                                <td style="width:105px;" class="scanStatus" state="waitingScan" id="6" align="right">
                                    <span class="waitingScan">Waiting for scan...</span>
                                    <div class="progress active progress-striped" style="width: 100px; display: none;">
                                        <div class="progress-bar" style="float: left; width: 100px;" data-percentage="0">Analysing...</div>
                                    </div>
                                    <span class="label label-success foundNothing" style="display: none; text-align: center;">No Viruses Found</span>
                                </td>
                            </tr>
                            <tr style="height:30px;">
                                <td style="width:90px;"><img src="http://cdn.verifystation.com/bitdefender.png"></td>
                                <td style="width:105px;" class="scanStatus" state="waitingScan" id="6" align="right">
                                    <span class="waitingScan">Waiting for scan...</span>
                                    <div class="progress active progress-striped" style="width: 100px; display: none;">
                                        <div class="progress-bar" style="float: left; width: 100px;" data-percentage="0">Analysing...</div>
                                    </div>
                                    <span class="label label-success foundNothing" style="display: none; text-align: center;">No Viruses Found</span>
                                </td>
                            </tr>
                            <tr style="height:30px;">
                                <td style="width:90px;"><img src="http://cdn.verifystation.com/clamav.png"></td>
                                <td style="width:105px;" class="scanStatus" state="waitingScan" id="6" align="right">
                                    <span class="waitingScan">Waiting for scan...</span>
                                    <div class="progress active progress-striped" style="width: 100px; display: none;">
                                        <div class="progress-bar" style="float: left; width: 100px;" data-percentage="0">Analysing...</div>
                                    </div>
                                    <span class="label label-success foundNothing" style="display: none; text-align: center;">No Viruses Found</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="table2">
                    <table class="waiting-for-scan" cellspacing="0">
                        <tbody>
                            <tr style="height:30px;">
                                <td style="width:90px;"><img src="http://cdn.verifystation.com/drweb.png"></td>
                                <td style="width:105px;" class="scanStatus" state="waitingScan" id="6" align="right">
                                    <span class="waitingScan">Waiting for scan...</span>
                                    <div class="progress active progress-striped" style="width: 100px; display: none;">
                                        <div class="progress-bar" style="float: left; width: 100px;" data-percentage="0">Analysing...</div>
                                    </div>
                                    <span class="label label-success foundNothing" style="display: none; text-align: center;">No Viruses Found</span>
                                </td>
                            </tr>
                            <tr style="height:30px;">
                                <td style="width:90px;"><img src="http://cdn.verifystation.com/fprot.png"></td>
                                <td style="width:105px;" class="scanStatus" state="waitingScan" id="6" align="right">
                                    <span class="waitingScan">Waiting for scan...</span>
                                    <div class="progress active progress-striped" style="width: 100px; display: none;">
                                        <div class="progress-bar" style="float: left; width: 100px;" data-percentage="0">Analysing...</div>
                                    </div>
                                    <span class="label label-success foundNothing" style="display: none; text-align: center;">No Viruses Found</span>
                                </td>
                            </tr>
                            <tr style="height:30px;">
                                <td style="width:90px;"><img src="http://cdn.verifystation.com/fsecure.png"></td>
                                <td style="width:105px;" class="scanStatus" state="waitingScan" id="6" align="right">
                                    <span class="waitingScan">Waiting for scan...</span>
                                    <div class="progress active progress-striped" style="width: 100px; display: none;">
                                        <div class="progress-bar" style="float: left; width: 100px;" data-percentage="0">Analysing...</div>
                                    </div>
                                    <span class="label label-success foundNothing" style="display: none; text-align: center;">No Viruses Found</span>
                                </td>
                            </tr>
                            <tr style="height:30px;">
                                <td style="width:90px;"><img src="http://cdn.verifystation.com/gdata.png"></td>
                                <td style="width:105px;" class="scanStatus" state="waitingScan" id="6" align="right">
                                    <span class="waitingScan">Waiting for scan...</span>
                                    <div class="progress active progress-striped" style="width: 100px; display: none;">
                                        <div class="progress-bar" style="float: left; width: 100px;" data-percentage="0">Analysing...</div>
                                    </div>
                                    <span class="label label-success foundNothing" style="display: none; text-align: center;">No Viruses Found</span>
                                </td>
                            </tr>
                            <tr style="height:30px;">
                                <td style="width:90px;"><img src="http://cdn.verifystation.com/ikarus.png"></td>
                                <td style="width:105px;" class="scanStatus" state="waitingScan" id="6" align="right">
                                    <span class="waitingScan">Waiting for scan...</span>
                                    <div class="progress active progress-striped" style="width: 100px; display: none;">
                                        <div class="progress-bar" style="float: left; width: 100px;" data-percentage="0">Analysing...</div>
                                    </div>
                                    <span class="label label-success foundNothing" style="display: none; text-align: center;">No Viruses Found</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="table3">
                    <table class="waiting-for-scan" cellspacing="0">
                        <tbody>
                            <tr style="height:30px;">
                                <td style="width:90px;"><img src="http://cdn.verifystation.com/kaspersky.png"></td>
                                <td style="width:105px;" class="scanStatus" state="waitingScan" id="6" align="right">
                                    <span class="waitingScan">Waiting for scan...</span>
                                    <div class="progress active progress-striped" style="width: 100px; display: none;">
                                        <div class="progress-bar" style="float: left; width: 100px;" data-percentage="0">Analysing...</div>
                                    </div>
                                    <span class="label label-success foundNothing" style="display: none; text-align: center;">No Viruses Found</span>
                                </td>
                            </tr>
                            <tr style="height:30px;">
                                <td style="width:90px;"><img src="http://cdn.verifystation.com/nod32.png"></td>
                                <td style="width:105px;" class="scanStatus" state="waitingScan" id="6" align="right">
                                    <span class="waitingScan">Waiting for scan...</span>
                                    <div class="progress active progress-striped" style="width: 100px; display: none;">
                                        <div class="progress-bar" style="float: left; width: 100px;" data-percentage="0">Analysing...</div>
                                    </div>
                                    <span class="label label-success foundNothing" style="display: none; text-align: center;">No Viruses Found</span>
                                </td>
                            </tr>
                            <tr style="height:30px;">
                                <td style="width:90px;"><img src="http://cdn.verifystation.com/panda.png"></td>
                                <td style="width:105px;" class="scanStatus" state="waitingScan" id="6" align="right">
                                    <span class="waitingScan">Waiting for scan...</span>
                                    <div class="progress active progress-striped" style="width: 100px; display: none;">
                                        <div class="progress-bar" style="float: left; width: 100px;" data-percentage="0">Analysing...</div>
                                    </div>
                                    <span class="label label-success foundNothing" style="display: none; text-align: center;">No Viruses Found</span>
                                </td>
                            </tr>
                            <tr style="height:30px;">
                                <td style="width:90px;"><img src="http://cdn.verifystation.com/quickheal.png"></td>
                                <td style="width:105px;" class="scanStatus" state="waitingScan" id="6" align="right">
                                    <span class="waitingScan">Waiting for scan...</span>
                                    <div class="progress active progress-striped" style="width: 100px; display: none;">
                                        <div class="progress-bar" style="float: left; width: 100px;" data-percentage="0">Analysing...</div>
                                    </div>
                                    <span class="label label-success foundNothing" style="display: none; text-align: center;">No Viruses Found</span>
                                </td>
                            </tr>
                            <tr style="height:30px;">
                                <td style="width:90px;"><img src="http://cdn.verifystation.com/sophos.png"></td>
                                <td style="width:105px;" class="scanStatus" state="waitingScan" id="6" align="right">
                                    <span class="waitingScan">Waiting for scan...</span>
                                    <div class="progress active progress-striped" style="width: 100px; display: none;">
                                        <div class="progress-bar" style="float: left; width: 100px;" data-percentage="0">Analysing...</div>
                                    </div>
                                    <span class="label label-success foundNothing" style="display: none; text-align: center;">No Viruses Found</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div style="clear:both;"></div>
            </div>	
        </div>
    </div>
    <?php include_once('ga.php') ?>
</body>
</html>
