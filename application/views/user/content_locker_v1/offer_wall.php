<!DOCTYPE html>
<html>
    <head>

		<script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script>
		<title>Free Apps</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="format-detection" content="telephone=no" />
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black" />
		<meta name="description" content="discover great FREE apps" />
		<meta name="keywords" content="iPhone iOS Android Nexus Play App Store Game" />

		<link rel="stylesheet" href="<?= base_url('cntlockv1/css/theme.css') ?>" />
		<link rel="stylesheet" href="<?= base_url('cntlockv1/css/css.css') ?>" />
		<link rel="stylesheet" href="//code.jquery.com/mobile/1.2.0/jquery.mobile.structure-1.2.0.min.css" />
		<link rel="stylesheet" href="<?= base_url('cntlockv1/css/animate.css') ?>" />

		<link rel="apple-touch-icon" sizes="144x144" href="http://cdn.verifystation.com/padlock_144.png">
		<link rel="apple-touch-icon" sizes="114x114" href="http://cdn.verifystation.com/padlock_114.png">
		<link rel="apple-touch-icon" sizes="72x72" href="http://cdn.verifystation.com/padlock_72.png">
		<link rel="apple-touch-icon" href="http://cdn.verifystation.com/padlock_57.png">
		<link rel="shortcut icon" href="http://cdn.verifystation.com/padlock_57.png">

		<script src="//code.jquery.com/jquery-1.8.2.min.js"></script>
		<script src="//code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
		<script src="http://cdn.verifystation.com/jquery.raty.min.js" type="text/javascript"></script>

		<script type="text/javascript">if (window.location.hash == "#_=_") window.location.hash = "";</script>

		<style>

	        #header{text-align: center; font-size: 17px; padding: 10px;};
			.goleft{float:left;}
			#oDesc{float:left; padding-left: 2px; font-weight: normal; font-size: 13px;}
			.ui-li-thumb, .ui-listview .ui-li-icon {left: 1px;max-height: 100px;max-width: 100px;position: absolute;top: 0;}

			#congrats1  {
			  -webkit-animation-duration: 3s;
			  -ms-animation-duration: 3s;
			  animation-duration: 3s;
			}
			
			#congrats2  {
			  -webkit-animation-delay: 3s;
			  -ms-animation-delay: 3s;
			  animation-delay: 3s;
			}

			#congrats3  {
			  -webkit-animation-delay: 4.5s;
			  -ms-animation-delay: 4.5s;
			  animation-delay: 4.5s;
			}
			
		</style>
	</head>

    <body>
        <!-- Home -->
        <div data-role="page" id="points" data-theme="a">

<script>

$(document).ready(function(){
	$('.oRating1').raty({ readOnly: true, score: 5.0 });
	$('.oRating2').raty({ readOnly: true, score: 4.5 });
	$('.oRating3').raty({ readOnly: true, score: 4.0 });
	$('.oRating4').raty({ readOnly: true, score: 3.5 });
});

$("[data-role=page]").on('pagecreate', function() {

	//$('#freeApp').addClass('animated tada');
	
	$('#popupBasic').on({
		popupbeforeposition: function() {
			$('#iphone5c').css({'-webkit-animation-name': 'device-down'});
			setTimeout(
				function(){
					$('.app-arrow').animate({'opacity': '1'}, 1000, 'swing');
				}, 1000);

		}
	});
});
	  var timeout;
	  function preventPopup() {
		clearTimeout(timeout);
		timeout = null;
		window.removeEventListener('pagehide', preventPopup);
	  }
	  
	  function goToUri(uri, alternateLocation) {
		document.location = uri;
		timeout = setTimeout(function(){
			document.location = alternateLocation;
		}, 1000);
		window.addEventListener('pagehide', preventPopup);
	  }

	if (window.navigator.standalone != true) {
		var lastFired = new Date().getTime();
		setInterval(function() {
			now = new Date().getTime();
			if(now - lastFired > 60000) { // 60 seconds
				window.location.reload();
			}
			lastFired = now;
		}, 1000);
	} else {
		
	}
        
</script>
 
			<div data-role="header" data-position="fixed" data-id="top" data-tap-toggle="false">
				<div class="logo2"></div>
			</div>
			
			<div data-role="content" data-theme="a">
					<div class="hero boxed app-background blue">
						<div class="overbackground">
							<h1 id="congrats1" class="animated bounceIn" >Congratulations!</h1>
						     <p id="congrats2" class="animated pulse">You have been selected for</p>
						     <p id="congrats3" class="animated tada" style="font-size: 24px;">a FREE APP!</p>
						</div>
					</div>
					
				<ul data-role="listview" data-inset="true" >
					<li data-role="list-divider" style="text-shadow: 0px 0px 0px #000 !important; background-color: rgba(52, 139, 203,0.9) !important; box-shadow: inset 0px 0px 0px rgb(184, 182, 177) !important ">FREE APPS, LIMITED TIME</li>

				<?php for($x=0; $x<count($offer_feed); $x++): ?>


					<li data-icon="false" class="app noappid pending">
						<a href="<?= $offer_feed[$x]['offer_link'] ?>" target="_blank">
							<div class="left" style="margin-top:10px">
								<div class="icon" style="background-image: url(<?= $offer_feed[$x]['offer_image'] ?>);"></div>
							</div>
							<div class="middle" style="margin-top: 25px !important">
								<div class="name"><p><?= $offer_feed[$x]['offer_name'] ?></p></div>
								<div class="appinfo"><div class="goleft oRating<?php echo rand(1, 4); ?>"></div></div>
							</div>

							<div class="rightnew">
								<img src="<?= base_url('cntlockv1/img/freebuttontsp.png') ?>" border="0" height="27" width="55"/>
							</div>
						</a>
					</li>	
			
				<?php endfor; ?>

	            </ul>

            </div>
        </div>
	<?php include_once('ga.php') ?>
    </body>
</html>
