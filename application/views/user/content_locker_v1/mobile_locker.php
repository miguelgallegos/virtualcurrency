<?php if($content_locker_fn == 'script'){?>
	window.location = '<?php echo base_url() . uri_string() . "?" . str_replace('script', 'direct', $_SERVER['QUERY_STRING'])?>';
<?php exit();?>	
<?php }?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Content Locked</title>

	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.1.1/jquery.mobile-1.1.1.min.css" />
	<link rel="apple-touch-icon" sizes="144x144" href="http://cdn.verifystation.com/padlock_144.png">
   	<link rel="apple-touch-icon" sizes="114x114" href="http://cdn.verifystation.com/padlock_114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="http://cdn.verifystation.com/padlock_72.png">
   	<link rel="apple-touch-icon" href="http://cdn.verifystation.com/padlock_57.png">
	<link rel="shortcut icon" href="http://cdn.verifystation.com/padlock_57.png">

	<style type="text/css">
        #header{text-align: center; font-size: 17px; padding: 10px;};
		.goleft{float:left;}
		#oDesc{float:left; padding-left: 2px; font-weight: normal; font-size: 13px;}
		.ui-li-thumb, .ui-listview .ui-li-icon {left: 1px;max-height: 100px;max-width: 100px;position: absolute;top: 0;}
		.ui-btn-text { font-size: 10px; }
    </style>

    <script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.1.1/jquery.mobile-1.1.1.min.js"></script>
    <script src="http://cdn.verifystation.com/jquery.raty.js" type="text/javascript"></script>

	<script type="text/javascript">
        
        function conversion_check() {
                setTimeout("conversion_check();", 20000);
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('ajax/clv1_conversion_check') ?>",
                    dataType: "json",
                    data: {
                        type: 		"mobile",
                        aff_id: 	"<?= $widget_settings['affiliate_id']; ?>",
                        w_id: 		"<?= $widget_settings['widget_id']; ?>",
                        ip: 		"<?= $widget_settings['user_ip']; ?>",
                        conv_req: 	"<?= $widget_settings['conversions_required']; ?>",
                        o_count: 	"<?= count($offer_feed); ?>", 
                        red_url: 	"<?= base64_encode($widget_settings['redirect_url']); ?>",
                        <?= $this->security->get_csrf_token_name() ?>: "<?= $this->security->get_csrf_hash() ?>"                        
                    },
                    success: function(data) {
                    	// If redirect is not blank, send browser to that URL
                    	if(data.redirect != '')
                    		window.location.replace(data.redirect);
                    	else
                    	{
                    		// If the page doesn't redirect, update the total number of conversions required
                    		$('#app_download_status').show();
                    		$('#app_download_status_count').text(data.offer_ids.length);
                    		$('#app_download_status_required').text("<?= $widget_settings['conversions_required']; ?>");

                    		$.each(data.offer_ids, function(index, value) {
                    			$('#' + value).addClass('ui-disabled').attr('href','#');
                    		})
                    	}
                    }
                })
        }

    	$(document).ready(function(){
		    $('.oRating1').raty({ readOnly: true, score: 5.0, path: "http://cdn.verifystation.com/" });
		    $('.oRating2').raty({ readOnly: true, score: 4.5, path: "http://cdn.verifystation.com/" });
		    $('.oRating3').raty({ readOnly: true, score: 4.0, path: "http://cdn.verifystation.com/" });
		    $('.oRating4').raty({ readOnly: true, score: 3.5, path: "http://cdn.verifystation.com/" });
    	});
	</script>
</head>

<body>

	<div data-role="page">
		<div data-role="header" data-theme="b" id="header"><?= $widget_settings['title'] ?></div>

		<div data-role="content">
			<div style="text-align:center;">

                <span id="app_download_status" style="color:green; font-weight: bold; text-decoration: underline; display: none;">
                <span id="app_download_status_count"></span> of <span id="app_download_status_required"></span> Apps Downloaded so far!</span>

				<p style="margin-top: 5px;"><?= $widget_settings['message'] ?></p>
			</div>

			<br />

			<ul data-role="listview">

				<?php for($x=0; $x<count($offer_feed); $x++): ?>
					<li id="<?= $offer_feed[$x]['offer_id'] ?>">
						<a class="oLink" onclick="conversion_check();" href="<?= $offer_feed[$x]['offer_link'] ?>" target="_blank">
							<img id="oThumb" width="74" height="74" src="<?= $offer_feed[$x]['offer_image'] ?>">
							<h3 id="oName"><?= $offer_feed[$x]['offer_name'] ?></h3>
							<div class="goleft oRating<?= rand(1,4)?>"></div>
						</a>
					</li>				
				<?php endfor; ?>

			</ul>

			<br>

			<div style="text-align:center;">
				<p>Checking Completion</p>
				<p><img width="220" height="19" src="http://cdn.verifystation.com/checking.gif"></p>
				<p>After Completion This Page Will Unlock!</p>
			</div>
			

			<?php if($widget_settings['affiliate_id'] != '1124'): // 1124 is the account for Andy McCune / Coast 9 ?>		
			<div data-role="footer" data-id="foo1" data-position="fixed">
				<div data-role="navbar">
					<ul>
						<li><a href="http://affiliates.theblumarket.com/terms" target="_blank">Terms of Service</a></li>
						<li><a href="http://theblumarket.com" target="_blank">Make Money. Sign Up Now</a></li>
					</ul>
				</div><!-- /navbar -->
			</div><!-- /footer -->
			<?php endif ?>



		</div>
	</div>	
	<?php include_once('ga.php') ?>
</body>
</html>	