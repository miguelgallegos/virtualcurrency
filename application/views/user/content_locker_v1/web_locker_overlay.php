<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <style type="text/css">
        body{margin: 125px 0px 0px 0px; overflow: hidden;}
        #widget_main{width: 550px; height: 275px; margin:0px auto; background: transparent url('<?= $widget_settings['background_url']; ?>') no-repeat scroll center top;}
        #widget_title{padding-top: 20px; font-family: Tahoma; font-size: 18px;}
        #widget_message{padding-top: 18px; font-family: Tahoma; font-size: 15px;}
        #widget_offers{height: 75px; padding: 15px 80px; font-family: Tahoma; font-size: 14px;}
        ul{list-style-type: none; margin: 0px; padding: 0px;}
        li{margin-top: 4px;}
        li a{text-decoration: none; cursor: pointer; color: #4485f3; font-weight: bold;}
        #widget_completion{padding-top: 15px; font-family: Tahoma; font-size: 12px}
        #completion_loading{margin-top: 5px; background: url("http://cdn.verifystation.com/checking.gif") no-repeat scroll center top; height: 20px; width: 220px;}
    </style>

    <script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    
    <script type="text/javascript">
        function conversion_check() {
                setTimeout("conversion_check();", 20000);
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('ajax/clv1_conversion_check') ?>",
                    dataType: "json",
                    data: {
                        type:       "web",
                        aff_id:     "<?= $widget_settings['affiliate_id']; ?>",
                        w_id:       "<?= $widget_settings['widget_id']; ?>",
                        ip:         "<?= $widget_settings['user_ip']; ?>",
                        conv_req:   "1",
                        o_count:    "<?= count($offer_feed); ?>", 
                        red_url:    "<?= base64_encode($widget_settings['redirect_url']); ?>",
                        <?= $this->security->get_csrf_token_name() ?>: "<?= $this->security->get_csrf_hash() ?>"                        
                    },
                    success: function(data) {
                        // If redirect is not blank, send browser to that URL
                        if(data.redirect != '')
                            window.top.location.replace(data.redirect);
                    }
                })
        }
    </script>

</head>

<body oncontextmenu="return false;">

    <div id="widget_main" align="center">
        <div id="widget_title"><?= $widget_settings['title']; ?></div>
        <div id="widget_message"><?= $widget_settings['message']; ?></div>
        <div id="widget_offers">
            <ul>
				<?php foreach($offer_feed as $offer): ?>
					<li id="{$offer['offer_id']}">
						<a onclick="conversion_check();" href="<?= $offer['offer_link'] ?>" target="_blank"><?= $offer['offer_name'] ?></a>
					</li>				
				<?php endforeach; ?>
            </ul>
        </div>
        <div id="widget_completion">
            <span>Checking For Completion.....</span>
            <div id="completion_loading"></div>
        </div>
    </div>
<?php include_once('ga.php') ?>
</body>
</html>
