<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script>
		<title><?= $lp_settings['lp_name']?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="format-detection" content="telephone=no" />
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black" />		
		<meta name="description" content="discover great FREE apps" />
		<meta name="keywords" content="iPhone iOS Android Nexus Play App Store Game" />

		<link rel="stylesheet" href="<?= base_url('cntlockv1/css/theme.css') ?>" />
		<link rel="stylesheet" href="<?= base_url('cntlockv1/css/css.css') ?>" />
		<link rel="stylesheet" href="//code.jquery.com/mobile/1.2.0/jquery.mobile.structure-1.2.0.min.css" />
		<link rel="stylesheet" href="<?= base_url('cntlockv1/css/animate.css') ?>" />

		<link rel="apple-touch-icon" sizes="144x144" href="http://cdn.verifystation.com/padlock_144.png">
		<link rel="apple-touch-icon" sizes="114x114" href="http://cdn.verifystation.com/padlock_114.png">
		<link rel="apple-touch-icon" sizes="72x72" href="http://cdn.verifystation.com/padlock_72.png">
		<link rel="apple-touch-icon" href="http://cdn.verifystation.com/padlock_57.png">
		<link rel="shortcut icon" href="http://cdn.verifystation.com/padlock_57.png">

		<script type="text/javascript">if (window.location.hash == "#_=_") window.location.hash = "";</script>

		<style>
			img {				
		 		max-width:640px;
		 		width: 100%;
		  		height:auto;
			}	
		</style>
	</head>	

	<body style="<?= $lp_settings['lp_bgcolor'] ?>">
        <!-- Home -->
		<div data-role="page" id="points" data-theme="a">
			<div data-role="content" data-theme="a">
				<div class="image" style="text-align: center;">
					<a href="<?= $lp_settings['gotoURL'] ?>">
					<img src="<?= $lp_settings['lp_image_url'] ?>" alt="<?= $lp_settings['lp_name'] ?>" />
					</a>
				</div>
		    </div>
		</div>		
		<?php include_once('ga.php') ?>
    </body>    
</html>

