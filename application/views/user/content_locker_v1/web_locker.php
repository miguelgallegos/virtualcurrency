<?php if(empty($content_locker_fn) || $content_locker_fn == 'direct'): ?><script type="text/javascript"><?php endif; ?>

var settings = { 
    "load_time":250,
    "opacity":75
};

<?php if($content_locker_fn == 'onclick'): ?> 
    var onclick_setting = "1";
<?php else: ?> 
    var onclick_setting = "0";
<?php endif; ?>

function page_setup() {
    var a = document['getElementsByTagName']('body')['item'](0);
    if (a == null) {
        setTimeout('page_setup();', 100)
    } else {
        cover_setup()
    }
};

function load_widget() {
    setTimeout('frame_show();', settings['load_time'])
}
function page_height() {
    var a = document;
    var b = a['body'];
    var c = a['documentElement'];
    return Math['max'](b['scrollHeight'], b['offsetHeight'], b['clientHeight'], c['offsetHeight'], c['scrollHeight'], c['clientHeight'])
};

function cover_setup() {
    var a = document['getElementsByTagName']('body')['item'](0);
    var b = document['createElement']('div');
    b['setAttribute']('id', 'cover');
    a['insertBefore'](b, a['firstChild']);
    document['getElementById']('cover')['style']['filter'] = 'alpha(opacity=' + settings['opacity'] + ')';
    document['getElementById']('cover')['style']['opacity'] = (settings['opacity'] / 100);
    b['style']['height'] = page_height() + 'px';
    if (onclick_setting != "1") scroll_hook();
    setTimeout('frame_setup();', 250)
};

function scroll_hook() {
    scroll(0, 0);
    if (document['getElementById']('cover')) {
        cover_height = document['getElementById']('cover')['style']['height']['replace']('px', '');
        if ((page_height() - cover_height) > 30) {
            document['getElementById']('cover')['style']['height'] = (page_height() + 'px')
        }
    };
    setTimeout('scroll_hook();', 300)
};

function frame_setup() {
    document['getElementById']('widget_frame')['contentWindow']['location']['replace']('<?= base_url("clv1/content_locker_web?" . $query_string)?>')
};

function frame_show() {
    document['getElementById']('cover')['style']['display'] = 'block';
    document['getElementById']('cover')['style']['visibility'] = 'visible';
    document['getElementById']('widget_frame')['style']['display'] = 'block';
    document['getElementById']('frame_main')['style']['display'] = 'block';
    scroll(0, 0)
};

function frame_hide() {
    document['getElementById']('cover')['style']['display'] = 'none';
    document['getElementById']('cover')['style']['visibility'] = 'none';
    document['getElementById']('widget_frame')['style']['display'] = 'none';
    document['getElementById']('frame_main')['style']['display'] = 'none';
    scroll(0, 0)
};

document['write']('<style type="text/css">');
document['write']('#cover{display:none; position:absolute; top: 0px; left: 0px; z-index: 22220; width: 100%; background-color: #000; filter:alpha(opacity=0); opacity: 0.0; -moz-opacity: 0.0;}');
document['write']('#frame_main{display:none; text-align: center; line-height: normal; position:absolute; top: 0px; left: 0px; z-index: 22222; width: 100%; height: 674px;}');
document['write']('#frame_holder{background: transparent; height: 768px; z-index: 22224;}');
document['write']('#widget_frame{position: relative; width: 100%; height: 768px; overflow-x: hidden; overflow-y: hidden; background-color: transparent; z-index: 22227;}');
document['write']('#close_button {float: right; margin-top: 10px; margin-right: 10px; width: 90px; text-align: left;}');
document['write']('#close_button a {display: block; height: 30px; font-family: arial, san-serif; color: white; font-size: 18px; text-decoration: none; background: url("http://cdn.verifystation.com/close.png") no-repeat scroll 63px -2px transparent;}');
document['write']('</style>');
document['write']('<div id="frame_main">');
if (onclick_setting == "1") 
    document['write']('<div id="close_button"><a href="#" onclick="frame_hide()">CLOSE</a></div>');
document['write']('<div id="frame_holder">');
document['write']('<iframe id="widget_frame" src="" allowtransparency="true" frameborder="0" scrolling="no"></iframe>');
document['write']('</div></div>');
if (onclick_setting == "0") 
    setTimeout('frame_show();', settings['load_time']);

window['onload'] = page_setup;


<?php if(empty($content_locker_fn) || $content_locker_fn == 'direct'): ?></script><?php endif; ?>
