<h1 class="app-name">Virtual Currency</h1>

<div class="scrollable sidebar-scrollable">
  <div class="scrollable-content">
    <div class="list-group" toggle="off" bubble target="mainSidebar">
      <a class="list-group-item" href="#/offers">Offers <i class="fa fa-chevron-right pull-right"></i></a>
      <a class="list-group-item" href="#/rewards">Rewards <i class="fa fa-chevron-right pull-right"></i></a>      
      <a class="list-group-item" href="#/invite"> Invite <i class="fa fa-chevron-right pull-right"></i></a>
      <a class="list-group-item" href="#/settings">Settings <i class="fa fa-chevron-right pull-right"></i></a>
    </div>

  </div>
</div>