<!doctype html>
<html lang="en" ng-app="myApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

<!--
    <link href="topcoat/css/topcoat-mobile-light.min.css" rel="stylesheet">
 -->   

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="msapplication-tap-highlight" content="no">

    <!--<link href="chui/chui-ios-3.8.0.min.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">
    -->
    <link rel="stylesheet" href="css/mobile-angular-ui-hover.min.css" />
    <link rel="stylesheet" href="css/mobile-angular-ui-base.min.css" />
    <link rel="stylesheet" href="css/mobile-angular-ui-desktop.min.css" />
    <link rel="stylesheet" href="css/jpnc.css" />

    <!-- PUT app specific css here -->

<!--
    <script src="lib/angular.js"></script>
    <script src="lib/angular-touch.js"></script>
    <script src="lib/angular-resource.js"></script>
    <script src="lib/angular-animate.js"></script>
    <script src="lib/angular-route.js"></script>
    -->

    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular-route.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular-resource.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular-animate.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular-touch.min.js"></script>
    <script src="lib/mobile-angular-ui.min.js"></script>

    <script src="js/app.js"></script>
    <script src="js/controllers.js"></script>
    <!--<script src="js/memory-services.js"></script>-->
    
    <script src="js/rest-services.js"></script>


    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <!--<script src="chui/chui-3.8.0.min.js"></script>-->
    <script src="js/jpnc.js"></script>

</head>

<body ng-controller="MainCtrl">

<!-- Sidebars -->
<div ng-include="'app/sidebar'" class="sidebar sidebar-left" toggleable parent-active-class="sidebar-left-in" id="mainSidebar"></div>

<div ng-include="'app/sidebarRight'" class="sidebar sidebar-right" toggleable parent-active-class="sidebar-right-in" id="rightSidebar"></div>

    <div class="app">

      <!-- Navbars -->

      <div class="navbar navbar-app navbar-absolute-top">
        <div class="navbar-brand navbar-brand-center" yield-to="title">
          <span>Virtual Currency</span>
        </div>
        <div class="btn-group pull-left">
          <div ng-click="toggle('mainSidebar')" class="btn btn-navbar sidebar-toggle">
            <i class="fa fa-bars"></i> Menu
          </div>
        </div>
        <div class="btn-group pull-right" yield-to="navbarAction">

            <div class="btn btn-navbar">
                <span class="badge badge-primary">{{accDetails.credit}}</span>
            </div>
        <!--  <div ng-click="toggle('rightSidebar')" class="btn btn-navbar">
            <i class="fa fa-comment"></i> Chat
          </div>
          -->
        </div>
      </div>

      <div class="navbar navbar-app navbar-absolute-bottom">
        <div class="btn-group justified">
          <a href="http://mobileangularui.com/" class="btn btn-navbar"><i class="fa fa-home fa-navbar"></i> Docs</a>
          <a href="https://github.com/mcasimir/mobile-angular-ui" class="btn btn-navbar"><i class="fa fa-github fa-navbar"></i> Sources</a>
          <a href="https://github.com/mcasimir/mobile-angular-ui/issues" class="btn btn-navbar"><i class="fa fa-exclamation-circle fa-navbar"></i> Issues</a>
        </div>
      </div>

      <!-- App Body -->
      <div class="app-body" ng-class="{loading: loading}">
        <div ng-show="loading" class="app-content-loading">
          <i class="fa fa-spinner fa-spin loading-spinner"></i>
        </div>

        <ng-view class="app-content" ng-hide="loading"></ng-view>
      </div>

    </div><!-- ~ .app -->


<!--
<div ng-view ng-class="slide"></div>
-->



</body>
</html>