<?php if (!defined('BASEPATH')) die();

class Locker_controller extends MY_Controller {

    // **********************************************************************
    //  Private Functions
    //  Functions used in this class
    // **********************************************************************
    private function load_locker($locker_type, $category=FALSE)
    {
        // Load locker_model
        $this->load->model('user/content_locker_v1/locker_model');

        // Retrieve Model Data
        $model_data = $this->locker_model->load_locker($locker_type, $category);

        // Load the web locker view (This will load the overlay in an iframe)
        $this->load->view($model_data['view'], $model_data['data']);        
    }


    // **********************************************************************
    //  Public Functions
    //  Functions called from the address bar
    // **********************************************************************
    public function content_locker() 
    {
        $this->load_locker('content_locker');
    }

    public function content_locker_web()
    {
        $this->load_locker('clv1_web_overlay');        
    }

    public function file_locker() 
    {
        $this->load_locker('file_locker');        
    }

    public function my_url()
    {
        $this->load_locker('super_url');
    }

    public function offer_wall()
    {
        $this->load_locker('offer_wall', HO_CATEGORY_OFFER_WALL_NONINCENT);
    }    

    public function landing_page()
    {
        $this->load_locker('landing_page');
    }

    public function redirect_to_global()
    {
        redirect(GLOBAL_REDIRECT_URL);
    }


}
