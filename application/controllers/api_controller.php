<?php 
include_once(APPPATH.'libraries/REST_Controller.php');
//mg - copied to lib and created this as second step
class Api_controller extends REST_Controller{ //REST_Controller moved to core, to see if it works originally suggested in libraries

    //provides interface for list and details, this service is called by angularjs
    public function offers_get()
    {
        //this method will be providing offers that gets from CL2 API - Offers
        $this->load->model('common/offer_service_model');

        if($this->get('id')){            
            $offer = $this->offer_service_model->get_offer_by_id($this->get('id'));        
        
            $this->response($offer, 200);
        }
        else if(!$this->get('id'))
        {
            $offers = $this->offer_service_model->get_offers();            
        
            $this->response($offers, 200);
        }
        else
        {
            $this->response(NULL, 404);
        } 
    }

    public function rewards_get()
    {
        //this method will be providing offers that gets from CL2 API - Offers
        $this->load->model('common/rewards_model');

        if($this->get('id')){            
            $reward = $this->rewards_model->getById($this->get('id'));        
        
            $this->response($reward, 200);
        }
        else if(!$this->get('id'))
        {
            $rewards = $this->rewards_model->get();
        
            $this->response($rewards, 200);
        }
        else
        {
            $this->response(NULL, 404);
        } 
    }

    //need to build a locker for VC items
    //instead of regular offers, return offers with the appropriate URL for tracking
    //
    //params:
    //affiliate id
    //dates
    function conversions_get()
    {
        if(!$this->get('id'))
        {
            $this->response(NULL, 400);
        }
 
        //$user = $this->user_model->get( $this->get('id') );
        $conversions = array('id' => 1, 'company' => 'Test Company', 'clicks' => 100, 'conversions' => '125', 'revenue' => 3400);
         
        if($conversions)
        {
            $this->response($conversions, 200); // 200 being the HTTP response code
        }
 
        else
        {
            $this->response(NULL, 404);
        }
    }    

    function acc_get(){        

        $this->load->model('common/bank_model');
        $credit = $this->bank_model->credit();
        $data = array('credit' => $credit);

        $this->response($data, 200);

    }

    //params:
    //type cl, fl, lp, wl
    //name, urls, images??
    public function locker_post()
    {
    	/* -rework
    	//receive all creation params for a 
		$result = $this->user_model->update( $this->post('id'), array(
            'name' => $this->post('name'),
            'email' => $this->post('email')
        ));
         
        if($result === FALSE)
        {
            $this->response(array('status' => 'failed'));
        }
         
        else
        {
            $this->response(array('status' => 'success'));
        }
        */
    }	

}