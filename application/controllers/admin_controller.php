<?php if (!defined('BASEPATH')) die();

class Admin_controller extends MY_Controller {

    function __construct()
    {
        parent::__construct();

        // Check to make sure user is logged in
        if (!$this->ion_auth->logged_in())
        {
            //redirect them to the login page
            redirect('login');
        }
        elseif (!$this->ion_auth->is_admin()) //remove this elseif if you want to enable this for non-admins
        {
            //redirect them to the home page because they must be an administrator to view this
            return show_error('You must be an administrator to view this page.');
        }
    }


    // **********************************************************************
    //  Private Functions
    //  Functions used within this controller only
    // **********************************************************************

    private function load_admin_view($admin_view_data)
    {
        $this->data['admin_view_head'] = $this->load->view('admin/includes/head', $admin_view_data, TRUE);
        $this->data['admin_view_logo'] = $this->load->view('admin/includes/logo', '', TRUE);
        $this->data['admin_view_footer_scripts'] = $this->load->view('admin/includes/footer_scripts', '', TRUE);

        $this->data['admin_view_nav_breadcrumbs'] = $this->load->view('admin/includes/nav_breadcrumbs', $admin_view_data, TRUE);
        $this->data['admin_view_nav_sidebar'] = $this->load->view('admin/includes/nav_sidebar', $admin_view_data, TRUE);

        $this->data['content'] = $admin_view_data['content'];

        $this->load->view('admin/layouts/admin_layout_full', $this->data);
    }

    // **********************************************************************
    //  Public functions 
    //  Each function corresponds to a page
    // **********************************************************************

    /*******************
     * Dashboard
     *******************/
    public function dashboard()
    {        
        // Set the current page
        $admin_view_data['current_page'] = 'dashboard';

        // Set content for this page
        $admin_view_data['content'] = $this->load->view('admin/content/dashboard', '', TRUE);

        // Load the admin template with the content and data
        $this->load_admin_view($admin_view_data);
    }


    /*******************
     * Manage Users
     *******************/
    public function manage_users($fn, $id='')
    {
        // Load click_manager
        $this->load->model('common/auth_model');

        // Load language files for the user management views
        $this->load->helper('language');        

        switch($fn)
        {
            case 'accounts':
                // Get User Account Data
                $admin_view_data = $this->auth_model->manage_user_accounts();

                // Set the current page
                $admin_view_data['current_page'] = 'manage_user_accounts';

                // Set content for this page
                $admin_view_data['content'] = $this->load->view('admin/content/manage_user_accounts', $admin_view_data, TRUE);

                // Load the admin template with the content and data
                $this->load_admin_view($admin_view_data);                                   

                break;

            case 'groups':
                // Get User Account Data
                $admin_view_data = $this->auth_model->manage_user_groups();

                // Set the current page
                $admin_view_data['current_page'] = 'manage_user_groups';

                // Set content for this page
                $admin_view_data['content'] = $this->load->view('admin/content/manage_user_groups', $admin_view_data, TRUE);

                // Load the admin template with the content and data
                $this->load_admin_view($admin_view_data);  

                break;

            case 'create_group':
                // Get User Account Data
                $admin_view_data = $this->auth_model->create_group();

                // Set the current page
                $admin_view_data['current_page'] = 'manage_user_groups_create';

                // Set content for this page
                $admin_view_data['content'] = $this->load->view('admin/content/manage_user_groups_create', $admin_view_data, TRUE);

                // Load the admin template with the content and data
                $this->load_admin_view($admin_view_data);                  

                break;

            case 'edit_group':
                // Get User Account Data
                $admin_view_data = $this->auth_model->edit_group($id);

                // Set the current page
                $admin_view_data['current_page'] = 'manage_user_groups_edit';

                // Set content for this page
                $admin_view_data['content'] = $this->load->view('admin/content/manage_user_groups_edit', $admin_view_data, TRUE);

                // Load the admin template with the content and data
                $this->load_admin_view($admin_view_data);                  

                break;

            case 'delete_group':
                $this->auth_model->delete_group($id);
                break;

            case 'activate_user':                
                $this->auth_model->activate_user($id);
                break;

            case 'deactivate_user':
                $this->auth_model->deactivate_user($id);
                break;

            case 'create_user':
                // Get User Account Data
                $admin_view_data = $this->auth_model->create_user();

                // Set the current page
                $admin_view_data['current_page'] = 'manage_user_accounts_create';

                // Set content for this page
                $admin_view_data['content'] = $this->load->view('admin/content/manage_user_accounts_create', $admin_view_data, TRUE);

                // Load the admin template with the content and data
                $this->load_admin_view($admin_view_data);  

                break;

            case 'edit_user':
                // Get User Account Data
                $admin_view_data = $this->auth_model->edit_user($id);

                // Set the current page
                $admin_view_data['current_page'] = 'manage_user_accounts_edit';

                // Set content for this page
                $admin_view_data['content'] = $this->load->view('admin/content/manage_user_accounts_edit', $admin_view_data, TRUE);

                // Load the admin template with the content and data
                $this->load_admin_view($admin_view_data);  

                break;

            case 'delete_user':
                $this->auth_model->delete_user($id);
                break;

            default:
                redirect('/');
                break;

        }

    }



    /*******************
     * Offer Pages
     *******************/
    public function manage_offers()
    {        
        // Set the current page
        $admin_view_data['current_page'] = 'manage_offers';

        // Get model data
        $this->load->model('admin/dashboard_model');
        $admin_view_data['database'] = $this->dashboard_model->get_database_dump();      

        // Set content for this page
        $admin_view_data['content'] = $this->load->view('admin/content/manage_offers', $admin_view_data, TRUE);

        // Load the admin template with the content and data
        $this->load_admin_view($admin_view_data);        
    }       


    /*******************
     * Statistics
     *******************/
    public function stats()
    {        
        // Set the current page
        $admin_view_data['current_page'] = 'stats';

        // Load click_manager
        $this->load->model('common/click_manager_model');        

        // Retrieve click profile for user agent
        $click_data = $this->click_manager_model->get_click_profile();

        // Load Content Locker Data
        $this->load->model('common/offer_server_model');
        $admin_view_data['get_offers'] = $this->offer_server_model->get_offers(FALSE, $click_data);

        // Set content for this page
        $admin_view_data['content'] = $this->load->view('admin/content/stats', $admin_view_data, TRUE);

        // Load the admin template with the content and data
        $this->load_admin_view($admin_view_data);
    }       

    /*******************
     * Sync Tool Pages
     *******************/
    public function sync_tool()
    {
        // Set the current page
        $admin_view_data['current_page'] = 'sync_tool';

        $this->load->model('user/content_locker_v1/locker_model');        
        $this->load->database();

        //$admin_view_data['v1_lockers'] = $this->locker_model->retrieve_content_locker_v1_for_check('user_widgets');
        $admin_view_data['v1_lockers'] = array();

        // Set content for this page
        $admin_view_data['content'] = $this->load->view('admin/content/sync_tool', $admin_view_data, TRUE);        
        

        // Load the admin template with the content and data
        $this->load_admin_view($admin_view_data);
    }


    /*******************
     * Update Pages
     *******************/
    public function update()
    {
        // Set the current page
        $admin_view_data['current_page'] = 'update';

        $this->load->model('admin/dashboard_model');        
        $admin_view_data['update_time'] = $this->dashboard_model->update();

        // Set content for this page
        $admin_view_data['content'] = $this->load->view('admin/content/update', $admin_view_data, TRUE);        

        // Load the admin template with the content and data
        $this->load_admin_view($admin_view_data);
    }


}
