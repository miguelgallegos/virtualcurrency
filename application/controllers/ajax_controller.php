<?php if (!defined('BASEPATH')) die();

class Ajax_controller extends MY_Controller {

    // **********************************************************************
    //  Public functions 
    //  Functions used by routes.php
    // **********************************************************************



	//check params:
	//aff_id
	//offer_id
	// transaction ID is getting checked when a conversion occurs
	public function conversion_check(){

		$this->output->enable_profiler(false);

		//$aff_id = 3212;//get from session when available
		//use ip address too?
		$download_id = $this->input->get('did');
		//$download_id = 'mydownloadid999';
		//check offers_service
		$os = new Offers_Service_api();
		$os->prepare('conversions', 'findByDownloadId'); //FIND by download id, execution, label id, etc!
		//ON DISPLAY, show the new downloadId appended to the offer URL or generate on AJAX?
		$os->addParameter('download_id', $download_id);
		//$os->addParameter('affiliate_id', $aff_id);
		$res = $os->execute(false, false);
			
		//write to bank if success
		if($res['response']['data']){
			$this->load->model('common/bank_model');

			$offer_points = $res['response']['data']['offer_points'];
			$offer_id = $res['response']['data']['offer_id'];

			$this->bank_model->addTransaction('credit', $offer_points, $offer_id ,$download_id);
		}

		echo json_encode($res['response']['data']);        
	}

	public function redeemReward(){
		$this->output->enable_profiler(false);
		$this->load->model('common/bank_model');
		$this->load->model('common/rewards_model');

		$reward_id = $this->input->get('rid');

		$c = $this->bank_model->credit(); //TODO: User ID still hardcoded!!!
		$r = $this->rewards_model->getById($reward_id);
		$data = array();
		if($c - $r['points'] >= 0){
			$data['redeemed'] = true;
			$data['message'] = 'success';

			//$type, $points, $item_id, $widget_id			
			$this->bank_model->addTransaction('debit', $r['points'], $r['id'], ''); //rewards don't go through widget id conversion system

			$data['credit'] = $this->bank_model->credit(); //recalculates

		}else{
			$data['redeemed'] = false;
			$data['message'] = 'You need ' . abs($c - $r['points']) . ' more points to redeem this reward.';
		}

		echo json_encode($data);
	}

	//// DELETE NON USED BELOW:


    /**
     * verify_redirect_url()
     * Verify if the redirect_url exists
     */
	public function verify_redirect_url($redirect_url)
	{
		// Load the cURL library
		$this->load->library('curl');

		// Add Options
		$this->curl->option('TIMEOUT', 5);
		$this->curl->option('CONNECTTIMEOUT', 5);
		$this->curl->option('RETURNTRANSFER', TRUE);

		// Do a simple get
		$this->curl->simple_get($redirect_url);

		// Return the http response code
		return $this->curl->info['http_code'];
	}


    // **********************************************************************
    //  Public functions 
    //  Functions used by routes.php
    // **********************************************************************

    /**
     * clv1_conversion_check()
     * Determine if there is a conversion
     */
	public function clv1_conversion_check()
	{

		//if test or local, turn off
		//$this->output->enable_profiler(false);

        // Make sure that the requested number of conversions required is not greater the number of displayed offers
        $conversions_required = $this->input->post('conv_req') ? $this->input->post('conv_req') : '1';
        $offer_count = $this->input->post('o_count') ? $this->input->post('o_count') : MAXIMUM_DISPLAY_OFFER_FEED;

        $conversions_required = ($conversions_required > $offer_count) ? $offer_count : $conversions_required;

		// Retrieve Post Data
		$post_data = array(
			'type' 			=> $this->input->post('type'),
			'affiliate_id' 	=> $this->input->post('aff_id'),
			'widget_id' 	=> $this->input->post('w_id'),
			'ip' 			=> $this->input->post('ip'),
			'redirect_url'	=> $this->input->post('red_url'),
			'offer_count'	=> $offer_count,
			'conv_req'		=> $conversions_required
			);

		// Check to see if there are conversions for specified 'widget_id' and 'user_ip'
		$where_clause = array(
			'user_ip' 	=> $post_data['ip'],
			'widget_id' => $post_data['widget_id']
			);

		$conversions_result = $this->cimongo->where($where_clause, TRUE)->get('ho_widget_conversions')->result_array();

		//print_r($post_data);
		//print_r($conversions_result);		

		// Prepare AJAX array
		$ajax_array = array('success' => '0', 'redirect' => '', 'offer_ids' => array() );

		if(count($conversions_result) > 0)
		{
			// Set 'success' to TRUE
			$ajax_array['success'] = '1';

			// Add converting offer IDs to the ajax array
			foreach($conversions_result as $conversion)
				$ajax_array['offer_ids'][] = $conversion['offer_id'];						

			// If the number of conversions is equal or greater than the number of conversions required, then redirect
	        if(count($conversions_result) >= $conversions_required)
	        {
				// Prepare redirect URL - prep_url() makes sure that it has http:// in the URL
				$redirect_url = prep_url(base64_decode($post_data['redirect_url']));

				// Verify if the URL exists, else use Global Redirect
				$http_response_code = $this->verify_redirect_url($redirect_url);

				// Check to see if the website returns a 'successful' http_code
				if($http_response_code >= 200 && $http_response_code < 300)
					$ajax_array['redirect'] = $redirect_url;
				else
					$ajax_array['redirect'] = GLOBAL_REDIRECT_URL;
			}
		}

		// Display JSON code 
		echo json_encode($ajax_array, JSON_UNESCAPED_SLASHES);
	}


	//TODO: add support for all types of lockers
	public function check_widget(){

		header('Content-Type: application/json');
		
		$this->output->enable_profiler(false);
		
		$cfg = array(
				'widget' => array('collection' => 'clv1_user_widgets', 'key' => 'widget_id', 'table' => 'user_widgets'),
				'locker' => array('collection' => 'clv1_user_locks', 'key' => 'lock_id', 'table' => 'user_locks'),
				'landing' => array('collection' => 'clv1_user_lps', 'key' => 'lp_id', 'table' => 'user_lps'),
				'wall' => array('collection' => 'clv1_user_walls', 'key' => 'wall_id', 'table' => 'user_walls'),
			);
		
        $this->load->database();

        //Retrieve items from DB table and MongoDB collection
        $mysqlItems = $this->db->where(array($cfg[$this->input->post('t')]['key'] => $this->input->post('wi')))->get($cfg[$this->input->post('t')]['table'])->result_array();

		$mongoItems = $this->cimongo->where(array($cfg[$this->input->post('t')]['key'] => $this->input->post('wi')))->get($cfg[$this->input->post('t')]['collection'])->result_array();
		
		$success = false;
		$inSync = true;
		$nonMatchingFields = array();

		//check if there are any items in the responses
		if(isset($mongoItems[0]) && isset($mysqlItems[0])){
			$mongoItem = 	$mongoItems[0];
			$success = true;

			$mysqlItem = $mysqlItems[0];
			$oldKeys = array_keys($mysqlItem);
			
			//compare if items are the same field-wise
			foreach ($oldKeys as $key) {
				//BOOLEANS ARE STORING DIFF?
				if(isset($mongoItem[$key]) && $mongoItem[$key] != $mysqlItem[$key]){
					$inSync = false;
					$nonMatchingFields[$key] = array('v1' => $mysqlItem[$key], 'v2' => $mongoItem[$key]);
				}
			}

		}else{
			$mongoItem = array();
			$mysqlItem = array();
		}		

		echo json_encode($data = array('item' => $mongoItem, 'success' => $success, 'old_item' => $mysqlItem , 'inSync' => $inSync, 'nonSyncFields' => $nonMatchingFields));
	}

}