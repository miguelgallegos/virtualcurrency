<?php if (!defined('BASEPATH')) die();

class User_Controller extends MY_Controller {

    function __construct()
    {
        parent::__construct();

        // Check to make sure user is logged in
        if (!$this->ion_auth->logged_in())
        {
            //redirect them to the login page
            redirect('login');
        }
        elseif (!$this->ion_auth->is_admin()) //remove this elseif if you want to enable this for non-admins
        {
            //redirect them to the home page because they must be an administrator to view this
            return show_error('You must be an administrator to view this page.');
        }
    }


    // **********************************************************************
    //  Public functions 
    //  Each function corresponds to a page
    // **********************************************************************

    /*******************
     * Dashboard
     *******************/
    public function dashboard()
    {        
        // Set the current page
        $this->data['current_page'] = 'dashboard';

        // Load dashboard view via Smarty Parser
        $this->parser->parse('admin/pages/dashboard', $this->data);
    }


}
