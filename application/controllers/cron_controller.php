<?php if (!defined('BASEPATH')) die();



// Cron Jobs: v2.verifystation.com

# * * * * * php /home/production_ie2k/public_html/index.php cron_controller update_ho_widget_conversions
# * * * * * php /home/production_ie2k/public_html/index.php cron_controller retrieve_content_locker_v1
# */5 * * * * php /home/production_ie2k/public_html/index.php cron_controller update_ho_offers
# */5 * * * * php /home/production_ie2k/public_html/index.php cron_controller update_ho_offer_server

// Cron Jobs: staging.verifystation.com

#* * * * * staging.verify.com /usr/local/bin/php /home/staging.verify.com/public_html/index.php cron_controller update_ho_widget_conversions
#* * * * * staging.verify.com sleep 10 && /usr/local/bin/php /home/staging.verify.com/public_html/index.php cron_controller retrieve_content_locker_v1
#*/5 * * * * staging.verify.com sleep 20 && /usr/local/bin/php /home/staging.verify.com/public_html/index.php cron_controller update_ho_offers
#*/5 * * * * staging.verify.com sleep 30 && /usr/local/bin/php /home/staging.verify.com/public_html/index.php cron_controller update_ho_offer_server

// See /var/spoo/mail/root for details on cron jobs 

 
class Cron_controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        // If this is not accessed as a command line request, then send to a 404 response
        if(!$this->input->is_cli_request())
        {          
          show_404();
          return;
        }        
    }

    private function get_elapsed_time($update_function)
    {
        // Load update_model
        $this->load->model('common/update_model');

        // Start Stopwatch
        $time_start_update = microtime(true);
        
        // Call the update function
        call_user_func( array($this->update_model, $update_function) );

        // Stop Stopwatch
        $time_end_update = microtime(true);

        // Return the elapsed time
        return $time_end_update - $time_start_update;
    }

    public function update_ho_widget_conversions()
    {
        // Update Function
        $update_function = 'update_ho_widget_conversions';

        // Run update and get elapsed time
        $elapsed_time = $this->get_elapsed_time($update_function);

        // Echo out the time it took to run this function
        echo $update_function . ' ran in: ' . number_format($elapsed_time, 2) . ' seconds';
    }

    public function update_ho_offers()
    {
        // Update Function
        $update_function = 'update_ho_offers';

        // Run update and get elapsed time
        $elapsed_time = $this->get_elapsed_time($update_function);

        // Echo out the time it took to run this function
        echo $update_function . ' ran in: ' . number_format($elapsed_time, 2) . ' seconds';
    }

    public function update_ho_offer_server()
    {
        // Update Function
        $update_function = 'update_ho_offer_server';

        // Run update and get elapsed time
        $elapsed_time = $this->get_elapsed_time($update_function);

        // Echo out the time it took to run this function
        echo $update_function . ' ran in: ' . number_format($elapsed_time, 2) . ' seconds';
    }        

    public function retrieve_content_locker_v1()
    {
        // Update Function
        $update_function = 'retrieve_content_locker_v1';

        // Run update and get elapsed time
        $elapsed_time = $this->get_elapsed_time($update_function);

        // Echo out the time it took to run this function
        echo $update_function . ' ran in: ' . number_format($elapsed_time, 2) . ' seconds';
    }

}