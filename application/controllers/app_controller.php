<?php if (!defined('BASEPATH')) die();

class App_controller extends MY_Controller {

    function __construct()
    {
        parent::__construct();

        // Check to make sure user is logged in
        if (!$this->ion_auth->logged_in())
        {
            //redirect them to the login page
            //redirect('login');
        }
        //elseif (!$this->ion_auth->is_admin()) //remove this elseif if you want to enable this for non-admins
        //{
        //    //redirect them to the home page because they must be an administrator to view this
        //    return show_error('You must be an administrator to view this page.');
        //}
        
    }
	
	//loads main app?
	public function index(){

        //loads offers on default view
        //$credit = $this->bank_model->credit();
        //$data = array('credit' => $credit);
        $this->output->enable_profiler(FALSE);//!!!!        
        $this->load->view('app_layout' /*, $data*/);
	}

    public function sidebar(){
        $this->load->view('includes/sidebar');
    }
    public function sidebarRight(){
        $this->load->view('includes/sidebarRight');
    }

    public function invite(){
        $this->load->view('invite');
    }

    public function settings(){
        $this->load->view('settings');
    }    

	//LOAD MAIN!
}
