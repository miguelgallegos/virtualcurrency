<?php if (!defined('BASEPATH')) die();

class login_controller extends MY_Controller {

    function __construct()
    {
        parent::__construct();

        // Load form helper
        $this->load->helper('form');
    }    

    // **********************************************************************
    //  Private Functions
    //  Functions used within this controller only
    // **********************************************************************

    private function load_login_view($admin_view_data)
    {
        $this->data['admin_view_head'] = $this->load->view('admin/includes/head', $admin_view_data, TRUE);
        $this->data['admin_view_logo'] = $this->load->view('admin/includes/logo', '', TRUE);
        $this->data['admin_view_footer_scripts'] = $this->load->view('admin/includes/footer_scripts', '', TRUE);

        $this->load->view('admin/layouts/login_layout', $this->data);
    }


    // **********************************************************************
    //  Public functions 
    //  Each function corresponds to a page
    // **********************************************************************
    public function login()
    {
        // Load Authentication Model
        $this->load->model('common/auth_model');

        // Verify Login
        $this->auth_model->verify_login();

        // Set the current page
        $admin_view_data['current_page'] = 'login';

        // Return message to the view
        $admin_view_data['message'] = $this->session->flashdata('message');

        // Set content for this page
        $admin_view_data['content'] = $this->load->view('admin/login', $admin_view_data, TRUE);

        // Load the admin template with the content and data
        $this->load_login_view($admin_view_data);
    }

    public function forgot_password()
    {
        // Load Authentication Model
        $this->load->model('common/auth_model');        

        // Process forgot_password()
        $this->auth_model->forgot_password();

        // Set the current page
        $admin_view_data['current_page'] = 'forgot_password';

        // Return message to the view
        $admin_view_data['message'] = $this->session->flashdata('message');

        // Set content for this page
        $admin_view_data['content'] = $this->load->view('admin/forgot_password', $admin_view_data, TRUE);

        // Load the admin template with the content and data
        $this->load_login_view($admin_view_data);
    }

    public function reset_password($reset_code)
    {
        // Load Authentication Model
        $this->load->model('common/auth_model');        

        // Process reset_password
        $admin_view_data = $this->auth_model->reset_password($reset_code);

        // Set the current page
        $admin_view_data['current_page'] = 'reset_password';

        // Return message to the view
        $admin_view_data['message'] = $this->session->flashdata('message');

        // Set content for this page
        $admin_view_data['content'] = $this->load->view('admin/reset_password', $admin_view_data, TRUE);

        // Load the admin template with the content and data
        $this->load_login_view($admin_view_data);
    }

	public function logout()
	{
		//log the user out
		$logout = $this->ion_auth->logout();

		//redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
		redirect('login');
	}    


}