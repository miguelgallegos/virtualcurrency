<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['host'] = "ds027698.mongolab.com";
$config['port'] = 27698;
$config['db'] = "jpnc_dev_virtual_currency";
$config['user'] = "jpnc_vc";
$config['pass'] = "eRjvEuG3u5EvwE7";

//ENV specific config load
$sn = $_SERVER['SERVER_NAME'];
if(file_exists(dirname(__FILE__) . "/" . ENVIRONMENT . "/cimongo.$sn.php")){	
	include(ENVIRONMENT . "/cimongo.$sn.php");	
}

/*  
 * Defaults to FALSE. If FALSE, the program continues executing without waiting for a database response. 
 * If TRUE, the program will wait for the database response and throw a MongoCursorException if the update did not succeed.
*/
$config['query_safety'] = FALSE;

//If running in auth mode and the user does not have global read/write then set this to true
$config['db_flag'] = TRUE;

//consider these config only if you want to store the session into mongoDB
//They will be used in MY_Session.php
$config['sess_use_mongo'] = FALSE;
$config['sess_collection_name']	= 'ci_sessions';