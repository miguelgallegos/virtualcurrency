<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//TODO: check if needed
// Retrieve conversions 180 seconds ago
if(!defined('CONVERSIONS_RETRIEVE_TIME')) define('CONVERSIONS_RETRIEVE_TIME', 180);

// Delete conversions after 24 hours
if(!defined('CONVERSIONS_DELETE_TIME')) define('CONVERSIONS_DELETE_TIME', 60 * 60 * 24);

// Categories used in Has Offers
if(!defined('HO_CATEGORY_CONTENT_LOCKER_MOBILE')) define('HO_CATEGORY_CONTENT_LOCKER_MOBILE', '2');
if(!defined('HO_CATEGORY_CONTENT_LOCKER_WEB')) define('HO_CATEGORY_CONTENT_LOCKER_WEB', '4');
if(!defined('HO_CATEGORY_OFFER_WALL_NONINCENT')) define('HO_CATEGORY_OFFER_WALL_NONINCENT', '6');
if(!defined('HO_CATEGORY_PREMIUM_OFFERS')) define('HO_CATEGORY_PREMIUM_OFFERS', '8');

if(!defined('HO_TARGETING_ANDROID')) define('HO_TARGETING_ANDROID', '28');
if(!defined('HO_TARGETING_IOS')) define('HO_TARGETING_IOS', '32');
if(!defined('HO_TARGETING_IPHONE')) define('HO_TARGETING_IPHONE', '48');
if(!defined('HO_TARGETING_IPAD')) define('HO_TARGETING_IPAD', '50');
if(!defined('HO_TARGETING_MOBILE')) define('HO_TARGETING_MOBILE', '79');
if(!defined('HO_TARGETING_DESKTOP')) define('HO_TARGETING_DESKTOP', '81');

// Domains and URLS
if(!defined('TRACKING_DOMAIN')) define('TRACKING_DOMAIN', 'theblumarket.go2cloud.org');
if(!defined('GLOBAL_REDIRECT_URL')) define('GLOBAL_REDIRECT_URL', 'http://flightshopping.biz/');

// Offer Walls 
if(!defined('MAXIMUM_DISPLAY_OFFER_FEED')) define('MAXIMUM_DISPLAY_OFFER_FEED', '10');

/*
|--------------------------------------------------------------------------
| HasOffers Global Constants
|--------------------------------------------------------------------------
*/

// HasOffers API Info
if(!defined('NET_ID')) define('NET_ID', 'theblumarket');
if(!defined('API_KEY')) define('API_KEY', 'NET7fSY1mAnsRfk2IX4nqtgTHz5nas');
if(!defined('API_DOMAIN')) define('API_DOMAIN', 'http://theblumarket.api.hasoffers.com');


// Rackspace CDN container
if(!defined('RS_LOCATION')) define('RS_LOCATION','ORD');
if(!defined('RS_CONTAINER_NAME')) define('RS_CONTAINER_NAME','verifystation_lps');

// Mailchimp Configuration
if(!defined('MAILCHIMP_LIST_ID')) define('MAILCHIMP_LIST_ID','39f38ed768');

//tech support email
if(!defined('TECH_SUPPORT_EMAIL')) define('TECH_SUPPORT_EMAIL', 'techsupport@98interactive.com');
//contact support email
if(!defined('CONTACT_SUPPORT_EMAIL')) define('CONTACT_SUPPORT_EMAIL','support@theblumarket.com');
//accounting email
if(!defined('ACCOUNTING_EMAIL')) define('ACCOUNTING_EMAIL','accounting@theblumarket.com');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
if(!defined('FILE_READ_MODE')) define('FILE_READ_MODE', 0644);
if(!defined('FILE_WRITE_MODE')) define('FILE_WRITE_MODE', 0666);
if(!defined('DIR_READ_MODE')) define('DIR_READ_MODE', 0755);
if(!defined('DIR_WRITE_MODE')) define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

if(!defined('FOPEN_READ')) define('FOPEN_READ',                                                 'rb');
if(!defined('FOPEN_READ_WRITE')) define('FOPEN_READ_WRITE',                                             'r+b');
if(!defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')) define('FOPEN_WRITE_CREATE_DESTRUCTIVE',         'wb'); // truncates existing file data, use with care
if(!defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')) define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',       'w+b'); // truncates existing file data, use with care
if(!defined('FOPEN_WRITE_CREATE')) define('FOPEN_WRITE_CREATE',                                 'ab');
if(!defined('FOPEN_READ_WRITE_CREATE')) define('FOPEN_READ_WRITE_CREATE',                               'a+b');
if(!defined('FOPEN_WRITE_CREATE_STRICT')) define('FOPEN_WRITE_CREATE_STRICT',                           'xb');
if(!defined('FOPEN_READ_WRITE_CREATE_STRICT')) define('FOPEN_READ_WRITE_CREATE_STRICT',         'x+b');

/* End of file constants.php */
/* Location: ./application/config/constants.php */
