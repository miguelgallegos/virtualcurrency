<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//db overrides - 98interactive is $active_group when working on localhost
$db['theblumarket']['hostname'] = 'localhost';
$db['theblumarket']['username'] = 'root';
$db['theblumarket']['password'] = '';
$db['theblumarket']['database'] = '98int_main';