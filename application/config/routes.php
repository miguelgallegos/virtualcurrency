<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

##------------------------------------------##
// Core Routing and Controllers
##------------------------------------------##
$route['default_controller'] = "app_controller/index";
$route['404_override'] = '';


##------------------------------------------##
// Authentication Routing and Controllers
##------------------------------------------##

$route['login'] 					= 'login_controller/login';
$route['logout']					= 'login_controller/logout';
$route['forgot_password']			= 'login_controller/forgot_password';
$route['reset_password/?(.*)']		= 'login_controller/reset_password/$1';


##------------------------------------------##
// Basic Routing and Controllers
##------------------------------------------##
//////$route['app']				= 'app_controller/index';
$route['offers']				= 'offers_controller';
$route['offer/detail']				= 'offers_controller/detail';
$route['offer/detail/(:num)']				= 'offers_controller/detail/$1';

$route['rewards']				= 'rewards_controller';
$route['reward/detail']				= 'rewards_controller/detail';
$route['reward/detail/(:num)']				= 'rewards_controller/detail/$1';


$route['app/(.*)']   = 'app_controller/$1';
$route['ajax/(.*)']			= 'ajax_controller/$1';
$route['api/(.*)']   = 'api_controller/$1';
/*
$route['admin/(.*)']		= 'admin_controller/$1';
$route['admin/(.*)/(.*)']	= 'admin_controller/$1/$2';

$route['ajax/(.*)']			= 'ajax_controller/$1';
$route['clv1/(.*)']			= 'locker_controller/$1';
$route['api/(.*)']   = 'api_controller/$1';
*/
//$route['user']			= 'user_controller/dashboard';
//$route['user/(.*)']		= 'user_controller/$1';

/* End of file routes.php */
/* Location: ./application/config/routes.php */