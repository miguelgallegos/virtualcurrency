'use strict';

angular.module('myApp', [
    'ngTouch',
    'ngRoute',
    'ngAnimate',
    'myApp.controllers',
    //'myApp.memoryServices'
    'myApp.restServices'
]).
config(['$routeProvider', function ($routeProvider) {
	//$routeProvider.when('/home', {templateUrl: 'partials/home.html'});
    $routeProvider.when('/offers', {templateUrl: 'partials/offer-list.html', controller: 'OffersListCtrl'});
    $routeProvider.when('/offer/:offerId', {templateUrl: 'partials/offer-detail.html', controller: 'OfferDetailCtrl'});
	$routeProvider.when('/rewards', {templateUrl: 'partials/rewards-list.html', controller: 'RewardsListCtrl'});
	$routeProvider.when('/reward/:rewardId', {templateUrl: 'partials/reward-detail.html', controller: 'RewardDetailCtrl'});
	$routeProvider.when('/invite', {templateUrl: 'partials/invite.html'});
	$routeProvider.when('/settings', {templateUrl: 'partials/settings.html', controller: 'SettingsCtrl'});

    $routeProvider.otherwise({redirectTo: '/offers'});
}]);