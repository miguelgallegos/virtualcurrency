'use strict';

angular.module('myApp.controllers', [])
    .controller('MainCtrl', ['$scope', '$rootScope', '$window', '$location', function ($scope, $rootScope, $window, $location) {
        $scope.slide = '';
        $scope.mike = '';        
        $rootScope.back = function() {
          $scope.slide = 'slide-right';
          $window.history.back();
        }
        $rootScope.go = function(path){
          $scope.slide = 'slide-left';
          $location.url(path);
        }
    }]).controller('OffersListCtrl', ['$scope', '$routeParams', 'Offer', function ($scope, $routeParams, Offer) {
        $scope.offers = Offer.query();
    }])
    .controller('OfferDetailCtrl', ['$scope', '$routeParams', '$http', 'Offer', function ($scope, $routeParams, $http, Offer) {
        $scope.offer = Offer.get({id: $routeParams.offerId});

        //download button handler
        $scope.downloadButton = {};
        $scope.downloadButton.doClick = function(item, event){
            //$http.get('');// check conversions API check!
            //make service to support this! something local
            //create a CI application
            //mimic a function here, with code only
            //user bank

        }
    }])
    .controller('RewardsListCtrl', ['$scope', '$routeParams', 'Rewards', function ($scope, $routeParams, Rewards) {
        $scope.rewards = Rewards.query();
    }])
    .controller('RewardDetailCtrl', ['$scope', '$routeParams', 'Rewards', function ($scope, $routeParams, Rewards) {
        $scope.reward = Rewards.get({rewardId: $routeParams.rewardId});
    }])
    ;
