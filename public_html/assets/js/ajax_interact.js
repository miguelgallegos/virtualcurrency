$(document).ready(function(){

	var myInterval;
	var execCount = 0;
	var duration = 2000;
	var maxExecPost = 10;
	$('.search-widget-id').click(function(e){
		execCount = 0;
		myInterval = setInterval(execPost, duration);
	});

	$('.cancel-search-widget-id').click(function(e){
		stopExecPost();
	});


	function stopExecPost() {
	    clearInterval(myInterval);
	}

	function execPost(){

			if(execCount >= maxExecPost){
				stopExecPost();
			}

			execCount++;

			var tk = $('.search-form').find('input[type="hidden"]').val();

			var v = $('.widget-id-input').val();

			var t = $("input:radio[name ='radio-inline']:checked").val();
			
			$.post(site_url + 'ajax/check_widget', {wi: v, t: t, ie2k_token: tk }, function(data){

				//animate progress bar
				$('.progress-bar').css('width', (execCount * maxExecPost)+'%');

				if(data.success){
					stopExecPost();
					var color = 'alert-success';
					var icon = 'check';
					var partMessage = 'was found';
				} else {
					var color = 'alert-danger';
					var icon = 'times';
					var partMessage = 'was not found';
				}

					var anAlert = '<div class="alert '+ color +' fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-'+ icon +'"></i>' +
							'<strong>Item ID '+ v +'</strong> '+ partMessage +' ('+ t +' type).</div>';
					$('.results').html('');		
					$('.results').append(anAlert);

					if(!data.inSync){
						$.each(data.nonSyncFields, function(i, e){
							//console.log(i);
							//console.log(e);

						var anAlert1 = '<div class="alert alert-warning fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-warning"></i>' +
							'<strong>Field "'+ i +'" </strong> not matching V1: <span class="label label-primary">'+ e.v1 +'</span> => V2: <span class="label label-warning">'+ e.v2 +'</span></div>';
							$('.results').append(anAlert1);

						});
					}


			});

	}

});