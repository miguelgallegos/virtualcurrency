'use strict';

angular.module('myApp.controllers', [])
    .controller('MainCtrl', ['$scope', '$rootScope', '$window', '$location', 'AccDetails', function ($scope, $rootScope, $window, $location, AccDetails) {
        $scope.slide = '';
        $rootScope.back = function() {
          $scope.slide = 'slide-right';
          $window.history.back();
        }
        $rootScope.go = function(path){
          $scope.slide = 'slide-left';
          $location.url(path);
        }

        $scope.accDetails = AccDetails.get();

        //additions per mobile ngJS
      $rootScope.$on("$routeChangeStart", function(){
        $rootScope.loading = true;
      });

      $rootScope.$on("$routeChangeSuccess", function(){
        $rootScope.loading = false;
      });

    }]).controller('OffersListCtrl', ['$scope', '$routeParams', 'Offer', function ($scope, $routeParams, Offer) {
        $scope.offers = Offer.query();
    }])
    .controller('OfferDetailCtrl', ['$scope', '$routeParams', '$http', '$timeout', 'Offer', function ($scope, $routeParams, $http, $timeout, Offer) {
        $scope.offer = Offer.get({id: $routeParams.offerId});

        //download button handler
        $scope.downloadButton = {};
        $scope.showButton = true;                
        $scope.downloadButton.doClick = function(item, event){
    //DID can be generated on this page load
            //PASS User Id too?
            var link = item.offer.offer_link;
            var url_split  = link.split('?');
            var params = url_split[1].split('&');
            var did = '';
            
            for(var i = 0 ; i < params.length ; i ++){
                var toks = params[i].split('=');
                if(toks[0] == 'aff_sub'){
                    did = toks[1];
                }
            }

            //var test = setTimeout(function(){ console.log('testing');}, 2000);
            var monitor = function(){
                //EXTRACT the DownloadID! from aff_sub
                $http.get('ajax/conversion_check?did=' + did)
                //$http.get('ajax/conversion_check?did=mydownloadid999') //TEST
                .success(function(data, status, headers, config) {
                    console.log(data);
                    //CONVERION FOUND
                    if(data.id){ //has id will tell if conversion exists
                        
                        item.offer.offer_points = data.offer_points;
                        
                        var credit = $scope.accDetails.credit;
                        $scope.accDetails.credit = parseInt(credit) + parseInt(data.offer_points);
                        
                        //end monitor
                        $timeout.cancel(monitor);
                        $scope.showButton = false;
                    } else {
                        //check if downloaded, converted, don't show what's has been converted
                        $timeout(monitor, 2000);                        
                    }

                }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });

            }

            $timeout(monitor, 500);
            window.open(link);

        }


    }])
    .controller('RewardsListCtrl', ['$scope', '$routeParams', 'Rewards', function ($scope, $routeParams, Rewards) {
        $scope.rewards = Rewards.query();
    }])
    .controller('RewardDetailCtrl', ['$scope', '$rootScope', '$routeParams', '$http', 'Rewards', function ($scope, $rootScope, $routeParams, $http, Rewards) {
        $scope.reward = Rewards.get({id: $routeParams.rewardId});
        
        $scope.rewardButton = {};
        //$scope.rewardClaimError = '';
        $scope.showRewardButton = true;                
        $scope.rewardButton.doClick = function(item, event){                
          
                $http.get('ajax/redeemReward?rid=' + $routeParams.rewardId)                
                .success(function(data, status, headers, config) {
                    console.log(data);

                    if(data.redeemed){ //has id will tell if conversion exists
                                                                        
                        $scope.accDetails.credit = data.credit;                        
                        $scope.showRewardButton = false;

                    } else {
                        $scope.rewardClaimError = data.message;
                        $rootScope.toggle('overlay1', 'on');

                    }

                }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
                

        }


    }])
    .controller('SettingsCtrl', ['$scope', '$routeParams', function ($scope, $routeParams) {
        //$scope.reward = Rewards.get({id: $routeParams.rewardId});
        $scope.scrollItems = [
            {title:'Max Offers to display', value: '50'}, 
            {title: 'Max Rewards to display', value: '50'},
            {title: 'Alert me on new offers', value: 'true'},
            {title: 'Sort Offers by', value: 'Max Points'},
        ];
    }])
    /*
    .controller('LoginCtrl', ['$scope', '$routeParams', function ($scope, $routeParams) {
        console.log('Login');
    }])
*/
    ;
