'use strict';

(function () {

    var offers = [
        {"id": 192, "name": "Castle Clash - Incent (iOS, Tier 1)", "conversions": 2419, "advertiser":"Blind Ferret", "image": "https://media.go2speed.org/brand/files/theblumarket/192/Castle%20Clash.png"},
        {"id": 878, "name": "Slice - Your Smart Shopping Assistant - (iOS, INCENT, Free, US 36MB)", "conversions": 426, "advertiser":"AdAction", "image": "https://media.go2speed.org/brand/files/theblumarket/878/slice.png"},
        {"id": 440, "name": "MyVegas Slots - iPhone CA incent", "conversions": 214, "advertiser":"Raftika", "image": "https://media.go2speed.org/brand/files/theblumarket/440/MyVegas.jpeg"},
        {"id": 454, "name": "Get an iPhone 6 - (Desktop US, Email Submit)", "conversions": 0, "advertiser":"Spark Modal", "image": "https://media.go2speed.org/brand/files/theblumarket/454/iphone6.jpg"}
    ],

    rewards = [
        {"id": 1, "name": "$5 PayPal USD Funds", "points": 600, "image": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgREhQUERMVFBQUFR4YFhQUFxQUFRkaFBkWFiAWFhYeHCggGCYnGxcUJzEtJikuLi42Fx8zRDMvNygtLiwBCgoKDg0OFxAQGywlHyQvLS0sLCwsLDctLCssLCwsLCwvLCwsLCwsLCwsLCwsKywsLCwsLCwsLCwsLCwsLDAvK//AABEIAEoASgMBEQACEQEDEQH/xAAcAAABBAMBAAAAAAAAAAAAAAAABAUGBwECAwj/xAA6EAABAgMDBwoFAwUAAAAAAAABAgMABBEFEiEGEzFRcrHRBxQiMkFSYXGRkhUjYoGyM6HSF0JDgvD/xAAZAQEAAwEBAAAAAAAAAAAAAAAAAgMEAQX/xAAiEQACAgEDBQEBAAAAAAAAAAAAAQIRAwQSIRMxMkFRFCL/2gAMAwEAAhEDEQA/ALpdeUTdRSvadISOMAYMo2esVKOsqO4YCAMcyl9R9yuMAHMpfUfcrjABzKX1H3K4wAcyl9R9yuMAHMpfUfcrjABzJjUfcrjAApTjeJJUjtr1k+PiIAVAwAilT03dofimAFVYAbMprYTJyr0wRezaahNaVJwAr5mJQjukkRnLbFsps8r+Uvdlx4XFfyjb+WBi/VIP6v5Td1j2K/lHfywH6pHeW5ZLdB6bLCx4X0H1qd0celj9OrVS9on+RnKJZloHNlJYfp+mogpVsL7fIgGM2TBKHPo0Y8ymTGsUlxzmD0FbJ3QBrLE3E7I3QBxlz03dofiIAU1gCu+W60Lkm20NLror5IFY06WNysz6l1GikWGlrUlKdKlBI81Gkb265MCVui6pXkgsS4nOOOldOkQQBXwEYHqpXwblpo0QHlByKVZqkFDhcacqEkiigR2HXhGnDl3mfNh2coi0nNOMuIcQaKbUFAjwNYtatUUxdOz0rZFvJdeWwsUcShLidSkLANR4g6Y8yWOoqS7HpxyXJxY7Pnoq2TuissMyvUTsjcIATMn5ju0PxEAd70AUry22hfm22gcGm6nzWa7hG7Sx/lsw6qXKRGMgpdly0JYOEJSHLxJNB0cQPWkXZnUGVYVc1Z6KmLRk0AqW62kaypMeYotnpWik+VbK2UnXG2pc3mmakr7FKOHR8AI36fE4ptmHUZFLhESsCy3pqYaZQKlawD4JriT9ounLbFspxx3SSLbkZlKrbTmzVKEluo7QhFDGZqsHJpTvPwWE8roq2TujEbDrK9RGyNwgBGk/Md2h+IgDqDAHm/Laf5xPTDlajOFI8kYcY9TFGoJHmZpXNjKK9mnw0xYVq/R0uTK8KLV4UUqOcIl/bHyyMisoZnqMFCO1x75SANZJx9BFcs0I+yccE2TGUNlWS0puWWH5tYo5MAfLQO63/wBwivbLK7lwvhY5Rxqo8v6PfJtYzwKppwEXgUt10m91lxDU5F4Inpsb82Tt5XRV5HdGM1iqV6iNkbhADepXzHdofiIATWxPBmXedP8AjbUr70oP3IiUVckiMnSbPM5UTUnSTU+Zxj1jym7ZYmS7gkrNS+hCC9MuqCVrSFEIRhgDGeS35KfZGhPp47Xdj5JnLJ1AW3dSlWIN1pBpr1xCXQTpkl1mrQPZNZUzH672GpSyR7RhHVmxR8UHhyy8mO9jZDSDRCnjnVDQmlED7dsVT1MpduC2GnjHl8ksCgMBgB2CMxoNXVdE+R3QAvleojZG4QA2Pq+a5tD8RAEey9lp56Rdbl03lrKRdGBKa1NPQRbhajNNleVNxaRUScispCQObKFdFSkD74xu68Pph6E/hLralkpMlJJIVmG0oUU4i+s9KIYnxKf0nlVuMCzW6JAA0AAekYHybja/HDoX4AL8AauLwPlADtK9RGyNwgBmnF0ecHiD+wgDnnI6BjygsNyaWlQeU3dFKCpB8dIi7FlUFVWU5cW92nQjsfJFtl1Lq3SspNQLtMdZNcYnk1G6NJEIafbK2yUZyMxpC/ABfgAvwBq66AD5QBIJZJuJ2RujgEtsWapzpowcSMNRGowBG3Jy5gtKkkaRQndjHThp8Tl/q9i+EAHxOX+r2L4QAfE5f6vYvhAB8Tl/q9i+EAHxOX+r2L4QBkWkydF4/wCqhvAgBys2z33iFOApbH9p6x844dJOBABAGqkpOkCANM033R6CADNN90eggAzTfdHoIAM033R6CADNN90eggDKW29Q9BAHSACAP//Z"},
        {"id": 2, "name": "$10 iTunes US Gift Card", "points": 6000, "image": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgTEhUUEhQVFhQXGB8bFxYWFxMeIBcfHxgXGxoZFxwdHSghHR0lHxgbITEiJTUsLi4uGR80ODMsNzQ5LiwBCgoKDg0OGxAQGywmHyUvMi8sMi0sNCwsLCwsLCwvLDQsLCwsLCwsNSwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLP/AABEIAEoASgMBEQACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAFAgMEBgcAAf/EADoQAAECBAMCDAQFBQEAAAAAAAECAwAEESEFEjFBUQYTFCJSYXGBkZKh0TJCscEjU3KCsgczc+HwJP/EABoBAAIDAQEAAAAAAAAAAAAAAAEEAAMFAgb/xAAwEQABAwMCAwcDBAMAAAAAAAABAAIDBBESITETQVEFIlJhcZGhIzKxFMHw8UJigf/aAAwDAQACEQMRAD8A1XGMXKFcWg3HxHd1Dri6KLIXKBKCrnXdcyvEwxgFzdcmdc6R8TBwCF04mbc6R8TAwCl0vlTlDzjpvMThhS6RypzpHTeYnDCl14qacrTMrxMQxhS64zbmgUq2pqYmCN1LlMYdQbnMjaDr3RW6EHZQFWRDqCAQRQiohQhwXd1nEzOEuLKtStXfcxqsZoFSTquS/HWKl13GEfaOsVLo7I4NOrFVUbB6WvbT3hV87G6N1RDSppweWoQX7/sjj9Q7wrrEJp3AXdW3Er6jb1FRBFT4ghj0QZ9DzZosFK9gP1G8Qy3Fwu1cnRMlwAdW/fELVLphycEQN1UukJxd8CgUaCwit0Yuuroa++nOv9av5GGmN0CqLtUtDpHWPXu3x3guS5XPD5ZiVbDrwq6r4U7uode8xnSPdO7BmyuAxFyoM1i0w4ecbdEWA94vjga0aDVcl5KQ2CpKyKHKASNtK3I7NsdudiQDzUGouvGptaTVJKeyC6MO0IQDrIzKzrEyOKeAC/lUPtuP1hKSJ0JzZsrGuDtCqnjKXmXC2rUaHeNhEPxuErcgqXXabFCluHaY6LFLqIqcRALNV1dRm5xpxbhQoGjiwRtBzqsRDLYyGj0VJNt1aOBkpxsykKHNQM6txoQEjxNe6Fq1/Dh05ow956k4/ivGvrNbJOVPdr4mJS0+EfmVJJLuUNp0qISKVJoIve3FtyuQ65sjE041KOtc6ppVwVBCkmxp2ipodwhNgdUsd8KxzhG4BR8clwyu39tYzNnq3d0WU7+K3/YboSnA+SFmcVWoNCLinpDBivoq+Ij3ChYfkmppNlJoFHqJyq8FUMZ9LeKodEdlfLZzA5UGYdAuTbeY1MFQHKvOcIsPBIqo31APpEMbui7uoD7ZD7pCiDxq7j9ao0IbFjR5BKSuORstR/o5MvqXNZ15iltGWoFdXK37hGT201oEePn+yvonXyQrgdizLk4hEwlsI5+YrPNrela21hiugxprxk302VMDwZrOWhNO4WjPMoQ2llvmNqASA6o2KgdMteaDtooxgu4zrRkkk7jotG7B3uScljwcy/iOSy1m6iVN6nYL6DQCCRU7NDgPRD6R3IUKYmZRVWGgiYLf4rQSpsjKPibN9bkAU2iOmse3vuu2+h9eRQyaTiLHogvDnEMETLNLlQ0VKWKhBSFAFJ+IC4vYg7Ye7OjmdMWyX25peqkjDAW2ScMnHV4DMrNARxmXqooU164M0Yb2iwDyRjN6clZNPLeX8airtNvCN0hg2CRbIUFU0KmFi7VMhxsrQ+wnjnf8i/5qjmJ3cb6JaV2pV1/pfNJanMp+F5BQe0HMn7jvhTtRucF+hXdDJjLiTv8AlRcd4OFM8pg2S44CDYDIo1Ua6CgzeEXU9Zem4g3A+Qqp4i2oDORN7qfwtxdD6US7VOTtWTlFAugok06IGm/WKuz4DFeV/wBx+EayrD7Rs2CrfIUdEeAjT4yz7p6S41hxLrdQUmtE2zD5kW3i0VTWljMZ5/nkrIZTG8OCK8NcFkkOIfl6FqZGdKU0qFGlaDca9xrClBVvLDHLuz8J2sjGQez/ACR7hJL8kwhqU+d0gKA7eMcP0HfCNK/j1pmOw/gTNQeDTBnMrL5liNrJItQdbAqbekUF2qvD9FbJiXSHXK3/ABF2/eqKmO7jfRUPPePqp8ohy1DlIIICdai4JMFzgQQeaXJLTcb8lobjMviTACsqZpsbf+ug+hjHDn0knVpWrdlbFbZwVNnMLmWl5XUlJ3nQ9YOhEasc7JBdpWTLE+I2cE3xH1jrMqrRcJepAAqTsAqe6DxAN1ACTYK1cHeDDTP/AKpuiQjnJQdh6SuvcnfGXVVhk+lHz3K2aWl4Y4s39KtcKsTdmXi4eammVsG4y9e5RN/CHaWIQx25ndJVM/Gky5DZVx9onUePvDOSjShqpW51gEBAPKtkzK0cctfjFV8xhdju6PRcSHvEeadZQE9sElVkXUqWccQoLSopWNFDZ/qK3AOFio0uY7Ju6tkrwmqjLMtBY2lIHqk28IRdRkG8ZstJnaIcLTNullfBU0JbIvpkX9rQLVOwPyujJRHUt+EpOOYS0Dydi+8gJ9dYBp5X/e5T9dTxj6TdfRV/F8Rm37rV8JskfCD2be+HIYWR/b7pCeokmPe26II+kEWFjqncYYuqhYKGqVPaPpBvorA5R+QV2GIXaqDZaNwqwJaXFPITVKrqA+U7T2GM2lqBbByd7QpXB3EbtzVcEtth66y8glol4F0ck8WzpHJQunCk2/UPpA1UuEkIuYKmQTRZMEKZDqm1S16jvg3UyT8hg77yglsW2q2JHXHEkwjFyr4InzGzf+rQGMHkEpSkNpOUAVI1oKXjHMryb3XomwxtFkQMVlXckGxGTlcx/DR5U+0MMe626zJ4o8z3R7KMiTlfy0eVPtFmbuqp4MfhHsu5HK/lo8qfaDm7qpwY/CPZemUlugjyp9oGbuqJhj8I9l5yOV6CPKn2g5u6qcKPwj2XKk5XoI8qfaJm7qpwmeEey4Scr+Wjyp9ombuqHBj8I9keZbbSAEgAbgAITcSTqtVjQ1osEsmCFcv/2Q=="},
        {"id": 3, "name": "Xbox $10 Gift Code", "points": 5000, "image": "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSy766eUKTF5Hoa2VCgvIhKPRDJ6Rt_u0tPxcxiMwfpaxwGEvrt"},
        {"id": 4, "name": "Nexus 7 WiFi 16GB (2013)", "points": 150000, "image": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQ7-FZ9oJjLkgEg5Dxz1O_AK-pf3WtDq04Tmkt-O-V5BADI9RhI"}
    ]
    ,
    findById = function (id) {
        var offer = null,
            l = offers.length,
            i;
        for (i = 0; i < l; i = i + 1) {
            if (offers[i].id === id) {
                offer = offers[i];
                break;
            }
        }
        return offer;
    },

findRewardById = function (id) {
        var reward = null,
            l = rewards.length,
            i;
        for (i = 0; i < l; i = i + 1) {
            if (rewards[i].id === id) {
                reward = rewards[i];
                break;
            }
        }        
        return reward;
    };

    angular.module('myApp.memoryServices', [])
        .factory('Offer', [
            function () {
                return {
                    query: function () {
                        return offers;
                    },
                    get: function (offer) {                        
                        return findById(parseInt(offer.offerId));
                    }
                }

            }])
        .factory('Rewards', [
            function () {
                return {
                    query: function () {
                        return rewards;
                    },
                    get: function (reward) {                        
                        return findRewardById(parseInt(reward.rewardId));
                    }
                }

            }])
        ;

}());