'use strict';

angular.module('myApp.restServices', ['ngResource'])
    /*.factory('Employee', ['$resource',
        function ($resource) {
            return $resource('http://localhost:3000/employees/:employeeId', {});
        }])

    .factory('Report', ['$resource',
        function ($resource) {
            return $resource('http://localhost:3000/employees/:employeeId/reports', {});
        }])*/
    .factory('Offer', ['$resource', 
        function ($resource) {           	     	
            return $resource('http://local.virtualcurrency.com/api/offers/format/json/id/:id', {});
        }])
    .factory('Rewards', ['$resource', 
        function ($resource) {                      
            return $resource('http://local.virtualcurrency.com/api/rewards/format/json/id/:id', {});
        }])    
    .factory('AccDetails', ['$resource', 
        function ($resource) {                      
            return $resource('http://local.virtualcurrency.com/api/acc/format/json', {});
        }])
    ;

    //TODO: create conversion check
    //