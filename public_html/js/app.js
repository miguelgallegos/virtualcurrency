'use strict';

angular.module('myApp', [
    'ngTouch',
    'ngRoute',
    'ngAnimate',
    'mobile-angular-ui',
    'myApp.controllers',
    //'myApp.memoryServices'
    'myApp.restServices'
]).
config(['$routeProvider', function ($routeProvider) {
	//$routeProvider.when('/home', {templateUrl: 'partials/home.html'});
    //$routeProvider.when('/offers', {templateUrl: 'partials/offer-list.html', controller: 'OffersListCtrl'});
    //$routeProvider.when('/offer/:offerId', {templateUrl: 'partials/offer-detail.html', controller: 'OfferDetailCtrl'});
	//$routeProvider.when('/rewards', {templateUrl: 'partials/rewards-list.html', controller: 'RewardsListCtrl'});
	//$routeProvider.when('/reward/:rewardId', {templateUrl: 'partials/reward-detail.html', controller: 'RewardDetailCtrl'});
	//$routeProvider.when('/invite', {templateUrl: 'partials/invite.html'});
	//$routeProvider.when('/settings', {templateUrl: 'partials/settings.html', controller: 'SettingsCtrl'});

    //CI Integration
    //$routeProvider.when('/login', {templateUrl: 'login', controller: 'LoginCtrl'});
    /*$routeProvider.when('/login', {templateUrl: 'login', controller: 'LoginCtrl'});
    $routeProvider.when('/login', {templateUrl: 'login', controller: 'LoginCtrl'});
    $routeProvider.when('/login', {templateUrl: 'login', controller: 'LoginCtrl'});
    */

    $routeProvider.when('/offers', {templateUrl: 'offers', controller: 'OffersListCtrl'});
    $routeProvider.when('/offer/:offerId', {templateUrl: 'offer/detail', controller: 'OfferDetailCtrl'});
    $routeProvider.when('/rewards', {templateUrl: 'rewards', controller: 'RewardsListCtrl'});    
    $routeProvider.when('/reward/:rewardId', {templateUrl: 'reward/detail', controller: 'RewardDetailCtrl'});
    $routeProvider.when('/invite', {templateUrl: 'app/invite'});
    $routeProvider.when('/settings', {templateUrl: 'app/settings', controller: 'SettingsCtrl'}); //settings will be using a controller


    $routeProvider.otherwise({redirectTo: '/offers'});
}]);